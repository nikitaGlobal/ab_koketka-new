<?php
    /**
     * Show options for ordering
     *
     * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
     *
     * HOWEVER, on occasion WooCommerce will need to update template files and you
     * (the theme developer) will need to copy the new files to your theme to
     * maintain compatibility. We try to do this as little as possible, but it does
     * happen. When this occurs the version of the template file will be bumped and
     * the readme will list any important changes.
     *
     * @see         https://docs.woocommerce.com/document/template-structure/
     * @package     WooCommerce/Templates
     * @version     3.6.0
     */
    
    if ( ! defined('ABSPATH')) {
        exit;
    }
    $filter = apply_filters('NGWooFilterparams', array());
    $a      = 1;
?>
<form class="woocommerce-ordering_" method="get" action="">
    <div class="products_filter">
        <a href="#0" class="filter_button">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/filter.svg"
                 alt="Filter">
            филтър
        </a>

        <div class="filter_show" <?php if (isset($_REQUEST[$filter['prefix']])){?>style="display:block;"<?php } ?>>
            <div class="select_wrap">
                <?php foreach ($filter['params'] as $key => $param) {
                    ?>
                    <div class="<?php if ($param['fieldtype'] == 'range') {
                        echo 'price';
                    } else {
                        echo $param['fieldtype'];
                    } ?>_item <?php echo $key; ?>_<?php echo $param['fieldtype']; ?>">
                        <label for="<?php echo $key; ?>"><?php echo $param['label']; ?></label>
                        <?php
                            ngTheme::wooFilter($key, $param);
                        ?></div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php if (isset($_GET['product_cat'])) { ?>
        <input type="hidden" name="product_cat"
               value="<?php echo $_GET['product_cat']; ?>">
    <?php } ?>
    <input id="ngsearchSubmit" type="submit" style="display:none;">
</form>
<?php return; ?>
<select name="orderby" class="orderby"
        aria-label="<?php esc_attr_e('Shop order', 'woocommerce'); ?>">
    <?php foreach ($catalog_orderby_options as $id => $name) : ?>
        <option value="<?php echo esc_attr($id); ?>" <?php selected($orderby,
            $id); ?>><?php echo esc_html($name); ?></option>
    <?php endforeach; ?>
</select>
<input type="hidden" name="paged" value="1"/>
<?php wc_query_string_form_fields(null,
    array('orderby', 'submit', 'paged', 'product-page')); ?>
</form>
