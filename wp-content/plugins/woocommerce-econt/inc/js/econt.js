
jQuery(document).ready(function(){
//econt checkout and order office autocomplete

jQuery( "#econt_offices_town" ).autocomplete({
minLength: 2,
source: function( request, response ) {
jQuery.ajax({
url: ajaxurl,
dataType: "json",
data: {

action:'econt_handle_ajax', 
//city: request.term
city: request.term

},
success: function( data ) {
//response( data );
  response(jQuery.map(data, function(item) {
                        return {
                            label:      item.label,
                            value:      item.value,
                            city_id:    item.id,
                            post_code:   item.post_code
                            
                       };
                }));

},




});
},


select: function( event, ui ) {
   
var city_id = ui.item.city_id;
var post_code = ui.item.post_code;

jQuery('#econt_offices_postcode').val(post_code);
jQuery('#econt_office_locator').show(); //show office locator button after the city is selected
jQuery('#econt_offices_postcode, label[for="econt_offices_postcode"], #econt_offices, label[for="econt_offices"]').show();

jQuery.ajax({
url: ajaxurl,
dataType: "json",
data: {

action:'econt_handle_ajax', 
office_city_id: city_id, 
delivery_type: 'to_office'
},
success: function( data ) {

 jQuery('#econt_offices').empty()
//jQuery("#econt_offices").selectWoo();
jQuery("#econt_offices").select2();

jQuery.each(data, function(key, value) {

  var newOption = new Option(value.value + ' [о.к.:' + value.id + ']', value.id, false, false);
  jQuery('#econt_offices').append(newOption).trigger('change');

});
jQuery(".select2-container").css("width", "100%"); //fix a bug which shrinks the width with some WP themes
if(econt_php_vars.isCheckout){ //if we are in checkout not in admin panel
  calculate_loading(); //calculate loading cost for shipping to office
}
}
});



},


});
//end of econt checkout and order office autocomplete

//econt checkout and order machine autocomplete
jQuery( "#econt_machines_town" ).autocomplete({
minLength: 2,
source: function( request, response ) {
jQuery.ajax({
url: ajaxurl,
dataType: "json",
data: {

action:'econt_handle_ajax', 
//city: request.term
city: request.term

},
success: function( data ) {
//response( data );
  response(jQuery.map(data, function(item) {
                        return {
                            label:      item.label,
                            value:      item.value,
                            city_id:    item.id,
                            post_code:   item.post_code
                            
                       };
                }));

},




});
},


select: function( event, ui ) {
   
var city_id = ui.item.city_id;
var post_code = ui.item.post_code;

jQuery('#econt_machines_postcode').val(post_code);
jQuery('#econt_machines_postcode, label[for="econt_machines_postcode"], #econt_machines, label[for="econt_machines"]').show();

jQuery.ajax({
url: ajaxurl,
dataType: "json",
data: {

action:'econt_handle_ajax', 
machine_city_id: city_id,
//delivery_type: 'to_office'

},
success: function( data ) {

 jQuery('#econt_machines').empty()

//selectValues = { "1": "test 1", "2": "test 2" };
jQuery.each(data, function(key, value) {
    jQuery('#econt_machines').append(jQuery("<option/>", {
        value: value.id,
        text: value.value + ' [о.к.:' + value.id + ']'
    }));
});

calculate_loading(); //calculate loading cost for shipping to machine


}
});



},


});
//end of econt checkout and order machine autocomplete


//econt admin settings office autocomplete
jQuery( "#woocommerce_econt_shipping_method_office_town" ).autocomplete({
minLength: 2,
source: function( request, response ) {
jQuery.ajax({
url: ajaxurl,
dataType: "json",
data: {

action:'econt_handle_ajax', 
city: request.term

},
success: function( data ) {
  response(jQuery.map(data, function(item) {
                        return {
                            label:      item.label,
                            value:      item.value,
                            city_id:    item.id,
                            post_code:   item.post_code
                            
                       };
                }));

},


});
},



select: function( event, ui ) {
   
var city_id = ui.item.city_id;
var post_code = ui.item.post_code;


jQuery('#woocommerce_econt_shipping_method_office_postcode').val(post_code);

jQuery.ajax({
url: ajaxurl,
dataType: "json",
data: {

action:'econt_handle_ajax', 
office_city_id: city_id,
delivery_type: 'from_office'

},
success: function( data ) {

 jQuery('#woocommerce_econt_shipping_method_office_office').empty()

//selectValues = { "1": "test 1", "2": "test 2" };
jQuery.each(data, function(key, value) {
    jQuery('#woocommerce_econt_shipping_method_office_office').append(jQuery("<option/>", {
        value: value.id,
        text: value.value
    }));
    
});
jQuery('#woocommerce_econt_shipping_method_office_office').on('change, click', function() {
jQuery('#woocommerce_econt_shipping_method_office_code').val(this.value);

});


}
});



},


});
//econt admin settings office autocomplete




//econt admin settings to APS autocomplete
jQuery( "#woocommerce_econt_shipping_method_machine_town" ).autocomplete({
minLength: 2,
source: function( request, response ) {
jQuery.ajax({
url: ajaxurl,
dataType: "json",
data: {

action:'econt_handle_ajax', 
city: request.term

},
success: function( data ) {
  response(jQuery.map(data, function(item) {
                        return {
                            label:      item.label,
                            value:      item.value,
                            city_id:    item.id,
                            post_code:   item.post_code
                            
                       };
                }));

},


});
},



select: function( event, ui ) {
   
var city_id = ui.item.city_id;
var post_code = ui.item.post_code;


jQuery('#woocommerce_econt_shipping_method_machine_postcode').val(post_code);

jQuery.ajax({
url: ajaxurl,
dataType: "json",
data: {

action:'econt_handle_ajax', 
machine_city_id: city_id 

},
success: function( data ) {

 jQuery('#woocommerce_econt_shipping_method_machine_machine').empty()

//selectValues = { "1": "test 1", "2": "test 2" };
jQuery.each(data, function(key, value) {
    jQuery('#woocommerce_econt_shipping_method_machine_machine').append(jQuery("<option/>", {
        value: value.id,
        text: value.value
    }));
    
});
jQuery('#woocommerce_econt_shipping_method_machine_machine').on('change, click', function() {
jQuery('#woocommerce_econt_shipping_method_machine_code').val(this.value);

});


}
});



},


});
//end of econt admin settings to APS autocomplete




//econt checkout and order to/from door autocomplete
jQuery( "#econt_door_town" ).autocomplete({
minLength: 2,
source: function( request, response ) {
jQuery.ajax({
url: ajaxurl,
dataType: "json",
data: {

action:'econt_handle_ajax', 
//city: request.term
city: request.term

},
success: function( data ) {
//response( data );
  response(jQuery.map(data, function(item) {
                        return {
                            label:      item.label,
                            value:      item.value,
                            city_id:    item.id,
                            post_code:   item.post_code
                            
                       };
                }));

},


});

},


select: function( event, ui ) {
   
var city_id = ui.item.city_id;
var post_code = ui.item.post_code;
//var city_name = ui.item.label;
//alert(  sender_city_id );
//show express city courier if sender city = customer city
if( city_id == sender_city_id ){
jQuery("#econt_city_courier_field").slideToggle();
}else{
jQuery("#econt_city_courier_field").hide();
}

jQuery('#econt_door_postcode').val(post_code);

//calculate_loading(); //calculate loading cost for shipping to door

//show door fields after town is selected
jQuery('#econt_door_postcode, label[for="econt_door_postcode"], #econt_door_street, label[for="econt_door_street"], #econt_door_quarter, label[for="econt_door_quarter"], #econt_door_street_num, label[for="econt_door_street_num"], #econt_door_street_bl, label[for="econt_door_street_bl"], #econt_door_street_vh, label[for="econt_door_street_vh"], #econt_door_street_et, label[for="econt_door_street_et"], #econt_door_street_ap, label[for="econt_door_street_ap"], #econt_door_other, label[for="econt_door_other"], #econt_delivery_days, label[for="econt_delivery_days"]').show();

jQuery( "#econt_door_street" ).autocomplete({
minLength: 2,
source: function( request, response ) {
jQuery.ajax({
url: ajaxurl,
dataType: "json",
data: {

action:'econt_handle_ajax', 
//city: request.term
door_city_id: city_id,
door_street_name: request.term,
type: 'street'

},
success: function( data ) {
//response( data );
  response(jQuery.map(data, function(item) {
                        return {
                            label:      item.label,
                            value:      item.value,
                            //city_id:    item.id,
                            //post_code:   item.post_code
                            
                       };
                }));



},




});
//calculate_loading(); //calculate loading to door
},

});





jQuery( "#econt_door_quarter" ).autocomplete({
minLength: 2,
source: function( request, response ) {
jQuery.ajax({
url: ajaxurl,
dataType: "json",
data: {

action:'econt_handle_ajax', 
//city: request.term
door_city_id: city_id,
door_quarter_name: request.term,
type: 'quarter'

},
success: function( data ) {
//response( data );
  response(jQuery.map(data, function(item) {
                        return {
                            label:      item.label,
                            value:      item.value,
                            //city_id:    item.id,
                            //post_code:   item.post_code
                            
                       };
                }));

},


});
},

});


},


});



//hide and show fields in checkout and order

jQuery('#econt_shipping_to').on('change', function () {
    if(this.value == 'DOOR'){

jQuery('.econt_shipping_to_office').hide();
jQuery('.econt_shipping_to_machine').hide();
jQuery("#econt_office_locator").hide();

jQuery('#econt_door_town').removeAttr('value');
jQuery('#econt_door_postcode').removeAttr('value');
jQuery('#econt_door_street').removeAttr('value');
jQuery('#econt_door_quarter').removeAttr('value');
jQuery('#econt_door_street_num').removeAttr('value');
jQuery('#econt_door_street_bl').removeAttr('value');
jQuery('#econt_door_street_vh').removeAttr('value');
jQuery('#econt_door_street_et').removeAttr('value');
jQuery('#econt_door_street_ap').removeAttr('value');
jQuery('#econt_door_other').removeAttr('value');


jQuery('.econt_shipping_to_door').slideToggle();
jQuery('#econt_door_postcode, label[for="econt_door_postcode"], #econt_door_street, label[for="econt_door_street"], #econt_door_quarter, label[for="econt_door_quarter"], #econt_door_street_num, label[for="econt_door_street_num"], #econt_door_street_bl, label[for="econt_door_street_bl"], #econt_door_street_vh, label[for="econt_door_street_vh"], #econt_door_street_et, label[for="econt_door_street_et"], #econt_door_street_ap, label[for="econt_door_street_ap"], #econt_door_other, label[for="econt_door_other"], #econt_delivery_days, label[for="econt_delivery_days"]').hide();

}else if(this.value == 'OFFICE'){ 

jQuery('.econt_shipping_to_door').hide();
jQuery('.econt_shipping_to_machine').hide();
jQuery("#econt_city_courier_field").hide();

jQuery('#econt_offices_town').removeAttr('value');
jQuery('#econt_offices_postcode').removeAttr('value');
jQuery('#econt_offices').empty();

jQuery('.econt_shipping_to_office').slideToggle();
jQuery('#econt_offices_postcode, label[for="econt_offices_postcode"], #econt_offices, label[for="econt_offices"]').hide();

} else if(this.value == 'MACHINE'){

jQuery('.econt_shipping_to_door').hide();
jQuery('.econt_shipping_to_office').hide();
jQuery("#econt_city_courier_field").hide();
jQuery("#econt_office_locator").hide();

jQuery('#econt_machines_town').removeAttr('value');
jQuery('#econt_machines_postcode').removeAttr('value');
jQuery('#econt_machines').empty();

jQuery('.econt_shipping_to_machine').slideToggle();
jQuery('#econt_machines_postcode, label[for="econt_machines_postcode"], #econt_machines, label[for="econt_machines"]').hide();

} else if(this.value == 0) {

jQuery('.econt_shipping_to_door').hide();
jQuery('.econt_shipping_to_office').hide();
jQuery('.econt_shipping_to_machine').hide();
jQuery("#econt_city_courier_field").hide();
jQuery("#econt_office_locator").hide();

}

jQuery('#button_calculate_loading').val(econt_php_vars.calculateShippingCostText);

});

//admin order details show only the needed field when to/from APS
var receiver_shipping_to = jQuery("#receiver_shipping_to").val();
var sender_door_or_office = jQuery("#sender_door_or_office").val();
if( receiver_shipping_to == 'MACHINE' || sender_door_or_office == 'MACHINE' ){

if( client_cd_agreement == 0 ){

    jQuery('#order_cd').val(0);
    jQuery('#row_order_cd').hide();
}

 jQuery('.not_used_to_aps').hide();
 jQuery('.used_from_aps').hide();
jQuery('.priority_time').hide();
 
 if ( sender_door_or_office == 'MACHINE' && receiver_shipping_to != 'MACHINE' ) {
 
 jQuery('.used_from_aps').show();
 
 if( receiver_shipping_to == 'DOOR' ){
   jQuery('.priority_time').show(); 
 }


 }


}

//hide size under 60cm if not office to office
if ( sender_door_or_office != 'OFFICE' || receiver_shipping_to != 'OFFICE' ) {
    jQuery('#size_under_60cm').hide();
}

jQuery('#sender_door_or_office').on('change', function () {
 if( (this.value == 'DOOR' && receiver_shipping_to != 'MACHINE') || (this.value == 'DOOR2' && receiver_shipping_to != 'MACHINE') || (this.value == 'OFFICE' && receiver_shipping_to != 'MACHINE') ){

jQuery('#order_cd').removeAttr('disabled');
 jQuery('.not_used_to_aps').show();
 jQuery('.used_from_aps').show();
 jQuery('.priority_time').show();
 jQuery('#row_order_cd').show();


 } else if(this.value == 'MACHINE' || receiver_shipping_to == 'MACHINE') {

if( client_cd_agreement == 0 ){

jQuery('#order_cd').val(0);
jQuery('#row_order_cd').hide();
}

 jQuery('.not_used_to_aps').hide();
 jQuery('.used_from_aps').hide();
 jQuery('.priority_time').hide();

 if ( this.value == 'MACHINE' && receiver_shipping_to != 'MACHINE' ) {
 
 jQuery('.used_from_aps').show();
 
 if( receiver_shipping_to == 'DOOR' ){
   Query('.priority_time').show(); 
 }


 }
}
//hide size under 60cm if not office to office
if(this.value != 'OFFICE'){
  jQuery('#size_under_60cm').hide();   
}

if(this.value == 'OFFICE' && receiver_shipping_to == 'OFFICE'){
  jQuery('#size_under_60cm').show();
}

});

var woocommerce_econt_shipping_method_send_from = jQuery('#woocommerce_econt_shipping_method_send_from').val();

jQuery('#woocommerce_econt_shipping_method_send_from').on('change', function () {
if(this.value == 'MACHINE'){
if(jQuery('#woocommerce_econt_shipping_method_cd').val() == 1 && jQuery('#woocommerce_econt_shipping_method_client_cd_num').val() == 0 ){

//alert('Когато изпращате от АПС и активирате услугата плащане при доставка (наложен платеж) е задължително да изпозвате споразумение за събиране на наложен платеж!');
alert(econt_php_vars.apsAlertText2);
jQuery('#woocommerce_econt_shipping_method_send_from').val( woocommerce_econt_shipping_method_send_from );


} else {
    //alert('услуги, които можете да използвате, когато изпращате от АПС са:\n- наложен платеж (когато използвате споразумение за събиране на НП) \n- обратна разписка\n- двупосочна пратка\n- Час за приоритет (когато изпращате до адрес)\n- Преглед\n- Преглед и тест\n- Преглед, тест и избор');
    alert(econt_php_vars.apsAlertText2);
} 

}

});

//jQuery("#woocommerce_econt_shipping_method_refreshdata").prop('value', 'Обнови');
jQuery("#woocommerce_econt_shipping_method_refreshdata").prop('value', econt_php_vars.refreshText);
//econt admin settings refresh econt offices and adresses
jQuery('#woocommerce_econt_shipping_method_refreshdata').click(function(){

    var username  = jQuery("#woocommerce_econt_shipping_method_username").val();
    var password  = jQuery("#woocommerce_econt_shipping_method_password").val();
    var live      = jQuery("#woocommerce_econt_shipping_method_live").val();


jQuery("#woocommerce_econt_shipping_method_refreshdata").prop('value', econt_php_vars.refreshWaitText);
jQuery("#woocommerce_econt_shipping_method_refreshdata").addClass('ui-autocomplete-loading');
    jQuery.ajax({

        url: ajaxurl,
        dataType: "json",
        data:{
        action:'econt_handle_ajax',
        refresh_data: 1,
        username: username,
        password: password,
        live: live,
        }, 
        //type: 'post',


        success: function(data){
        jQuery("#woocommerce_econt_shipping_method_refreshdata").removeClass('ui-autocomplete-loading');
        jQuery("#woocommerce_econt_shipping_method_refreshdata").prop('value', data.msg);
   //console.log(data);
            if(data.error === 1){
                alert(data.msg);
            }
        },
    });


});
//end of econt admin settings refresh econt offices and adresses


//start of econt admin sync profile and clients_access
jQuery("#woocommerce_econt_shipping_method_refreshprofile").prop('value', econt_php_vars.refreshText);

//econt admin settings sync econt profile and clients_access
jQuery("#woocommerce_econt_shipping_method_refreshprofile").click(function(){

    var username  = jQuery("#woocommerce_econt_shipping_method_username").val();
    var password  = jQuery("#woocommerce_econt_shipping_method_password").val();
    var live      = jQuery("#woocommerce_econt_shipping_method_live").val();



jQuery("#woocommerce_econt_shipping_method_refreshprofile").prop('value', econt_php_vars.refreshWaitText);
jQuery("#woocommerce_econt_shipping_method_refreshprofile").addClass('ui-autocomplete-loading');
    jQuery.ajax({

        url: ajaxurl,
        dataType: "json",
        data:{
        action:'econt_handle_ajax',
        sync_profile: 1,
        username: username,
        password: password,
        live: live,
        }, 
        //type: 'post',


        success: function(data){
            console.log('refresh_profile data', data);
        jQuery("#woocommerce_econt_shipping_method_refreshprofile").removeClass('ui-autocomplete-loading');
        jQuery("#woocommerce_econt_shipping_method_refreshprofile").prop('value', data.msg);
   //console.log(data);
            //if(data.indexOf('Error') === -1){
            if(data.error === 0){
                location.reload();
            }else if(data.error === 1){
                alert(data.msg);
            }
        },
    });


});
//end of econt admin sync profile and clients_access


  var form = jQuery('#order_loading_form');

//admin order calculate or create loading
   jQuery("#order_only_calculate_loading, #place_order").click( function() { 

    var data2 = jQuery('#order_loading_form').serialize();
    jQuery('#econtLoader').show();

    jQuery.ajax({

        url: ajaxurl,
        dataType: "json",
        data: data2 + '&action=econt_handle_ajax&action2=only_calculate_loading',

        
        success: function(data){
            //console.log('calc_loading_response', data);
            jQuery('#econtLoader').hide();
            jQuery('#create_loading tr').remove();
            if(data.length !== 0){
                jQuery.each(data, function(key, val) {
                    if(key == 'econt_shipping_expenses'){
                jQuery('<tr><td>'+econt_php_vars.totalShippingCostText+'</td><td id="'+key+'"><strong>'+val['total_shipping_cost']+val['currency_symbol']+'</strong></td><tr>').appendTo('#create_loading');
                jQuery('<tr><td>'+econt_php_vars.totalShippingCustomerCostText+'</td><td id="'+key+'"><strong>'+val['customer_shipping_cost']+val['currency_symbol']+'</strong></td><tr>').appendTo('#create_loading');
                }else if (key == 'warning'){
                    alert( val );
                 }

                });
            } else {
              alert('Възникна грешка. Моля, проверете потребителското име и парола в настройките за доставка с Еконт.');  
            }

        },
    });
  });

      jQuery("#order_create_loading").click( function() { 

    var data2 = jQuery('#order_loading_form').serialize();
    jQuery('#econtLoader').show();

    jQuery.ajax({

        url: ajaxurl,
        dataType: "json",
        data: data2 + '&action=econt_handle_ajax&action2=create_loading',

        
        success: function(data){
        //console.log('create_loading_response', data);    
        jQuery('#econtLoader').hide();
        jQuery('#create_loading tr').remove();

            if( data['warning'] ){ 
            alert( data['warning'] ); 
            } else if(data.length === 0) {
            alert('Възникна грешка. Моля, проверете потребителското име и парола в настройките за доставка с Еконт.');
            } else { 

                jQuery('<tr><td>'+econt_php_vars.loadingPdfLinkText+'</td><td id="pdf_url"><strong><a href="'+data['pdf_url']+'" target="_blank">'+data['pdf_url']+'</a></strong></td><tr>').appendTo('#create_loading');
            
            
               jQuery('<tr><td>'+econt_php_vars.loadingNumberText+'</td><td id="loading_num"><strong>'+data['loading_num']+'</strong></td><tr>').appendTo('#create_loading');
             
                
                jQuery('<tr><td>'+econt_php_vars.totalShippingCostText+'</td><td id="total_sum"><strong>'+data['total_shipping_cost']+data['currency_symbol']+'</strong></td><tr>').appendTo('#create_loading');
             
                
                jQuery('<tr><td>'+econt_php_vars.totalShippingCustomerCostText+'</td><td id="order_total_sum"><strong>'+data['customer_shipping_cost']+data['currency_symbol']+'</strong></td><tr>').appendTo('#create_loading');
             

                location.reload();
            }
        },
    });
  });
//end of admin order calculate or create loading

jQuery("#econt_offices_town", "#woocommerce_econt_shipping_method_office_town", "#econt_door_town", "#econt_door_street", "#econt_door_quarter").attr("autocomplete","off");

//hide econtLoading div in checkout
jQuery('#econtLoader').hide();

function calculate_loading(loading){

if(typeof loading !== 'undefined'){    
    jQuery('#econtLoader').show();
    jQuery('input[type="submit"]').attr('disabled','disabled');
}

var econt_shipping_to = jQuery("#econt_shipping_to").val();

if(jQuery('#payment_method_cod').is(':checked')){ 
var payment_method_cod = 1; 
}else{ 
var payment_method_cod = 0; 
}
//console.log('cod: ' + payment_method_cod);

var pack_count = 1;

var receiver_name = jQuery("#billing_company").val();
var receiver_name_person = jQuery("#billing_first_name").val()+' '+jQuery("#billing_last_name").val();
var receiver_phone_num = jQuery("#billing_phone").val();
var receiver_email = jQuery("#billing_email").val();

if ( econt_shipping_to == 'DOOR' ){

var receiver_city =  jQuery("#econt_door_town").val();
var receiver_post_code = jQuery("#econt_door_postcode").val();
var receiver_street = jQuery("#econt_door_street").val();
var receiver_quarter = jQuery("#econt_door_quarter").val();
var receiver_street_num = jQuery("#econt_door_street_num").val();
var receiver_street_bl = jQuery("#econt_door_street_bl").val();
var receiver_street_vh = jQuery("#econt_door_street_vh").val();
var receiver_street_et = jQuery("#econt_door_street_et").val();
var receiver_street_ap = jQuery("#econt_door_street_ap").val();
var receiver_street_other = jQuery("#econt_door_other").val();
var econt_city_courier = jQuery("#econt_city_courier").val();
var delivery_day_id = jQuery("#econt_delivery_days").val();

var receiver_office_code = '';

}else if (econt_shipping_to == 'OFFICE'){

var receiver_city =  jQuery("#econt_offices_town").val();
var receiver_post_code = jQuery("#econt_offices_postcode").val();
var receiver_office_code = jQuery("#econt_offices").val();

var receiver_street = '';
var receiver_quarter = '';
var receiver_street_num = '';
var receiver_street_bl = '';
var receiver_street_vh = '';
var receiver_street_et = '';
var receiver_street_ap = '';
var receiver_street_other = '';
var econt_city_courier = '';
var delivery_day_id = '';

}else if (econt_shipping_to == 'MACHINE'){

var receiver_city =  jQuery("#econt_machines_town").val();
var receiver_post_code = jQuery("#econt_machines_postcode").val();
var receiver_office_code = jQuery("#econt_machines").val();

var receiver_street = '';
var receiver_quarter = '';
var receiver_street_num = '';
var receiver_street_bl = '';
var receiver_street_vh = '';
var receiver_street_et = '';
var receiver_street_ap = '';
var receiver_street_other = '';
var econt_city_courier = '';
var delivery_day_id = '';

}

jQuery.ajax({

        url: ajaxurl,
        dataType: "json",
        
        data: {
        action: 'econt_handle_ajax',
        action2: 'only_calculate_loading',
        receiver_name: receiver_name,
        receiver_name_person: receiver_name_person,
        receiver_phone_num: receiver_phone_num,
        receiver_email: receiver_email,
        receiver_shipping_to: econt_shipping_to,
        receiver_city: receiver_city,
        receiver_post_code: receiver_post_code,
        receiver_office_code: receiver_office_code,
        receiver_street: receiver_street,
        receiver_quarter: receiver_quarter,
        receiver_street_num: receiver_street_num,
        receiver_street_bl: receiver_street_bl,
        receiver_street_vh: receiver_street_vh,
        receiver_street_et: receiver_street_et,
        receiver_street_ap: receiver_street_ap,
        receiver_street_other: receiver_street_other,
        econt_city_courier: econt_city_courier,
        delivery_day_id: delivery_day_id,
        pack_count: pack_count,
        payment_method_cod: payment_method_cod,

        },

        success: function(data){

            jQuery.each(data, function(key, val) {
            if(key == 'econt_shipping_expenses'){
                

            jQuery("#button_calculate_loading").prop('value', econt_php_vars.shippingCostText + ' ' + val['customer_shipping_cost']);
            jQuery("#econt_customer_shipping_cost").attr('value', val['customer_shipping_cost'] );
            jQuery("#econt_total_shipping_cost").attr('value', val['total_shipping_cost'] );
            jQuery('#'+key).remove();
            
            if(econt_php_vars.incShippingCost == 1){
                //console.log('econt_php_vars.incShippingCost: ' + econt_php_vars.incShippingCost);
                jQuery( 'body' ).trigger( 'update_checkout' ); //inc shipping cost fee
            }
            if(econt_php_vars.incShippingCost == 0){
                //console.log('php_vars.incShippingCost else: ' + econt_php_vars.incShippingCost);
                jQuery('.woocommerce-checkout-review-order-table tr:last').after('<tr id="'+key+'"><td>'+econt_php_vars.shippingPriceText+'</td><td><strong>'+val['customer_shipping_cost']+val['currency_symbol']+'</strong></td><tr>').appendTo('.woocommerce-checkout-review-order-table');   
            }
                        
            }else if (key == 'warning'){
                alert( val );
             }

            });
            
            if(typeof loading !== 'undefined'){    
                jQuery('#econtLoader').hide();
                jQuery('input[type="submit"]').removeAttr('disabled');
            }

        },


 });

}

    //avtomatizirano kalkulirane na cenata za shipping i nalojen platej
    jQuery("#econt_door_other, #button_calculate_loading, #econt_door_street, #econt_door_quarter").on('click' , function(e){
       console.log('isCheckout1', econt_php_vars.isCheckout);
       if(econt_php_vars.isCheckout){ //if we are in checkout not in admin panel
        calculate_loading();
      }

    });

    jQuery("#econt_city_courier, #econt_delivery_days").on('change' , function(e){
      console.log('isCheckout2', econt_php_vars.isCheckout);
      if(econt_php_vars.isCheckout){ //if we are in checkout not in admin panel
        calculate_loading();
      }

    });
    
    //preizchislqva cenata za dostavka i NP pri smqna na payment method
    /*
    jQuery( '.payment_methods' ).live('change', function () {
      var loading = 'yes';
      calculate_loading(loading);
    });
    */
    jQuery(function(){
        jQuery( 'body' ).on( 'updated_checkout', function() {

            jQuery('input[name="payment_method"]').change(function(){
                console.log("payment method changed");
                var loading = 'yes';
                calculate_loading(loading);
            });
        });
    });


    //reload na checkout page i nulirane na Econt shipping fee pri smqna na shipping method v chechout
    /*
    jQuery( '.shipping_method' ).on('change', function () {
    
      jQuery('#econtLoader').show();
    
      jQuery.ajax({

          url: ajaxurl,
          dataType: "json",
          data:{
          action:'econt_handle_ajax',
          action2: 'shipping_method_change',
          }, 

          success: function(data){
          
          setTimeout(function(){
              
              location.reload();
          }, 4000);  

          },
      });
        
    });
    */
    //reload na checkout page i nulirane na Econt shipping fee pri smqna na shipping method v chechout
    jQuery('form.checkout').on('change','input[name^="shipping_method"]',function() {
        var value = jQuery(this).val();
        console.log('on change input[name^="shipping_method"]');
        jQuery('#econtLoader').show();
       
        jQuery.ajax({

            url: ajaxurl,
            dataType: "json",
            data:{
            action:'econt_handle_ajax',
            action2: 'shipping_method_change',
            chosen_shipping_method: value,
            },

            success: function(data){
           
            //setTimeout(function(){
               
                location.reload();
            //}, 2000); 

            },
        });
    });


jQuery("#delete_loading").click(function(e){

var loading_num = jQuery("#loading_num").val();
jQuery('#econtLoader').show();

jQuery.ajax({

        url: ajaxurl,
        dataType: "json",
        
        data: {
        action: 'econt_handle_ajax',
        action2: 'delete_loading',
        loading_num: loading_num,

        },

        success: function(data){
        jQuery('#econtLoader').hide();
        location.reload(); 

        },


 });

 });


jQuery("#button_request_of_courier").click(function(e){

window.open('http://ee.econt.com/?target=EeRequestOfCourier&eshop=1', '_blank');

});

//zabranqva promqnata na ofis kod i poshtenski ofis kod v admin nastrojkite na plugina
jQuery('#woocommerce_econt_shipping_method_office_postcode, #woocommerce_econt_shipping_method_office_code, #woocommerce_econt_shipping_method_machine_postcode, #woocommerce_econt_shipping_method_machine_code').prop('readonly', true);

//zatvarq sekciqta "customer fileds" v poruchkata
jQuery('#postcustom').addClass('closed');




function edit_receiver_address(order_id){

var econt_shipping_to = jQuery("#econt_shipping_to").val();
//DOOR
var econt_door_town =  jQuery("#econt_door_town").val();
var econt_door_postcode = jQuery("#econt_door_postcode").val();
var econt_door_street = jQuery("#econt_door_street").val();
var econt_door_quarter = jQuery("#econt_door_quarter").val();
var econt_door_street_num = jQuery("#econt_door_street_num").val();
var econt_door_street_bl = jQuery("#econt_door_street_bl").val();
var econt_door_street_vh = jQuery("#econt_door_street_vh").val();
var econt_door_street_et = jQuery("#econt_door_street_et").val();
var econt_door_street_ap = jQuery("#econt_door_street_ap").val();
var econt_door_other = jQuery("#econt_door_other").val();
//OFFICE
var econt_offices_town =  jQuery("#econt_offices_town").val();
var econt_offices_postcode = jQuery("#econt_offices_postcode").val();
var econt_offices = jQuery("#econt_offices").val();
//MACHINE
var econt_machines_town =  jQuery("#econt_machines_town").val();
var econt_machines_postcode = jQuery("#econt_machines_postcode").val();
var econt_machines = jQuery("#econt_machines").val();

jQuery.ajax({

        url: ajaxurl,
        dataType: "json",
        
        data: {
        action: 'econt_handle_ajax',
        action2: 'edit_receiver_address',
        econt_shipping_to: econt_shipping_to,
        econt_door_town: econt_door_town,
        econt_door_postcode: econt_door_postcode,
        econt_door_street: econt_door_street,
        econt_door_quarter: econt_door_quarter,
        econt_door_street_num: econt_door_street_num,
        econt_door_street_bl: econt_door_street_bl,
        econt_door_street_vh: econt_door_street_vh,
        econt_door_street_et: econt_door_street_et,
        econt_door_street_ap: econt_door_street_ap,
        econt_door_other: econt_door_other,
        econt_offices_town: econt_offices_town,
        econt_offices_postcode: econt_offices_postcode,
        econt_offices: econt_offices,
        econt_machines_town: econt_machines_town,
        econt_machines_postcode: econt_machines_postcode,
        econt_machines: econt_machines,
        order_id: order_id,

        },

        success: function(data){

          if( data['warning'] ){ 
            alert( data['warning'] ); 
          }else{
            location.reload();
          }

        },


 });

}

jQuery("#save_receiver_address").click(function(e){

  edit_receiver_address(order_id);

});

jQuery("a.edit_econt_address").click(function(e){
  e.preventDefault();
  if(loading_is_imported != '1'){ //if loading is imported prevent from editing receiver address
    jQuery('.econt-table').hide();
    jQuery('#econt_edit_receiver_address').show();
  }
});


    var env = jQuery('select[name=woocommerce_econt_shipping_method_live]').val();

    var set_env = function(env){
      
      //var html_live = 'По подразбиране е зададена Реaлна среда за работа, въведете си:<br>-Потребителско име за достъп до e-Econt;<br>-Парола за достъп до e-Econt;<br>-Кликнете на бутона "Обновете информацията", и изчакайте докато се изтегли нужната информация от сървърите на Еконт и се появят нови полета с различни настройки на модула;';
      //var html_test = 'Тестова среда (Ако се избере Тестова среда, всички заявки ще се пращат към тестовата система на ЕКОНТ);<br>ВНИМАНИЕ: Потребителските имена и пароли за двете системи са различни, затова ако не разполагате с потребителско име и парола за тестовата система – изберете „Реална“ от падащото меню.';
      var html_live = econt_php_vars.htmlLiveText;
      var html_test = econt_php_vars.htmlTestText;

      if( env == 1){

        jQuery("#woocommerce_econt_shipping_method_live_description").html(html_live);

      }else if (env == 0){

        jQuery("#woocommerce_econt_shipping_method_live_description").html(html_test);

      }

    }

    set_env(env);


    jQuery("#woocommerce_econt_shipping_method_live").change(function(){

      var env = jQuery('select[name=woocommerce_econt_shipping_method_live]').val();

      set_env(env);

    });


    //jQuery("form[name='checkout']").attr("autocomplete", "off");
    
    jQuery.each([ 'econt_offices_town', 'econt_door_town', 'econt_door_street', 'econt_door_quarter' ], function( index, value ){  
        //jQuery("input[name='"+ value +"']").attr("autocomplete", "new-username");
        jQuery("input[name='"+ value +"']").attr("autocomplete", "off"); 
    });
/*
 //make post code fileds in Checkout readonly 
 jQuery.each([ 'econt_offices_postcode', 'econt_door_postcode', 'econt_machines_postcode' ], function( index, value ){  
        jQuery('#' + value).attr('readonly', 'readonly');
    });
*/

  function receiveMessage(event) {
    if (event.origin !== econt_php_vars.office_locator_domain)
      return;

    message_array = event.data.split('||');
    jQuery("#econt_offices").val(message_array[0]).trigger('change');
    jQuery.colorbox.close();
  }

  if (window.addEventListener) {
    window.addEventListener('message', receiveMessage, false);
  } else if (window.attachEvent) {
    window.attachEvent('onmessage', receiveMessage);
  }

  if (jQuery('#econt_offices_town').val()) {
    url = econt_php_vars.office_locator + '&address=' + jQuery('#econt_offices_town').val();
  } else {
    url = econt_php_vars.office_locator;
  }
    
  jQuery('#econt_offices_town').change(function () {

    if (jQuery('#econt_offices_town').val()) {
      url = econt_php_vars.office_locator + '&address=' + jQuery('#econt_offices_town').val();
    } else {
      url = econt_php_vars.office_locator;
    }

  });

  jQuery( '#econt_office_locator' ).on('click', function(e) {
  console.log('#econt_office_locator clicked');
    jQuery.colorbox({
      overlayClose: true,
      href : url,
      iframe : true,
      opacity: 0.5,
      width  : '99%',
      height : '99%'
    });
    jQuery(window).resize(function(){
      jQuery.colorbox.resize({
        width: '99%',
        height: '99%'
      });
    });
  });

});
