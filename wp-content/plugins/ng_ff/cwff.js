jQuery(document).ready(
    function () {
        jQuery('body').on(
            'DOMNodeInserted', '.cw_ffform', function () {
                cwforminit(this);
            }
        );
    }
);
jQuery('.cw_ffform').each(
    function () {
        cwforminit(this);
    }
);
function cwforminit(form)
{
    let $form = jQuery(form);
    if ($form.hasClass('cwforminit')) { return;}
    $form.addClass('cwforminit');
    let $formid = ($form.data('cwform'));
    jQuery.ajax(
        {
            url: cw_ff.ajaxurl,
            type: 'POST',
            async: true,
            data: {
                action: 'cwfft'
            },
            success: function (out) {
                window.cwfft = out;

            }
        }
    );
    $form.find('input,select, textarea').keyup(
        function () {
            cwvalidate(jQuery(this));
        }
    );

    $form.find('[data-cwfilter]').each(
        function (ee) {
            jQuery(this).keypress(
                function (e) {
                    var char = String.fromCharCode(e.which);
                    var txt = jQuery(this).val();
                    var filter = '' + jQuery(this).data('cwfilter') + '';
                    var regex = new RegExp(filter);
                    var now = txt + char;
                    if (!regex.test(now)) {
                        e.preventDefault();
                        return false;
                    }
                }
            );

        }
    );
    jQuery($form).find('[data-cwvalidate]').change(
        function () {
            cwvalidate(jQuery(this));
        }
    );
    $form.submit(
        function (e) {
            let $thisform=jQuery(this);
            let reply = cwformcollectdata(this);
            reply.append('action', 'cw_ff');
            reply.append('cwfft', window.cwfft);
            $thisform.find('[data-cwmessage]').each(
                function () {
                    cwvalidate(jQuery(this));
                }
            );
            e.preventDefault();
            $thisform.addClass('cwprocessing');
            jQuery.ajax(
                {
                    url: cw_ff.ajaxurl,
                    type: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: reply,
                    success: function (out) {
                        $thisform.removeClass('cwprocessing');
                        if (!out.success) {
                            if (out.data.type === 'field') {
                                $thisform.find('[name="' + out.data.details.cwname + '"]').addClass('error');
                            } else {
                                alert(out.data.details);
                            }
                        } else {
                            //$thisform.find('*').css({visibility: 'hidden'});
                            $thisform.addClass('ngsubmitted');
                            $thisform.addClass('ngsubmitted'+$formid);
                            let formdata=false;
                            if ($thisform.is('[data-cwform1]')) {
                                formdata=($thisform.data('cwform1'));
                            }
                            if (formdata) {
                                        $thisform.before(formdata['success']);
                            }
                            jQuery().get($thisform.attr('action'));
                            jQuery('body').trigger('ngsubmitted');
                            jQuery('body').trigger('ngsubmitted'+$formid);
                            if (typeof cwffsuccess === 'function') {
                                cwffsuccess($thisform);
                            }
                        }
                    }
                }
            );
        }
    );
}
function cwformcollectdata(formz)
{
    let data = new FormData();
    let $forms = jQuery(formz);
    data.append('cwform', $forms.data('cwform'));
    $forms.find('input,textarea').each(
        function () {
            let $this = jQuery(this);
            if ($this.is('[type="checkbox"]')) {
                if ($this.is(':checked')) {
                    data.append($this.attr('name'), $this.val());
                }
            } else {
                data.append($this.attr('name'), $this.val());
            }
        }
    );
    $forms.find('select').each(
        function () {
            let $this = jQuery(this);
            data.append($this.attr('name'), $this.find('option:selected').text());
        }
    );
    $forms.find('input[type="file"]').each(
        function () {

            let $this = jQuery(this);
            if ($this.val() != '') {
                data.append($this.attr('name'), $this[0].files[0]);
            }
        }
    );

    return data;
}
function cwvalidate($input)
{
    $input.removeClass('error');
    $input.removeAttr('title');
    let input = $input.val();

    if (input === '') {
        return true;
    }
    var regex = new RegExp($input.data('cwvalidate'));
    if (!regex.test(input)) {
        $input.addClass('error');
        if ($input.data('cwmessage')) {
            if ($input.data('cwmessage')) {

                $input[0].setCustomValidity($input.data('cwmessage'));
            }
            return true;
        }
    } else {
        if ($input.data('cwmessage')) {
            $input[0].setCustomValidity("");

        }
        return false;
    }

}
