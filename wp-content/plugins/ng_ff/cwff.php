<?php
    /**
     * Plugin Name: nikitaGlobal custom feedback form
     * Plugin URI: http://nikita.global
     * Description: Allows to use custom form code.
     * Just include file with your <form></form> with shortcode
     * Author: Nikita Menshutin
     * Version: 3.7
     * Author URI: http://nikita.global
     *
     * PHP version 5.5.9
     *
     * @category NikitaGlobal
     * @package  NikitaGlobal
     * @author   Nikita Menshutin <nikita@nikita.global>
     * @license  http://nikita.global commercial
     * @link     http://nikita.global
     * */
    defined('ABSPATH') or die("No script kiddies please!");
    
    /**
     * Our core class goes here
     *
     * @category NikitaGlobal
     * @package  NikitaGlobal
     * @author   Nikita Menshutin <nikita@nikita.global>
     * @license  http://nikita.global commercial
     * @link     http://nikita.global
     * */
class NGCore
{
        
    /**
         * Constructor
         */
    function __construct()
    {
        $this->prefix = NG::prefix();
        if (! session_id()) {
            session_start();
        }
        $this->ver         = NG::version();
        $this->tags        = NG::tags();
        $this->url         = $this->getUrl();
        $this->life        = HOUR_IN_SECONDS * 12;
        $this->period      = MINUTE_IN_SECONDS * 5;
        $this->endPointUrl = get_site_url() . '/' . $this->prefix;
        $this->postType=NG::postType();
        $this->settingsSection=$this->prefix . 'section';
        add_action('init', array($this, 'initSession'));
        add_action('init', array($this, 'initSubmissions'));
        add_action('init', array($this, 'sendMailsDelayed'));
        add_shortcode($this->prefix, array($this, 'printform'));
        add_action('wp_enqueue_scripts', array($this, 'scripts'));
        add_action('wp_ajax_' . $this->prefix, array($this, 'ajax'));
        add_action('wp_ajax_nopriv_' . $this->prefix, array($this, 'ajax'));
        add_action('wp_ajax_cwfft', array($this, 'getToken'));
        add_action('wp_ajax_nopriv_cwfft', array($this, 'getToken'));
        add_action('customize_register', array($this, 'customizeSettings'));
        add_filter('cwffrecipients', array($this, 'sendMailTo'));
        add_filter('wp_mail_from_name', array($this, 'setMailFrom'));
        $this->_initMetabox();
    }
        
    /**
         * Enqueing scripts to footer
         *
         * @return void
         */
    public function scripts()
    {
        wp_register_script(
            $this->prefix,
            plugin_dir_url(__FILE__) . 'cwff.js',
            array('jquery'),
            $this->ver,
            true
        );
        wp_localize_script(
            $this->prefix,
            $this->prefix,
            array(
            'ajaxurl' => str_replace(
                array(
                    'http://',
                    'https://'
                ),
                '//',
                admin_url('admin-ajax.php')
            )
            )
        );
        wp_enqueue_script($this->prefix);
    }
        
    /**
         * Registering custom post types
         *
         * @return void
         */
    public function initSubmissions()
    {
        $labels = array(
        'name'               => __('Submissions'),
        'singular_name'      => __('Submission'),
        'add_new'            => __('Add submission'),
        'all_items'          => __('All submissions'),
        'add_new_item'       => __('Add submission'),
        'edit_item'          => __('Edit submission'),
        'new_item'           => __('New submission'),
        'view_item'          => __('View submission'),
        'search_items'       => __('Search submission'),
        'not_found'          => __('No submissions found'),
        'not_found_in_trash' => __('No submissions found in trash'),
        'parent_item_colon'  => __('Parent submission')
        );
        $args   = array(
        'labels'              => $labels,
        'public'              => false,
        'show_ui'             => true,
        'has_archive'         => false,
        'show_in_menu'        => true,
        'publicly_queryable'  => false,
        'query_var'           => true,
        'exclude_from_search' => true,
        'rewrite'             => false,
        'capability_type'     => 'post',
        'capabilities'        => array(
            'create_posts' => 'do_not_allow',
        ),
        'map_meta_cap'        => true,
        'hierarchical'        => false,
        'supports'            => array(
            'title',
            'editor'
        )
        );
        register_post_type($this->postType, $args);
    }
        
    /**
         * Get token
         *
         * @return void
         */
    public function getToken()
    {
        $token = array_values(array_slice($this->tokens, -1));
        echo $token[0];
        wp_die();
    }
        
    /**
         * Recipients
         *
         * @param array $to addresses
         *
         * @return array recipients
         */
    public function sendMailTo($to = array())
    {
        $option = get_theme_mod($this->prefix . 'mail');
        if (! $option || strlen($option) == 0) {
            return $to;
        }
        $option    = str_replace(' ', '', $option);
        $addresses = explode(',', $option);
        return array_merge($to, $addresses);
    }
    
    /**
     * Setting from name for wp_mail
     *
     * @param string $from name
     *
     * @return string from name
     */
    public function setMailFrom($from)
    {
        $name = get_theme_mod($this->prefix . 'from');
    
        if (! $name || strlen($name) == 0) {
            return $from;
        }
        return $name;
    }
        
    /**
         * Register settings in backend
         *
         * @param object $wp_customize settings
         *
         * @return void
         */
    public function customizeSettings($wp_customize)
    {
        $wp_customize->add_section(
            $this->settingsSection, array(
            'title'    => __('NG FF Mail Settings', $this->prefix),
            'priority' => 500
            )
        );
        $wp_customize->add_setting(
            $this->prefix . 'mail',
            array('capability' => 'edit_theme_options')
        );
        $wp_customize->add_setting(
            $this->prefix . 'from',
            array('capability' => 'edit_theme_options')
        );
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize,
                $this->prefix.'mail',
                array(
                'label'    => __('Form mail recipients', $this->prefix),
                'description' => __(
                    'Please fill in with comma separated list', $this->prefix
                ),
                'settings' => $this->prefix.'mail',
                'priority' => 10,
                'section'  => $this->settingsSection,
                'type'     => 'option',
                )
            )
        );
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize,
                $this->prefix . 'from',
                array(
                'label'       => __('Mail from name', $this->prefix),
                'description' => __('Please fill in name', $this->prefix),
                'settings'    => $this->prefix . 'from',
                'priority'    => 20,
                'section'     => $this->settingsSection,
                'type'        => 'option',
                )
            )
        );
    }
    
    /**
         * Method for ajax
         *
         * @return void (dies)
         */
    public function ajax()
    {
        $formdata      = $_POST[$this->prefix]['formdata'];
        $this->formurl = $_POST[$this->prefix]['url'];
        $this->form    = get_transient($formdata);
        $nonce         = $_POST[$this->prefix]['protect'];
        if (! $this->verify($nonce, $formdata)) {
            wp_die();
        }
        $token      = ($_POST['cwfft']);
        $validtoken = false;
        foreach ($this->tokens as $v) {
            if ($v == $token) {
                $validtoken = true;
            }
        }
        if (! $validtoken) {
            wp_die();
        }
        foreach ($this->form as $k => $v) {
            $this->form[$k]['value']='';
            if (isset($_POST[$v['cwname']])) {
                $this->form[$k]['value'] = $_POST[$v['cwname']];
            }
        }
        if (isset($_FILES) && ! empty($_FILES)) {
            $this->formfiles = array();
            foreach ($_FILES as $k => $v) {
                if ($v['error'] != 0) {
                    $msg = array('type' => 'file', 'details' => 'File error');
                    wp_send_json_error($msg);
                }
                $this->formfiles[$k] = $v;
            }
        }
        $this->formValidate();
        $this->saveSubmission();
        wp_die();
    }
        
    /**
         * Validating form, calling submethods if required
         *
         * @return void
         */
    public function formValidate()
    {
        foreach ($this->form as $value) {
            //required
            if (isset($value['required']) && ! strlen($value['value'])) {
                $msg = array('type' => 'field', 'details' => $value);
                wp_send_json_error($msg);
            }
            if (! strlen($value['value'])) {
                continue;
            }
            //regex
            $method = 'formValidate' . ucfirst($value['type']);
            if (method_exists($this, $method) && strlen($value['type'])) {
                $this->{$method}($value);
            }
            if (isset($value['data-cwvalidate'])) {
                $this->formValidateRegex($value);
            }
        }
    }
        
    /**
         * Validating field value with regex
         *
         * @param array $value field data
         *
         * @return boolean valid or not (dies)
         */
    public function formValidateRegex($value)
    {
        if (! preg_match('/' . $value['data-cwvalidate'] . '/', $value['value'])) {
            $msg = array('type' => 'field', 'details' => $value);
            wp_send_json_error($msg);
                
            return false;
        }
            
        return true;
    }
        
    /**
         * Validate email
         *
         * @param array $value field data
         *
         * @return boolean
         */
    public function formValidateEmail($value)
    {
        if (! filter_var($value['value'], FILTER_VALIDATE_EMAIL)) {
            $msg = array('type' => 'field', 'details' => $value);
            wp_send_json_error($msg);
                
            return false;
        }
            
        return true;
    }
        
    /**
         * Validating hash (made with protect method)
         *
         * @param string $code   hash
         * @param string $action text which should correspond to hash
         *
         * @return boolean valid or not
         */
    public function verify($code, $action)
    {
        if ($this->protect($action) == $code) {
            return true;
        }
            
        return false;
    }
        
    /**
         * Protect method
         *
         * @param string $data to protect
         *
         * @return string hash
         */
    public function protect($data)
    {
        return md5($data);
    }
        
    /**
         * Shortcode to print form
         *
         * @param type $atts array atts
         * @param type $form string form name
         *
         * @return string printed form
         */
    public function printform($atts, $form)
    {
        $this->hasform = true;
        $xmlprefix     = '<?xml encoding="utf-8" ?>';
        $formid        = crc32(time());
        $html=$this->_getForm($form);
        if (! strlen($html)) {
            return $form . ' ' . __('not found');
        }
        $doc = new DOMDocument('1.0', 'UTF-8');
        @$doc->loadHTML(
            $xmlprefix . $html//,
            //            LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
        );
        $nodes_   = array();
        $node     = array();
        $form     = $doc->getElementsByTagName('form')->item(0);
        $formname = $form->getAttribute('name');
        $form->setAttribute('name', 'cw' . sanitize_title($formname));
        $formclass = ($form->getAttribute('class')) . ' ' . $this->prefix . 'form';
        $form->setAttribute('class', $formclass);
        $form->setAttribute('data-cwform', $formid);
        $form->setAttribute('method', 'get');
        $form->setAttribute('action', $this->endPointUrl);
        foreach ($this->tags as $tag) {
            $nodes_[] = $doc->getElementsByTagName($tag);
        }
        foreach ($nodes_ as $k => $v) {
            if (empty($v)) {
                continue;
            }
            foreach ($v as $node_) {
                foreach ($node_->attributes as $attr) {
                    $name                         = $node_->getAttribute('name');
                    $node[$name][$attr->nodeName] = $attr->nodeValue;
                }
                $node[$name]['cwname'] = $this->sanitize($name);
                $node_->setAttribute('name', $this->sanitize($name));
            }
        }
        $formsign                    = md5($formname . $formid . $this->url);
        $node[$this->prefix]['name'] = $formname;
        set_transient($formsign, $node, HOUR_IN_SECONDS * 12);
        $el = array(
        'formdata' => $formsign,
        'protect'  => $this->protect($formsign),
        'url'      => $this->url,
        );
        foreach ($el as $k => $v) {
            $newnode = $doc->createElement('input');
            $newnode->setAttribute('type', 'hidden');
            $newnode->setAttribute('name', $this->prefix . '[' . $k . ']');
            $newnode->setAttribute('value', htmlentities($v));
            $form->appendChild($newnode);
        }
        $node = array();
        foreach ($doc->getElementsByTagName('label') as $node_) {
            if (empty($v)) {
                continue;
            }
            foreach ($node_->attributes as $attr) {
                $id = $node_->getAttribute('for');
                if ($id) {
                    continue;
                }
                $name = $node_->getAttribute('for');
                if (! $name || strlen($name) == 0) {
                    continue;
                }
                $node[$name][$attr->nodeName] = $attr->nodeValue;
                $node_->setAttribute('for', $this->sanitize($name));
            }
        }
            
        return html_entity_decode(
            str_replace(
                $xmlprefix,
                '',
                preg_replace(
                    '~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i',
                    '',
                    $doc->saveHTML()
                )
            )
        );
    }
    
    /**
     * Get form from file or
     * template part
     *
     * @param string $form file name or template part
     *
     * @return string
     */
    private function _getForm($form)
    {
        ob_start();
        if (file_exists($form)) {
            include $form;
        } else {
            get_template_part($form);
        }
        return ob_get_clean();
    }
    
    
    /**
         * Init Session
         *
         * @return void
         */
    public function initSession()
    {
        $_SESSION[$this->prefix][time()] = md5(time() . $this->prefix . '12');
        foreach ($_SESSION[$this->prefix] as $k => $v) {
            if ((time() - (int)$k) > $this->life) {
                unset($_SESSION[$this->prefix][$k]);
            }
        }
        $this->tokens = ($_SESSION[$this->prefix]);
        session_write_close();
        $this->notify[] = get_option('admin_email');
        $this->notify   = apply_filters('cwffrecipients', $this->notify);
    }
        
    /**
         * Echo file contents
         *
         * @param string $file file
         *
         * @return void
         **/
    private function _giveFileContents($file)
    {
        $this->attach['name']     = '';
        $this->attach['checksum'] = '';
        $dir                      = $this->uploadDir(wp_upload_dir());
        $basedir=$dir['basedir'];
        if (! file_exists($basedir . $file)) {
            return false;
        }
        header("Content-Type: application/octet-stream");
        header("Content-Transfer-Encoding: Binary");
        header(
            "Content-disposition: attachment; filename=\"" .
            basename($file) .
            "\""
        );
        echo readfile($basedir . $file);
        die();
    }
        
    /**
         * Check rights validate md5 and let download formfiles
         *
         * @return bool
         **/
    private function _giveFile()
    {
        if (! current_user_can('manage_options')) {
            auth_redirect();
                
            return;
        }
        if ($_GET['c'] != md5($this->prefix . $_GET['a'] . $_GET['b'])) {
            return;
        }
        $file = $this->_createUploadDir() . '/' . $_GET['b'] . '.php';
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $_GET['a']);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        echo file_get_contents($file);
        die();
    }
        
    /**
         * Send emails after expiration
         * $this->period
         *
         * @return void
         */
    public function sendMailsDelayed()
    {
        if (! get_transient($this->prefix . 'send')) {
            $this->sendMails();
            set_transient($this->prefix . 'send', true, 30);
        }
    }
        
    /**
         * Send mails (called via endpoint)
         *
         * @return void dies
         */
    public function sendMails()
    {
        if (isset($_GET['file']) && isset($_GET['check'])
            && (md5(
                crc32(
                    ($_GET['file']
                                ) . $this->prefix
                )
            ) == $_GET['check']) 
        ) {
            $this->_giveFileContents($_GET['file']);
            
                
        }
        if (isset($_GET['a'])
            && isset($_GET['b'])
            && isset($_GET['c'])
        ) {
            $this->fileData = $_GET;
            $this->_giveFile();
        }
        $args = array(
        'post_type'      => $this->postType,
        'posts_per_page' => 5,
        //   'orderby' => 'rand',
        'meta_query'     => array(
            array(
                'key'     => $this->prefix . 'sent',
                'compare' => '==',
                'value'   => '0'
            ),
        )
        );
            
            
        $posts_ = get_posts($args);
            
            
        if (empty($posts_)) {
            return;
        }
        add_filter(
            'wp_mail_content_type',
            function ( $title ) {
                return "text/html;charset=utf-8"; 
            }
        );
            
            
        if (! isset($this->notify)) {
            $this->notify = array();
        }
            
        apply_filters($this->prefix . 'pre_send', $posts_);
            
        do_action($this->prefix . 'pre_send', $posts_);
        foreach ($posts_ as $pp) {
            $attachments = get_post_meta(
                $pp->ID,
                $this->prefix .
                'attachments',
                true
            );
                
            foreach ($this->notify as $email) {
                $message    = $pp->post_content;
                $messagekey=$this->prefix.$this->sanitize($email);
                if (get_post_meta($pp->ID, $messagekey)) {
                    continue;
                }
                $mailresult = wp_mail(
                    $email,
                    $pp->post_title,
                    $message, false,
                    $attachments
                );
                if (! $mailresult
                ) {
                    $this->_submissionLog($pp->ID, __('Failed send to '.$email));
                    error_log(print_r($mailresult, true));
                    return false;
                }
                $this->_submissionLog($pp->ID, __('Sent to '.$email));
                update_post_meta($pp->ID, $messagekey, $email);
            }
            $this->_submissionLog($pp->ID, __('Sent to all'));
            update_post_meta($pp->ID, $this->prefix . 'sent', 1);
        }
    }
        
    /**
         * Save submitted data in CPT
         *
         * @return void
         */
    public function saveSubmission()
    {
        $checksum=$this->prefix.'checksum';
        $this->form['url'] = $this->formurl;
        $this->form        = apply_filters(
            $this->prefix .
            'beforesave',
            $this->form
        );
        unset($this->form['url']);
        $message = '';
        foreach ($this->form as $k => $v) {
            if ($k == $this->prefix) {
                continue;
            }
            if ($v['type'] == 'file') {
                continue;
            }
            if (strlen($v['value']) > 0) {
                $message .= $k . ' : ' . $v['value'] . "\n<hr/>\n";
            }
        }
        if (isset($this->formfiles) && ! empty($this->formfiles)) {
            $message .= $this->saveSubmissionFiles();
        }
        $message .= '<br><br>';
        $message.=$this->_userData();
        $message.=NG::makeAhref($this->formurl, $this->formurl);
        $post_ = array(
            'post_type'    => $this->postType,
            'post_content' => $message,
            'post_title'   => 'Form submission [' .
                              $this->form[$this->prefix]['name'] .
                              ']',
            'post_status'  => 'publish'
        );
        $md5   = md5(serialize($post_));
            
        $prev = get_posts(
            array(
                'post_type'   => $this->postType,
                'post_status' => 'any',
                'meta_query'  => array(
                    'relation' => 'AND',
                    array(
                        'key'     => $checksum,
                        'value'   => $md5,
                        'compare' => '='
                    )
                )
            )
        );
        if (! empty($prev)) {
            wp_send_json_success();
        }
        $id = wp_insert_post($post_);
        update_post_meta($id, $this->prefix . 'sent', '0');
        if (! $id) {
            $msg = array(
                'type'    => 'local',
                'details' => 'Проблема сохранения формы'
            );
            wp_send_json_error($msg);
        }
        if (isset($this->formfiles) && ! empty($this->formfiles)) {
            $attachments = array();
            foreach ($this->formfiles as $k => $v) {
                $attachments[] = $v['attach'];
            }
            update_post_meta(
                $id,
                $this->prefix . 'attachments',
                $attachments
            );
        }
        update_post_meta($id, $checksum, $md5);
        update_post_meta($id, $this->prefix . 'raw', $this->form);
            
        wp_send_json_success();
    }
    
    /**
     * If user is logged in
     * add his data for rom submission
     *
     * @return string user data
     */
    private function _userData()
    {
        if (!is_user_logged_in()) {
            return '';
        }
        $out='<br>';
        $current_user = wp_get_current_user();
        $link=get_edit_user_link($current_user->ID);
        $out.='user id #'.$current_user->ID.' <a href="'.$link.'">';
        $out.=$current_user->data->user_nicename;
        $out.='</a><br>';
        return $out;
    }
    
    /**
     * Metaboxes for delivery logs
     *
     * @return void
     */
    private function _initMetabox()
    {
        add_action('add_meta_boxes', array($this,'initMetabox'));
        
    }
    
    /**
     * One more method to add metabox
     *
     * @return void
     */
    public function initMetabox()
    {
            add_meta_box(
                $this->prefix . 'log',
                __('Delivery report'),
                array($this, 'metaboxShowLog'),
                $this->postType
            );
    }
    
    /**
     * Displaying logs
     *
     * @return void
     */
    public function metaboxShowLog()
    {
        
        $log=get_post_meta(get_the_id(), $this->prefix.'log', true);
        if (!$log || empty($log)) {
            return;
        }
        
        echo '<table style="width:100%;">';
        foreach ($log as $row) {
            echo '<tr>';
            foreach ($row as $col) {
                echo '<td style="border-bottom:'
                     .' 1px solid #ccc;padding:0;margin:0;">';
                echo $col;
                echo '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
    }
    
    /**
     * Saving logs to submission post
     *
     * @param int    $pid     post id
     * @param string $message message
     *
     * @return void
     */
    private function _submissionLog($pid, $message)
    {
        $log=array(
            'time'=>date('Y-m-d H:i:s'),
            'timestamp'=>time(),
            'message'=>$message
        );
        $meta=get_post_meta($pid, $this->prefix.'log', true);
        if (!$meta || !is_array($meta)) {
            $meta=array();
        }
        $meta[]=$log;
        update_post_meta($pid, $this->prefix.'log', $meta);
    }
    
    /**
         * Save file submissions
         *
         * @return string
         **/
    public function saveSubmissionFiles()
    {
        $out = '';
            
        $out .= __('Files') . ': ';
        $out .= '<br>';
            
        foreach ($this->formfiles as $k => $v) {
            $contents                        = file_get_contents(
                $v['tmp_name']
            );
            $this->formfiles[$k]['checksum'] = md5(
                $contents
            );
            $this->attach                    = $this->formfiles[$k];
            $dir                             = $this->uploaddir(wp_upload_dir());
            $base                            = $v['size'];
                
            (move_uploaded_file($v['tmp_name'], $dir['path'] . '/' . $v['name']));
            $file                          = $this->formfiles[$k]['checksum'] .
                                             '/'
                                             . $v['name'];
            $this->formfiles[$k]['attach'] = ($dir['path'] . '/' . $v['name']);
            $get                           = add_query_arg(
                array(
                    'file'  => $file,
                    'check' => md5(
                        crc32(
                            $file . $this->prefix
                        )
                    )
                ),
                $this->endPointUrl
            );
            $out.=NG::makeAhref(
                $get,
                $v['name']
                . ' '
                . $v['type']
                . ' '
                . floor($v['size'] / 1024)
                . ' kb ('.$this->formfiles[$k]['checksum']
                .')'
            );
            $out                           .= '<br>';
        }
        return $out;
    }
        
    /**
         * Custom upload dir for attachments
         *
         * @param string $dir forlder for uploads
         *
         * @return string folder
         **/
    public function uploadDir($dir)
    {
        $prefix = $this->prefix . '/' . $this->attach['checksum'];
        $deny   = 'Deny from  all';
        $basedir=$dir['basedir'];
        $dirpath=$dir['path'];
        $htaccess='/.htaccess';
        file_put_contents(
            $basedir .
            '/' .
            $this->prefix .$htaccess,
            $deny
        );
            
        $dir['basedir'] .= '/' . $prefix;
        $dir['baseurl'] .= '/' . $prefix;
        $dir['path']    = $basedir;
        $dir['url']     = $basedir;
        $dir['subdir']  = '/' . $prefix;
        if (! is_dir($dirpath)) {
            mkdir($dirpath);
        }
        file_put_contents($dirpath . $htaccess, $deny);
        return $dir;
    }
        
    /**
         * Create custom upload folder if not created
         *
         * @return string path
         **/
    private function _createUploadDir()
    {
        $udir = wp_upload_dir();
        $dir  = $udir['basedir'] . '/' . $this->prefix;
        if (! is_dir($dir)) {
            mkdir($dir);
        }
        file_put_contents($dir . '/.htaccess', "Deny from all\n");
            
        return $dir . '/';
    }
        
    /**
         * Sanitize param
         *
         * @param string $name to sanitize
         *
         * @return string sanitized value
         */
    public function sanitize($name)
    {
        $suffix = '';
        if (strpos($name, '[]')) {
            $suffix = '[]';
        }
            
        return $this->prefix . crc32($name) . $suffix;
    }
        
    /**
         * Get current page url
         *
         * @return string url
         */
    public function getUrl()
    {
        $host='';
        if (isset($_SERVER['HTTP_HOST'])) {
            $host=$_SERVER['HTTP_HOST'];
        }
        return (isset($_SERVER['HTTPS']) ? "https" : "http") .
               "://" . $host . $_SERVER['REQUEST_URI'];
    }
}
    
    require dirname(__FILE__).'/ngabstract.php';
    $var = new NGCore();

