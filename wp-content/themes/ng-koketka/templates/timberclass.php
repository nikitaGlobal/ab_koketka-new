<?php
    
    Class Timberclass
    {
        function __construct()
        {
            $this->prefix = 'ngtheme';
            if ( ! class_exists('Timber')) {
                trigger_error('Please install timber library', E_USER_ERROR);
            }
            $this->wcendpoints = array(
                'order-pay',
                'order-received',
                'view-order',
                'edit-account',
                'edit-address',
                'lost-password',
                'customer-logout',
                'add-payment-method'
            );
        }
        
        private function _addMenus($context)
        {
            if ( ! get_nav_menu_locations() || empty(get_nav_menu_locations())) {
                return $context;
            }
            $locations        = get_nav_menu_locations();
            $context['menus'] = array();
            foreach ($locations as $location => $menuID) {
                $context['menus'][$location] = new Timbermenu($menuID);
            }
            
            return $context;
        }
        
        
        public function getContext($woocommerce = false)
        {
            $context = Timber::context();
            $context = $this->_addPosts($context);
            $context = $this->_addMenus($context);
            $context = $this->_addMods($context);
            if (class_exists('woocommerce')) {
                $context = $this->_addWoo($context);
            }
            if (isset($_REQUEST['s'])) {
                $context['s']=(string)$_REQUEST['s'];
            }
            //  $context=$this->_addMissingProducts($context);
            return $context;
        }
        
        private function _addPosts($context)
        {
            $context['single'] = false;
            if (is_single() || (is_page())) {
                $context['single'] = true;
            }
            if (function_exists('is_woocommerce') && (is_woocommerce())) {
                if (is_singular('product')) {
                    $context = $this->_addProduct($context);
                    
                    return $context;
                } else {
                    $context = $this->_addProducts($context);
                    
                    return $context;
                }
            }
            if (function_exists('is_woocommerce') && is_search()) {
                $context = $this->_addProducts($context);
            }
            $context['posts'] = Timber::get_posts();
            
            return $context;
        }
        
        private function _addWoo($context)
        {
            global $woocommerce;
            $context['cart'] = array(
                'url'    => wc_get_cart_url(),
                'totals' => $woocommerce->cart->get_cart_total(),
                'count'  => $woocommerce->cart->cart_contents_count
            );
            if ($context['user']) {
                $context['user']->logout_url    = wc_logout_url();
                $context['user']->myaccount_url = wc_get_page_permalink('myaccount');
            }
            $context['wcendpoints'] = array();
            $context['woolinks']=array();
            foreach (wc_get_account_menu_items() as $endpoint=>$label) {
                $context['wcendpoints'][$endpoint]   = array(
                    'url'    => wc_get_endpoint_url($endpoint),
                    'active' => is_wc_endpoint_url($endpoint),
                    'label'  => $label
                );
                $context['woolinks'][$endpoint]=$context['wcendpoints'][$endpoint];
            }
            foreach ($this->wcendpoints as $endpoint) {
                if (isset($context['woolinks'][$endpoint])) {
                    continue;
                }
                $context['woolinks'][$endpoint]=array(
                    'url'    => wc_get_endpoint_url($endpoint),
                    'active' => is_wc_endpoint_url($endpoint),
                    'label'  => $label
                );
            }
            
            $context['wcendpoints']['myaccount'] = array(
                'url'   => wc_get_page_permalink('myaccount'),
                'label' => __('My account', 'woocommerce')
            );
            $context['wcendpoints']['logout']= array(
                'url'   => wc_logout_url(),
                'label' => __('My account', 'woocommerce')
            );
            return $context;
        }
        
        private function _addProducts($context)
        {
            $posts               = Timber::get_posts();
            $context['products'] = $posts;
            $context['products'] = $this->_addProductsPrices($context['products']);
            $context['products'] = $this->_addMissingLinks($context['products']);
            if (is_product_category()) {
                $queried_object      = get_queried_object();
                $term_id             = $queried_object->term_id;
                $context['category'] = get_term($term_id, 'product_cat');
                $context['title']    = single_term_title('', false);
                
                //return $context;
            }
            
            return $context;
        }
        
        private function _addMissingLinks($items)
        {
            foreach ($items as $key => $item) {
                if (isset($value->link)) {
                    continue;
                }
                $items[$key]->link = get_permalink($item->ID);
            }
            
            return $items;
        }
        
        private function _addProduct($context)
        {
            $context['sidebar'] = Timber::get_widgets('shop-sidebar');
            $context['post']    = Timber::get_post();
            global $product;
            $product            = wc_get_product($context['post']->ID);
            $context['product'] = $product;
            $context['product'] = $this->_addProductVariations($product);
            
            // Get related products
            $context['related_products'] = $this->_addProductsRelated($product);
            
            // Restore the context and loop back to the main query loop.
            wp_reset_postdata();
            
            return $context;
        }
        
        private function _addProductsRelated($product)
        {
            $related_limit = wc_get_loop_prop('columns');
            $related_ids   = wc_get_related_products($product->get_id(),
                $related_limit);
            $out           = Timber::get_posts($related_ids);
            if ($out) {
                foreach ($out as $key => $item) {
                    $out[$key]->link = get_permalink($item->ID);
                    $out[$key]       = (object)array_merge(
                        (array)$out[$key],
                        (array)self::getPrices(wc_get_product($item->ID))
                    );
                }
            }
            
            return $out;
        }
        
        private function _addProductsPrices($products)
        {
            foreach ($products as $key => $item) {
                $prices         = self::getPrices(wc_get_product($item->ID));
                $products[$key] = (object)(array_merge((array)$item,
                    (array)$prices));
            }
            
            
            return $products;
        }
        
        public static function getPrices($product)
        {
            $out = array();
            if ( ! ($product)) {
                return;
            }
            if ( ! $product->is_on_sale()) {
                return array('price' => $product->get_price());
            }
            $out['item_is_on_sale'] = true;
            if ($product->is_type('variable')) {
                $out['variable'] = true;
                
                return array_merge($out, self::getProductVariablePrice($product));
            }
            foreach (self::prices() as $type) {
                $out[$type] = false;
                $try        = $product->{'get_' . $type}();
                if ((int)$try == 0) {
                    continue;
                }
                $out[$type] = $try;
            }
            
            return $out;
        }
        
        
        public static function prices()
        {
            return array('regular_price', 'sale_price', 'price');
        }
        
        public static function getProductVariablePrice($product)
        {
            $out                  = array();
            $minReg               = $product->get_variation_regular_price('min');
            $maxReg               = $product->get_variation_regular_price('max');
            $out['regular_price'] = $minReg;
            if ($minReg != $minReg) {
                $out['regular_pricemin'] = $minReg;
                $out['regular_pricemax'] = $maxReg;
            }
            $minSale           = $product->get_variation_sale_price('min');
            $maxSale           = $product->get_variation_sale_price('max');
            $out['sale_price'] = $minSale;
            if ($minSale != $maxSale) {
                $out['sale_pricemin'] = $minSale;
                $out['sale_pricemax'] = $maxSale;
            }
            
            return $out;
        }
        
        
        private function _addProductVariations($product)
        {
            if ( ! $product->is_type('variable')) {
                return $product;
            }
            $product->variable = true;
            
            
            $result = array();
            foreach ($product->get_variation_attributes() as $attribute => $values) {
                $result[$attribute] = array(
                    'key'    => $this->_getProductAttributeKeys($product)[$attribute],
                    'label'  => wc_attribute_label($attribute),
                    'values' => array()
                );
                foreach ($values as $value) {
                    $term_obj                               = get_term_by('slug',
                        $value, $attribute);
                    $term_id                                = $term_obj->term_id; // The ID  <==  <==  <==  <==  <==  <==  HERE
                    $term_name                              = $term_obj->name; // The Name
                    $term_slug                              = $term_obj->slug; // The Slug
                    $result[$attribute]['values'][$term_id] = array(
                        'id'   => $term_id,
                        'name' => $term_name,
                        'key'  => $term_slug
                    );
                }
            }
            $product->params          = $result;
            $variations_json          = json_encode($product->get_available_variations());
            $product->variations_json = function_exists('wc_esc_json') ? wc_esc_json($variations_json) : _wp_specialchars($variations_json,
                ENT_QUOTES, 'UTF-8', true);
            
            return $product;
        }
        
        private function _getProductAttributeKeys($product)
        {
            $keys = $product->get_attributes();
            $out  = array();
            foreach ($keys as $key => $values) {
                $out[$values['data']['name']] = $key;
            }
            
            return $out;
        }
        
        private function _setImages($context)
        {
            if ( ! isset($context['posts']) || ! is_array($context['posts']) || empty($context['posts'])) {
                return $context;
            }
            foreach ($context['posts'] as $id => $post_) {
                if ( ! isset($post_->_thumbnail_id) || (int)$post_->_thumbnail_id == 0) {
                    continue;
                }
                $context['posts'][$id]->coverImage = $post_->_thumbnail_id;
            }
            
            return $context;
        }
        
        private function _addMods($context)
        {
            $mods = get_theme_mods();
            if ( ! is_array($mods) || empty($mods)) {
                return $context;
            }
            foreach ($mods as $key => $mod) {
                $context['mods'][$key] = $mod;
            }
            
            return $context;
        }
        
        
        private function _setTemplate($context)
        {
            $templates = array();
            $debug     = debug_backtrace();
            foreach ($debug as $trace) {
                if (isset($trace['function'])
                    && $trace['function'] == 'do_action'
                    && in_array($this->prefix . 'render', $trace['args'])
                ) {
                    $fileparts = pathinfo($trace['file']);
                    $file      = $fileparts['filename'] . '.twig';
                }
            }
            if (function_exists('is_wc_endpoint_url')) {
                $templates = $this->_getWCEndPoint($templates);
            }
            $templates[] = $file;
            
            return $templates;
        }
        
        private function _getWCEndPoint($templates)
        {
            
            if (function_exists('is_wc_endpoint_url')) {
                foreach ($this->wcendpoints as $endpoint) {
                    if (is_wc_endpoint_url($endpoint)) {
                        $templates[] = $endpoint . '.twig';
                    }
                }
            }
            $check = array(
                'cart',
                'checkout',
                'product',
                'account_page',
                'checkout_pay_page',
                'product_tag',
                'shop'
            );
            foreach ($check as $string) {
                $function = 'is_' . $string;
                if (function_exists($function) && $function()) {
                    $templates[] = $string . '.twig';
                }
            }
            
            return $templates;
        }
        
        public function render($context = false, $templates = false)
        {
            if ( ! $context) {
            }
            $context = $this->getContext();
            if ( ! $templates) {
                $templates              = array_merge(
                    $this->_setTemplate($context),
                    array('index.twig')
                );
                $context['contextzone'] = $this->_setTemplate($context);
            }
            Timber::render($templates, $context);
        }
    }
    
    
    function timber_set_product($post)
    {
        global $product;
        
        if (is_woocommerce()) {
            $product = wc_get_product($post->ID);
        }
    }
    
    remove_action('woocommerce_after_single_product_summary',
        'woocommerce_output_related_products', 20);
    remove_action('woocommerce_after_shop_loop_item',
        'woocommerce_template_loop_add_to_cart', 10);