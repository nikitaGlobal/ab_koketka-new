<?php
if (!defined('ABSPATH')) exit;
 // Exit if accessed directly

if (!class_exists('Econt_Order')) {
    
    class Econt_Order
    {
        
        function __construct() {

            
            
            //add econt offices fields to checkout
            add_action('woocommerce_after_order_notes', array($this, 'econt_offices_checkout_fields'), 30, 1);
            
            //validate econt offices fields in checkout
            add_action('woocommerce_checkout_process', array($this, 'econt_offices_checkout_field_process'));
            
            //save econt office field to the order
            add_action('woocommerce_checkout_update_order_meta', array($this, 'econt_offices_checkout_field_update_order_meta'));
            
            //filter COD payment gateway if nesesery
            add_filter('woocommerce_available_payment_gateways',array($this, 'filter_cod_gateway'),1);
            
            //remove some billing fields
            add_filter('woocommerce_billing_fields', array($this, 'econt_filter_billing_fields'));
            
            //remove all shipping fields
            add_filter('woocommerce_shipping_fields', array($this, 'econt_filter_shipping_fields'));
            
            //add message about econt offices in thank you and order pages
            add_action('woocommerce_thankyou', array($this, 'econt_offices_display_order_data'), 20);

            //tuk shte pravq vrushtaneto na pratka ot klient
            add_action('woocommerce_view_order', array($this, 'econt_offices_display_order_data'), 20);
            
            //add econt fields to order emails.
            add_action('woocommerce_email_before_order_table', array($this, 'econt_email_details'), 10, 3);
            //remove "(optional)" from our non required fields introduced in Woo 3.4
            add_filter( 'woocommerce_form_field' , array($this, 'econt_remove_checkout_optional_fields_label'), 10, 4 );

        }



        public function woo_cart_has_virtual_product() {
            global $woocommerce;
            // By default, no virtual product
            $has_virtual_products = false;
            // Default virtual products number
            $virtual_products = 0;
            // Get all products in cart
            $products = $woocommerce->cart->get_cart();
            // Loop through cart products
            foreach( $products as $product ) {
               //  print_r($product);
                // Get product ID and '_virtual' post meta
                $product_id = $product['product_id'];
               // print_r(get_post_meta( $product_id, '_virtual', true ));
                //$is_virtual = $product['data']->virtual; //za stari versii na woocommerce predi 2.2 
                if (version_compare(WOOCOMMERCE_VERSION, '2.2', '>=')) {
                $is_virtual = get_post_meta( $product_id, '_virtual', true );
                }else{
                $is_virtual = $product['data']->virtual; //za stari versii na woocommerce predi 2.2  
                }
                // Update $has_virtual_product if product is virtual
                if( $is_virtual == 'yes' ){
                $virtual_products += 1;
                }
            }
            if( count($products) == $virtual_products ){
            $has_virtual_products = true;
            }
            return $has_virtual_products;
        } 





        
        /**
         * Add the  econt offices fields to the checkout
         */
        
        function econt_offices_checkout_fields($checkout) {
        $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
        $chosen_shipping = $chosen_methods[0];
        //echo $chosen_shipping;
        global $woocommerce;
        $woocommerce->shipping; //za da moga da suzdam object WC_Econt_Shipping_Method
        $wc_econt = new WC_Econt_Shipping_Method;
            if( $this->woo_cart_has_virtual_product() == false && $chosen_shipping == 'econt_shipping_method' && $wc_econt->enabled == 'yes'){
            echo '<div id="econtLoader" ></div><div id="econt_custom_checkout_field"><h2>' . __('Econt Express', 'woocommerce-econt') . '</h2>';
            if(!empty($wc_econt->description)){
                echo '<p>'. $wc_econt->description .'</p>';
            }
            
            $econt_mysql = new Econt_mySQL;
            
            //express city courier
            if( (int)$wc_econt->city_courier == 1 && $wc_econt->send_from == 'DOOR'){
            $sender_address = explode(';',$wc_econt->address);
            
            if( isset($sender_address[1]) ){
            $sender_city_id = $econt_mysql->getCityIdByCityName($sender_address[1]);
                        
            }else{
            $sender_city_id = $econt_mysql->getCityIdByCityName($wc_econt->office_town); 


            }
            
            
            echo '<script type="text/javascript"> var sender_city_id = "' . $sender_city_id['city_id'] .'";</script>'; //define sender city for express city courier 
            }else{

            echo '<script type="text/javascript"> var sender_city_id = ""; </script>'; //define sender city for express city courier 
            }
            //end of city courier

            //office locator in colorbox jquery
            $office_locator = 'https://www.bgmaps.com/templates/econt?office_type=to_office_courier&shop_url=' . get_site_url(); //HTTPS_SERVER;
            $office_locator_domain = 'https://www.bgmaps.com';
            //end of office locator

            //delivery days
            if((int)$wc_econt->delivery_days == 1){
            $delivery_days = $econt_mysql->delivery_days($wc_econt->username, $wc_econt->password, $wc_econt->live);
            }

            $econt_shipping_to = array();
            $econt_shipping_to[0] = __('please select...', 'woocommerce-econt');
            if($wc_econt->send_to_door == 1){
             $econt_shipping_to['DOOR'] = __('to door', 'woocommerce-econt');   
            }
            if($wc_econt->send_to_office == 1){
             $econt_shipping_to['OFFICE'] = __('to office', 'woocommerce-econt');              
            }
            if($wc_econt->send_to_machine == 1){
             $econt_shipping_to['MACHINE'] = __('to APS', 'woocommerce-econt');              
            }

            woocommerce_form_field(
            'econt_shipping_to', array('type' => 'select', 'required' => true, 'class' => array('econt_shipping_to form-row-wide'), 'label' => __('Shipping to', 'woocommerce-econt'), 'placeholder' => __('Shipping to', 'woocommerce-econt'), 'options' => $econt_shipping_to), $checkout->get_value('econt_shipping_to'));

            //To office
            if( $wc_econt->send_to_office == 1 ){
            woocommerce_form_field(
            'econt_offices_town', array('type' => 'text', 'required' => true, 'class' => array('econt_shipping_to_office form-row-wide'), 'label' => __('Town (Please, start typing and select from results.)', 'woocommerce-econt'), 'placeholder' => __('Enter your town', 'woocommerce-econt'),), $checkout->get_value('econt_offices_town'));
            
            woocommerce_form_field(
            'econt_offices_postcode', array('type' => 'text', 'required' => true, 'class' => array('econt_shipping_to_office form-row-wide'), 'label' => __('Post Code', 'woocommerce-econt'), 'placeholder' => __('Post Code', 'woocommerce-econt'),), $checkout->get_value('econt_offices_postcode'));
            
            include_once( ECONT_PLUGIN_DIR.'/inc/view/html-checkout-view.php' ); //office locator

            woocommerce_form_field(
            'econt_offices', array('type' => 'select', 'required' => true, 'class' => array('econt_shipping_to_office form-row-wide'), 'label' => __('Office', 'woocommerce-econt'), 'placeholder' => __('Select office', 'woocommerce-econt'), 'options' => array('0' => __('please select...', 'woocommerce-econt'))), $checkout->get_value('econt_offices'));
            
            if($wc_econt->partial_delivery == 1){
            echo '<div class="econt_shipping_to_office">' . __('We offer our customers partial delivery.', 'woocommerce-econt') . '</div>';    
            }
            //if( $wc_econt->send_to_door == 0 )
            //echo '<script type="text/javascript">jQuery(".econt_shipping_to_office").slideToggle();</script>';
            }

            //To Machine
            if( $wc_econt->send_to_machine == 1 ){
            woocommerce_form_field(
            'econt_machines_town', array('type' => 'text', 'required' => true, 'class' => array('econt_shipping_to_machine form-row-wide'), 'label' => __('24 часа Еконт - Автоматична пощенска станция” (АПС) е устройство, с което сами изпращате и получавате пратки денонощно, без почивен ден.  Научете повече на: <a href="http://www.econt.com/24-chasa-econt-aps/" target="_blank">http://www.econt.com/24-chasa-econt-aps/</a><br>Град (Моля, започнете да пишете и изберете от резултатите.)', 'woocommerce-econt'), 'placeholder' => __('Enter your town', 'woocommerce-econt'),), $checkout->get_value('econt_offices_town'));
            
            woocommerce_form_field(
            'econt_machines_postcode', array('type' => 'text', 'required' => true, 'class' => array('econt_shipping_to_machine form-row-wide'), 'label' => __('Post Code', 'woocommerce-econt'), 'placeholder' => __('Post Code', 'woocommerce-econt'),), $checkout->get_value('econt_offices_postcode'));
           

            woocommerce_form_field(
            'econt_machines', array('type' => 'select', 'required' => true, 'class' => array('econt_shipping_to_machine form-row-wide'), 'label' => __('Office', 'woocommerce-econt'), 'placeholder' => __('Select office', 'woocommerce-econt'), 'options' => array('0' => __('please select...', 'woocommerce-econt'))), $checkout->get_value('econt_offices'));

            if( $wc_econt->send_to_office == 0 )
            echo '<script type="text/javascript">jQuery(".econt_shipping_to_machine").slideToggle();</script>';
            

            }

            //to door
            if( $wc_econt->send_to_door == 1 ){
            woocommerce_form_field(
            'econt_door_town', array('type' => 'text', 'required' => true, 'class' => array('econt_shipping_to_door form-row-wide'), 'label' => __('Town (Please, start typing and select from results.)', 'woocommerce-econt'), 'placeholder' => __('Enter your town', 'woocommerce-econt'),), $checkout->get_value('econt_door_town'));
            
            woocommerce_form_field(
            'econt_door_postcode', array('type' => 'text', 'required' => true, 'class' => array('econt_shipping_to_door form-row-wide'), 'label' => __('Post Code', 'woocommerce-econt'), 'placeholder' => __('Post Code', 'woocommerce-econt'),), $checkout->get_value('econt_door_postcode'));
            
            woocommerce_form_field(
            'econt_door_street', array('type' => 'text', 'class' => array('econt_shipping_to_door form-row-wide'), 'label' => __('Street (Please, start typing and select from results.)', 'woocommerce-econt'), 'placeholder' => __('Enter your street', 'woocommerce-econt'),), $checkout->get_value('econt_door_street'));
            
            woocommerce_form_field(
            'econt_door_quarter', array('type' => 'text', 'class' => array('econt_shipping_to_door form-row-wide'), 'label' => __('Quarter (Please, start typing and select from results.)', 'woocommerce-econt'), 'placeholder' => __('Enter your quarter', 'woocommerce-econt'),), $checkout->get_value('econt_door_quarter'));
            
            woocommerce_form_field(
            'econt_door_street_num', array('type' => 'text', 'class' => array('econt_shipping_to_door form-row-wide'), 'label' => __('Street num.:', 'woocommerce-econt'), 'placeholder' => __('street num', 'woocommerce-econt'),), $checkout->get_value('econt_door_street_num'));
            
            woocommerce_form_field(
            'econt_door_street_bl', array('type' => 'text', 'class' => array('econt_shipping_to_door form-row-wide'), 'label' => __('Street block num.:', 'woocommerce-econt'), 'placeholder' => __('blok num', 'woocommerce-econt'),), $checkout->get_value('econt_door_street_bl'));
            
            woocommerce_form_field(
            'econt_door_street_vh', array('type' => 'text', 'class' => array('econt_shipping_to_door form-row-wide'), 'label' => __('Street entrance num.:', 'woocommerce-econt'), 'placeholder' => __('entr. num', 'woocommerce-econt'),), $checkout->get_value('econt_door_street_vh'));
            
            woocommerce_form_field(
            'econt_door_street_et', array('type' => 'text', 'class' => array('econt_shipping_to_door form-row-wide'), 'label' => __('Street floor num.:', 'woocommerce-econt'), 'placeholder' => __('fl. num', 'woocommerce-econt'),), $checkout->get_value('econt_door_street_et'));
            
            woocommerce_form_field(
            'econt_door_street_ap', array('type' => 'text', 'class' => array('econt_shipping_to_door form-row-wide'), 'label' => __('Street apartment num.:', 'woocommerce-econt'), 'placeholder' => __('ap. num', 'woocommerce-econt'),), $checkout->get_value('econt_door_street_ap'));
            woocommerce_form_field(
            'econt_door_other', array('type' => 'text', 'class' => array('econt_shipping_to_door form-row-wide'), 'label' => __('If your address is not in the list please type it here:', 'woocommerce-econt'), 'placeholder' => __('Enter other adress info', 'woocommerce-econt'),), $checkout->get_value('econt_door_other'));
            if( (int)$wc_econt->delivery_days == 1 && !empty($delivery_days)){
            woocommerce_form_field(
            'econt_delivery_days', array('type' => 'select', 'class' => array('econt_shipping_to_door form-row-wide'), 'label' => __('Delivery Days', 'woocommerce-econt'), 'placeholder' => __('', 'woocommerce-econt'), 'options' => $delivery_days), $checkout->get_value('econt_delivery_days'));    
            }
            if( $wc_econt->city_courier == 1 ){
            woocommerce_form_field(
            'econt_city_courier', array('type' => 'select', 'class' => array('econt_city_courier form-row-wide'), 'label' => __('Express city courier', 'woocommerce-econt'), 'placeholder' => __('', 'woocommerce-econt'), 'options' => array('0' => __('please select...', 'woocommerce-econt'), 'e1' => __('up to 60 minutes', 'woocommerce-econt'), 'e2' => __('up to 90 minutes', 'woocommerce-econt'), 'e3' => __('up to 120 minutes', 'woocommerce-econt'))), $checkout->get_value('econt_city_courier'));
            }
            if($wc_econt->partial_delivery == 1){
            echo '<div class="econt_shipping_to_door">' . __('We offer our customers partial delivery.', 'woocommerce-econt') . '</div>';    
            }

            woocommerce_form_field(
            'econt_customer_shipping_cost', array('type' => 'text', 'class' => array('econt_shipping_cost form-row-wide'), 'label' => __('Econt Customer Shipping Cost:', 'woocommerce-econt'), 'placeholder' => __('Enter Customer Shipping Cost', 'woocommerce-econt'), 'default' => '',), $checkout->get_value('econt_customer_shipping_cost'));
            woocommerce_form_field(
            'econt_total_shipping_cost', array('type' => 'text', 'class' => array('econt_shipping_cost form-row-wide'), 'label' => __('Econt Total Shipping Cost:', 'woocommerce-econt'), 'placeholder' => __('Econt Total Shipping Cost', 'woocommerce-econt'), 'default' => '',), $checkout->get_value('econt_total_shipping_cost'));

            }  
            echo '<p id="calculate_loading" class="button" style="display: block;">
            <input name="" class="button" id="button_calculate_loading" value="'.__('Calculate Loading','woocommerce-econt').'" type="button"></p></div><div class="econt_clear"></div>';

            }
            
           

        }
        
        function econt_offices_checkout_field_process() {
          
            // Check if set, if its not set add an error.
            $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
            $chosen_shipping = $chosen_methods[0];
            global $woocommerce;
            $woocommerce->shipping; //za da moga da suzdam object WC_Econt_Shipping_Method
            $wc_econt = new WC_Econt_Shipping_Method;
            $econt_mysql = new Econt_mySQL;

            if( $this->woo_cart_has_virtual_product() == false && $chosen_shipping == 'econt_shipping_method' && $wc_econt->enabled == 'yes' ){
                $validation = $econt_mysql->receiver_address_validation($_POST);
                if($validation['valid']  === false){
                    if ( function_exists( 'wc_add_notice' ) ) {
                    wc_add_notice($validation['msg'], 'error');
                    }else{
                        $woocommerce->add_error(sprintf($validation['msg']));
                    }
                }

            }
           
        }
        
        function econt_offices_checkout_field_update_order_meta($order_id) {
           $econt_mysql = new Econt_mySQL;
           $econt_mysql->shipping_field_update_order_meta($order_id, $_POST); 

        }
        
        function econt_filter_billing_fields($fields) {
        if(is_checkout()){
            $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
            $chosen_shipping = $chosen_methods[0];
            global $woocommerce;
            $woocommerce->shipping; //za da moga da suzdam object WC_Econt_Shipping_Method
            $wc_econt = new WC_Econt_Shipping_Method;

            if( $this->woo_cart_has_virtual_product() == false && $chosen_shipping == 'econt_shipping_method' && $wc_econt->enabled == 'yes' ){
     
                unset($fields["billing_country"]);
                
                //unset( $fields["billing_company"] );
                unset($fields["billing_address_1"]);
                unset($fields["billing_address_2"]);
                unset($fields["billing_city"]);
                unset($fields["billing_state"]);
                unset($fields["billing_postcode"]);
                
                //unset( $fields["billing_phone"] );
            }
        }
            return $fields;
        }
        
        function econt_filter_shipping_fields($fields) {
        if(is_checkout()){
            $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
            $chosen_shipping = $chosen_methods[0];
            global $woocommerce;
            $woocommerce->shipping; //za da moga da suzdam object WC_Econt_Shipping_Method
            $wc_econt = new WC_Econt_Shipping_Method;

            if( $this->woo_cart_has_virtual_product() == false && $chosen_shipping == 'econt_shipping_method' && $wc_econt->enabled == 'yes' ){


                unset($fields["shipping_first_name"]);
                unset($fields["shipping_last_name"]);
                unset($fields["shipping_company"]);
                unset($fields["shipping_address_1"]);
                unset($fields["shipping_address_2"]);
                unset($fields["shipping_city"]);
                unset($fields["shipping_postcode"]);
                unset($fields["shipping_country"]);
                unset($fields["shipping_state"]);
                
            }
        }
            return $fields;
        }
        
        function econt_offices_display_order_data($order_id) {

        global $woocommerce;
        $woocommerce->shipping; //za da moga da suzdam object WC_Econt_Shipping_Method
        $orders = new WC_Order($order_id);
        $wc_econt = new WC_Econt_Shipping_Method;
        //$econt_mysql = new Econt_mySQL;

        $shipping_items = $orders->get_items( 'shipping' );
        foreach($shipping_items as $el){
            $order_shipping_method_id = $el['method_id'] ;
        }

        if($order_shipping_method_id == 'econt_shipping_method'){

                    $econt_mysql = new Econt_mySQL;

                    $office_code = get_post_meta($order_id, 'Econt_Office', true);
                    $office = $econt_mysql->getOfficeByOfficeCode($office_code);

                    $machine_code = get_post_meta($order_id, 'Econt_Machine', true);
                    $machine = $econt_mysql->getOfficeByOfficeCode($machine_code);

                    $loading = $econt_mysql->getLoading($order_id);
        ?>
            <h3><?php
                    _e('Econt Express shipping data', 'woocommerce-econt', 'woocommerce-econt'); ?></h3>
            <table class="tbi-table">
                <caption><h3><?php _e('The goods will be shipped to:', 'woocommerce-econt'); ?></h3></caption>
                <thead>
                    <tr>
                        <th scope="col"><?php _e('Name', 'woocommerce-econt'); ?></th>
                        <th scope="col"><?php _e('Value', 'woocommerce-econt'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php
                    if (get_post_meta($order_id, 'Econt_Shipping_To', true) == 'OFFICE') { ?>
                    
                    <tr>
                        <td data-label="<?php _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Town:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Office_Town', true); ?></td>
                    </tr>
                    <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt office:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo $office['name'] . __(' address: ', 'woocommerce-econt') . $office['address']; ?></td>
                         </tr>
                    <?php
                    } 
                    elseif (get_post_meta($order_id, 'Econt_Shipping_To', true) == 'MACHINE') { ?>
                        <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Town:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Machine_Town', true); ?></td>
                    </tr>
                    <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt APS:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo $machine['name'] . __(' address: ', 'woocommerce-econt') . $machine['address']; ?></td>
                         </tr>

                    
                    <?php
                    } 
                    elseif (get_post_meta($order_id, 'Econt_Shipping_To', true) == 'DOOR') { ?>
                        <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Town:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Door_Town', true); ?></td>
                    </tr>
                    <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Postcode:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Door_Postcode', true); ?></td>
                    </tr>
                    </tr>
                     <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Street:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Door_Street', true); ?></td>
                    </tr>
                    <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Quarter:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Door_Quarter', true); ?></td>
                    </tr>
                     <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Street Num.:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Door_street_num', true); ?></td>
                    </tr>
                    <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Building Num.:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Door_building_num', true); ?></td>
                    </tr>
                     <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Entrance:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Door_Entrance_num', true); ?></td>
                    </tr>
                    <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Floor:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Door_Floor_num', true); ?></td>
                    </tr>
                     <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Apartment:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Door_Apartment_num', true); ?></td>
                    </tr>
                    <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Other:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Door_Other', true); ?></td>
                    </tr>
                    <?php
                    } ?>
                     <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Econt Shipping Cost:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Customer_Shipping_Cost', true); 
                        if(is_numeric( get_post_meta($order_id, 'Econt_Customer_Shipping_Cost', true) ))
                        echo get_woocommerce_currency_symbol();
                        ?></td>
                    </tr>
                    <?php  if((int)$wc_econt->return_item == 1){ 
        //tuk sa usloviqta za generirane na tovaritelnica za vrushtane na poruchka

                        ?>
                    <tr>
                        <td data-label="<?php
                        _e('Name', 'woocommerce-econt'); ?>"><?php
                        _e('Generate loading to return the received order:', 'woocommerce-econt'); ?></td>
                        <td data-label="<?php _e('Value', 'woocommerce-econt'); ?>"><?php
                        echo get_post_meta($order_id, 'Econt_Customer_Shipping_Cost', true) . get_woocommerce_currency_symbol(); ?></td>
                    </tr>
                    <?php  } ?>
                </tbody>
            </table>
        <?php
     
     }      
    
    }
        
        //email data
        
        function econt_email_order_meta_fields($fields, $sent_to_admin, $order) {

        global $woocommerce;
        $woocommerce->shipping; //za da moga da suzdam object WC_Econt_Shipping_Method
        $orders = new WC_Order($order->get_id());
        //$wc_econt = new WC_Econt_Shipping_Method;
        //$econt_mysql = new Econt_mySQL;

        $shipping_items = $orders->get_items( 'shipping' );
        foreach($shipping_items as $el){
            $order_shipping_method_id = $el['method_id'] ;
        }

        if($order_shipping_method_id == 'econt_shipping_method'){


            $getoffice = new Econt_mySQL;
            $office_code = get_post_meta($order->get_id(), 'Econt_Office', true);
            $office = $getoffice->getOfficeByOfficeCode($office_code);

            $machine_code = get_post_meta($order->get_id(), 'Econt_Machine', true);
            $machine = $getoffice->getOfficeByOfficeCode($office_code);
            
            if(get_post_meta($order->get_id(), 'Econt_Shipping_To', true) == 'OFFICE'){
                
                $fields['Econt_Office_Town'] = array('label' => __('Econt Town', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_Office_Town', true),);
                
                $fields['Econt_Office'] = array('label' => __('Econt office', 'woocommerce-econt'), 'value' => $office['name'] . __(' address: ', 'woocommerce-econt') . $office['address'],);
            }elseif(get_post_meta($order->get_id(), 'Econt_Shipping_To', true) == 'MACHINE'){
                $fields['Econt_Machine_Town'] = array('label' => __('Econt Town', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_MAchine_Town', true),);
                
                $fields['Econt_Machine'] = array('label' => __('Econt APS', 'woocommerce-econt'), 'value' => $machine['name'] . __(' address: ', 'woocommerce-econt') . $machine['address'],);

            }elseif (get_post_meta($order->get_id(), 'Econt_Shipping_To', true) == 'DOOR'){
                
                $fields['Econt_Door_Town'] = array('label' => __('Econt Town', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_Door_Town', true),);
                
                $fields['Econt_Door_Postcode'] = array('label' => __('Econt Postcode', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_Door_Postcode', true),);
                
                $fields['Econt_Door_Street'] = array('label' => __('Econt Street', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_Door_Street', true),);
                
                $fields['Econt_Door_Quarter'] = array('label' => __('Econt Quarter', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_Door_Quarter', true),);
                
                $fields['Econt_Door_street_num'] = array('label' => __('Econt Street Num.', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_Door_street_num', true),);
                
                $fields['Econt_Door_building_num'] = array('label' => __('Econt Building Num.', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_Door_building_num', true),);
                
                $fields['Econt_Door_Entrance_num'] = array('label' => __('Econt Entrance', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_Door_Entrance_num', true),);
                
                $fields['Econt_Door_Floor_num'] = array('label' => __('Econt Floor', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_Door_Floor_num', true),);
                
                $fields['Econt_Door_Apartment_num'] = array('label' => __('Econt Apartment', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_Door_Apartment_num', true),);
                
                $fields['Econt_Door_Other'] = array('label' => __('Econt Other', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_Door_Other', true),);
            }
                $fields['Econt_Customer_Shipping_Cost'] = array('label' => __('Econt Shipping Cost', 'woocommerce-econt'), 'value' => get_post_meta($order->get_id(), 'Econt_Customer_Shipping_Cost', true),);
        }
            return $fields;
        }
        
        //econt email
        public function econt_email_details($order, $sent_to_admin, $plain_text = false) {
           if (version_compare(WOOCOMMERCE_VERSION, '2.2', '>=')) {
            $shipping_items = $order->get_items('shipping');
            $shipping_method_id = '';
            
            foreach ($shipping_items as $key => $value) {
                
                $shipping_method_id = $value['method_id'];
                //$shipping_method_title = $value['name'];
                //$shipping_item_id = $key;
           }
            
            if ('econt_shipping_method' === $shipping_method_id) {
                
                $this->econt_offices_display_order_data($order->get_id());
            }
        
            }else{
             $this->econt_offices_display_order_data($order->get_id());   
            }

         }
    
        public function filter_cod_gateway($gateways){
            if ( is_admin() ) return $gateways; //don't exexute in admin area
            $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
            $chosen_shipping = $chosen_methods[0];
            global $woocommerce;
            $woocommerce->shipping; //za da moga da suzdam object WC_Econt_Shipping_Method
            $wc_econt = new WC_Econt_Shipping_Method;

            if( $this->woo_cart_has_virtual_product() == false && $chosen_shipping == 'econt_shipping_method' && $wc_econt->enabled == 'yes' ){
            
                if($wc_econt->cd != 1){
                unset($gateways['cod']);
                }       

            }
            
            return $gateways;

        }


        public function econt_remove_checkout_optional_fields_label( $field, $key, $args, $value ) {
            // Only on checkout page
            if( ! ( is_checkout() && ! is_wc_endpoint_url() ) ){
                return $field;
            }

            $optional = '&nbsp;<span class="optional">(' . esc_html__( 'optional', 'woocommerce' ) . ')</span>';
            $keys = array( 'econt_door_street', 'econt_door_quarter', 'econt_door_street_num', 'econt_door_street_bl', 'econt_door_street_vh', 'econt_door_street_et', 'econt_door_street_ap', 'econt_door_other', 'econt_delivery_days', 'econt_city_courier' );

            if( in_array($key, $keys) ){
                return str_replace( $optional, '', $field );
            }else{
                return $field;
            }
        }


    }
}


new Econt_Order();



?>