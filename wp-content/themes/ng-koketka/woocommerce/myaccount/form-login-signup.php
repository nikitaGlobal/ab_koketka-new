<?php
    if ( ! defined('ABSPATH')) {
        exit; // Exit if accessed directly.
    }
    if ('yes' === get_option('woocommerce_enable_myaccount_registration')) { ?>
        <form id="sign_up"
              class="form_custom form_sign_up<?php if ( ! $args['notmodal']) { ?> notmodal<?php } ?> "
              <?php if ( ! $args['notmodal']){ ?>style="display:none;"<?php } ?> <?php do_action('woocommerce_register_form_tag'); ?>>
            <?php do_action('woocommerce_register_form_start'); ?>
            <div class="form_item">
                <h4><?php esc_html_e('Register', 'woocommerce'); ?></h4>
                <?php if ('no' === get_option('woocommerce_registration_generate_username')) { ?>
                    <label class="required" for="reg_username">Ваш e-майл</label>
                    <input name="username" type="text" id="reg_username"
                           required="required">
                <?php } ?>
                <label class="required"
                       for="reg_email"><?php esc_html_e('Email address',
                        'woocommerce'); ?></label>
                <input type="text" id="reg_email" name="email"
                       value="<?php echo ( ! empty($_POST['email'])) ? esc_attr(wp_unslash($_POST['email'])) : ''; ?>">
                <?php if ('no' === get_option('woocommerce_registration_generate_password')) : ?>


                    <label for="reg_password"><?php esc_html_e('Password',
                            'woocommerce'); ?>&nbsp;</label>
                    <input type="password"
                           class="woocommerce-Input woocommerce-Input--text input-text"
                           name="password" id="reg_password"
                           autocomplete="new-password"/>
                
                
                <?php else : ?>

                    <p><?php esc_html_e('A password will be sent to your email address.',
                            'woocommerce'); ?></p>
                
                <?php endif; ?>


                <div class="checkbox_custom">
                    <input type="checkbox" id="privacy_politic"
                           name="user_consents[privacy-policy]" required="required">
                    <label for="privacy_politic">Вие прочетохте <a
                                href="<?php echo get_the_privacy_policy_link(); ?>">нашата
                            политика</a> за лични данни.</label>
                </div>
                <?php wp_nonce_field('woocommerce-register',
                    'woocommerce-register-nonce'); ?>
                <button type="submit"
                        class="woocommerce-Button woocommerce-button button woocommerce-form-register__submit"
                        name="register" value="<?php esc_attr_e('Register',
                    'woocommerce'); ?>"><?php esc_html_e('Register',
                        'woocommerce'); ?></button>
            </div>
            
            <?php if ( ! $args['notmodal']) { ?>
                <div class="popup_close">
                    <svg width="22" height="21" viewBox="0 0 22 21" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <line x1="18" y1="3.53553" x2="4.53553" y2="17"
                              stroke="white"
                              stroke-width="5" stroke-linecap="round"/>
                        <line x1="17.4645" y1="17" x2="4" y2="3.53554" stroke="white"
                              stroke-width="5" stroke-linecap="round"/>
                    </svg>
                </div>
            <?php } ?>
        </form>
    <?php } ?>