<?php


    //require 'templates/timberclass.php';
    $timber = new TimberClass();
    
    $context            = $timber->getContext();
    $context['sidebar'] = Timber::get_widgets( 'shop-sidebar' );
    wp_reset_postdata();
    if ( is_singular( 'product' ) ) {
        
        
        Timber::render( 'templates/single-product.twig', $context );
    } else {
    
        
    
        Timber::render('templates/archive-product.twig', $context);
    }
    