<?php
/**
* Our Abstract goes here
* methods are used along the theme
*
* @category NikitaGlobal
* @package  NikitaGlobal
* @author   Nikita Menshutin <nikita@nikita.global>
* @license  http://nikita.global commercial
* @link     http://nikita.global
* */
abstract class NG
{

    /**
* Plugin prefix
*
* @param string $suffix suffic
*
* @return string prefig
*/
    static function prefix($suffix = '')
    {
        return 'cw_ff' . $suffix;
    }

    /**
* Plugin version
*
* @return string version
*/
    static function version()
    {
        return '3.7';
    }

    /**
* Get tags used in html5 forms
*
* @return array tags
*/
    static function tags()
    {
        return array('input', 'textarea', 'select');
    }

    /**
* Print form in theme
*
* @param string $form file with form name
*
* @return void
*/
    static function form($form)
    {
        echo do_shortcode(
            '['
            . self::prefix()
            . ']'
            . $form
            . '[/'
            . self::prefix() .
            ']'
        );
    }

    /**
* Validate attrs for form fields
*
* @param type $suffix string field type
*
* @return void
*/
    static function validate($suffix)
    {
        $rules          = array();
        $rules['phone'] = 'data-cwvalidate="^[0-9\-\+]{9,15}$"' .
        'data-cwmessage="' . __(
            "from 7 to 9 characters. Please " .
            'use only digits, plus, minus and arentheses '
        ) . '"' .
        ' data-cwfilter="^\+?[0-9\-\(\)\ \+]+$"';
        $rules['email'] = 'data-cwvalidate=\'^(([^<>()\[\]\\.,;:' .
        '\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]' .
        '{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|((' .
        '[a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$\' ' .
        ' data-cwmessage="' . __("Wrong email address") . '"';
        apply_filters('cwff_rules', $rules);
        echo $rules[$suffix];
    }

    /**
* If wp_enqueue is not used, you can enqueue manually
*
* @return void
*/
    static function scriptTag()
    {
        echo '<script>';
        echo 'var ' . self::prefix() . '=' . json_encode(
            array(
            'ajaxurl' => str_replace(
                array(
                'http://',
                'https://'
                ),
                '//',
                admin_url('admin-ajax.php')
            )
            )
        );
        echo ";";
        echo '</script>';
        echo '<script src="';
        echo plugin_dir_url(__FILE__) .
             'cwff.js?version=' .
             self::version() .
             '"></script>';
    }
    
    public static function makeAhref($url, $title)
    {
        return '<a href="'.$url.'">'.$title.'</a>';
    }
    
    /**
     * Post type for submissions
     *
     * @return string
     */
    public static function postType()
    {
        return 'submission';
    }

    /**
    * Sanitize param
    *
    * @param string $name to sanitize
    *
    * @return string sanitized value
    */
    static function sanitize($name)
    {
        $suffix = '';
        if (strpos($name, '[]')) {
            $suffix = '[]';
        }

        return self::prefix() . crc32($name) . $suffix . '!';
    }
}

/**
* Including api hook class
*
* @include api class *
*/
