<?php
    
    /**
     * Plugin Name: NG Theme Options
     * Plugin URI: https://nikita.global
     * Description: Easily create and use theme options
     * Author: Nikita Menshutin
     * Version: 1.0
     * Text Domain: NgThemeOptions
     * Domain Path: languages
     *
     * PHP version 7.2
     *
     * @category NikitaGlobal
     * @package  NikitaGlobal
     * @author   Nikita Menshutin <nikita@nikita.global>
     * @license  https://nikita.global commercial
     * @link     https://nikita.global
     * */
    defined('ABSPATH') or die("No script kiddies please!");
if (! class_exists("NgThemeOptions")) {
    /**
         * Our main class goes here
         *
         * @category NikitaGlobal
         * @package  NikitaGlobal
         * @author   Nikita Menshutin <wpplugins@nikita.global>
         * @license  http://opensource.org/licenses/gpl-license.php GNU License
         * @link     https://nikita.global
         */
    Class NgThemeOptions
    {
            
        /**
             * Construct
             *
             * @return void
             **/
        public function __construct()
        {
            $this->prefix     = self::prefix();
            $this->version    = self::version();
            $this->pluginName = __('NG Theme Options');
            $this->options    = get_option($this->prefix);
            add_action(
                'customize_register',
                array($this, 'customizeRegister'),
                1000
            );
        }
    
        /**
         * Register theme mods
         *
         * @param object $wp_customize wp_customize
         *
         * @return void
         */
        public function customizeRegister($wp_customize)
        {
            $settings = apply_filters(strtolower($this->prefix), array());
            if (empty($settings)) {
                return;
            }
            foreach ($settings as $setting) {
                $setting = array_merge(
                    array(
                    'control'    => 'text',
                    'section'    => 'title_tagline',
                    'id'         => $this->_sanitize($setting['label']),
                    'type'       => 'theme_mod',
                    'priority'   => 50,
                    'settings'   => $setting['id'],
                    //          'capability'=>'theme_mod',
                    'capability' => 'edit_theme_options',
                    'transport'  => 'refresh'
                        ), $setting
                );
                $method  = '_register' . $setting['control'];
                if (! method_exists($this, $method)) {
                    continue;
                }
                    
                    
                $setting = $this->_registerSection($setting, $wp_customize);
                    
                    
                $wp_customize->add_setting(
                    $setting['id'],
                    $setting
                );
                $this->{$method}($setting, $wp_customize);
            }
        }
    
        /**
         * Register sections if missing
         *
         * @param array  $settings     settings
         * @param object $wp_customize wp_customize
         *
         * @return array settings
         */
        private function _registerSection($settings, $wp_customize)
        {
            $sections = (array_keys($wp_customize->sections()));
            if (in_array($settings['section'], $sections)) {
                return $settings;
            }
            $label               = $settings['section'];
            $settings['section'] = $this->_sanitize($settings['section']);
            if (in_array($settings['section'], $sections)) {
                return $settings;
            }
            $wp_customize->add_section(
                $settings['section'], array(
                'title'    => __($label, $this->prefix),
                'priority' => 500
                    )
            );
                
            return $settings;
        }
    
        /**
         * Textarea control
         *
         * @param array  $setting      settings
         * @param object $wp_customize wp_customize
         *
         * @return void
         */
        private function _registerTextArea($setting, $wp_customize)
        {
            $wp_customize->add_control(
                new WP_Customize_Control(
                    $wp_customize,
                    $setting['id'],
                    array(
                        'label'    => $setting['label'],
                        //Admin-visible name of the control
                        'settings' => $setting['id'],
                        //Which setting to load and manipulate (serialized is okay)
                        'priority' => $setting['priority'],
                        //Determines the order this control
                        // appears in for the specified section
                        'section'  => $setting['section'],
                        'type' => 'textarea',
                        //ID of the section this control should render in
                        // (can be one of yours, or a WordPress default section)
                        )
                )
            );
        }
        
        /**
         * Text control
         *
         * @param array  $setting      settings
         * @param object $wp_customize wp_customize
         *
         * @return void
         */
        private function _registerText($setting, $wp_customize)
        {
            $wp_customize->add_control(
                new WP_Customize_Control(
                    $wp_customize,
                    $setting['id'],
                    array(
                    'label'    => $setting['label'],
                    //Admin-visible name of the control
                    'settings' => $setting['id'],
                    //Which setting to load and manipulate (serialized is okay)
                    'priority' => $setting['priority'],
                    //Determines the order this control appears
                        // in for the specified section
                    'section'  => $setting['section'],
                    //ID of the section this control should render
                        // in (can be one of yours, or a WordPress default section)
                    )
                )
            );
        }
    
        /**
         * Color control
         *
         * @param array  $setting      settings
         * @param object $wp_customize wp_customize
         *
         * @return void
         */
        private function _registerColor($setting, $wp_customize)
        {
            $wp_customize->add_control(
                new WP_Customize_Color_Control( //Instantiate the color control class
                    $wp_customize, //Pass the $wp_customize object (required)
                    $setting['id'], //Set a unique ID for the control
                    array(
                        'label'    => $setting['label'],
                        //Admin-visible name of the control
                        'settings' => $setting['id'],
                        //Which setting to load and manipulate (serialized is okay)
                        'priority' => $setting['priority'],
                        //Determines the order this control appears
                        // in for the specified section
                        'section'  => $setting['section'],
                        //ID of the section this control should render in
                        // (can be one of yours, or a WordPress default section)
                        )
                )
            );
        }
    
        /**
         * Sanitize
         *
         * @param string $string to sanitize
         *
         * @return string sanitized
         */
        private function _sanitize($string)
        {
            return $this->prefix . crc32($string);
        }
            
        /**
             * Method returns prefix
             *
             * @return string prefix
             **/
        public static function prefix()
        {
            return 'NgThemeOptions';
        }
            
        /**
             * Method returns plugin version
             *
             * @return string version
             **/
        public static function version()
        {
            return '1.0';
        }
    }
        
    new NgThemeOptions();
}
