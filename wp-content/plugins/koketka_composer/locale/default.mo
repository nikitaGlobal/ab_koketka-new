��          |      �             !     8     V     g  !   �  v   �  �        �  a   �     F  f   b  �  �     �     �     �     �  !   �  v   !  �   �     G  a   b     �  f   �                     
                               	       About Koketka Composer Edit %s with Koketka Composer Koketka Composer Koketka Composer Settings Koketka Composer Template Library Note: If you are using non-latin characters be sure to activate them under Settings/Koketka Composer/General Settings. Responsive design settings are currently disabled. You can enable them in Koketka Composer <a href="%s">settings page</a> by unchecking "Disable responsive content elements". Show only Koketka Composer There are several resources available to Koketka Composer users to help you to get around plugin: Welcome to Koketka Composer You have an outdated version of Koketka Composer Design Options. It is required to review and save it. Project-Id-Version: Koketka Composer
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-11-09 10:19+0200
PO-Revision-Date: 2017-04-08 13:46+0300
Last-Translator: Michael M <hello@wpbakery.com>
Language-Team: Webcom
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;esc_attr__
X-Poedit-Basepath: ..
X-Generator: Poedit 2.0
Language: en
X-Poedit-SearchPath-0: ./config/.
X-Poedit-SearchPath-1: ./include/.
 About Koketka Composer Edit %s with Koketka Composer Koketka Composer Koketka Composer Settings Koketka Composer Template Library Note: If you are using non-latin characters be sure to activate them under Settings/Koketka Composer/General Settings. Responsive design settings are currently disabled. You can enable them in Koketka Composer <a href="%s">settings page</a> by unchecking "Disable responsive content elements". Show only Koketka Composer There are several resources available to Koketka Composer users to help you to get around plugin: Welcome to Koketka Composer You have an outdated version of Koketka Composer Design Options. It is required to review and save it. 