var ngblocksparams = {debug: false};
if (typeof ngblocks.__params != "undefined") {
    ngblocksparams = ngblocks.__params;
}
for (let i in ngblocks) {

    if (i == '__params') {

        continue;
    }
    ngBlockRegisterBlock(ngblocks[i]);
}

function ngBlockRegisterBlock(block) {
    if (ngblocksparams.debug) {
        //console.log(block);
        for (let i in block.fields) {
            block.fields[i].label += ' {{block.' + block.fields[i].name + '}}';
        }
    }
    var params = {
        title: block.block.title,
        icon: block.block.icon,
        category: block.block.category,
        attributes: ngBlockAttributes(block),
        edit: function (props) {
            var id = props.clientId;
            if (ngblocksparams.debug) {
                jQuery('[data-block="' + id + '"] .nglegend').remove();
                jQuery('[data-block="' + id + '"]').prepend('<span class="nglegend">' + block.block.template + '</span>');
            }
            if (props.isSelected) {
                jQuery('[data-block="' + id + '"] .ngBlockContent').hide();
                jQuery('[data-block="' + id + '"] .ngBlockEdit').show();
            } else {
                jQuery('[data-block="' + id + '"] .ngBlockEdit').hide();
                jQuery('[data-block="' + id + '"] .ngBlockContent').show();
            }


            function updateByName(event) {
                props.setAttributes({[event.target.name]: event.target.value});
            }

            ngUpdateBlock(props);
            var out = [];
            for (let i in block.fields) {
                var field = block.fields[i];
                var basics = {
                    type: field.type,
                    name: field.name,
                    style: {width: "100%"},
                    placeholder: field.label,
                    default: field.default,
                    value: props.attributes[field.name],
                    onChange: updateByName,
                    onCreate: updateByName,
                };
                if (typeof field.value == undefined && basics.default) {
                    block.fields[i].value = basics.default;
                    field.value = basics.default;
                }
                if (basics.default!==false && document.getElementsByName(field.name).length == 0 ) {
                    basics.value = basics.default;
                }
                out.push(React.createElement("span", {class: 'ngBlockEditLabel'}, field.label));
                if (typeof window['makeElement' + field.type] == 'function') {
                    out.push(window['makeElement' + field.type](basics, props));
                } else {
                    out.push(React.createElement("input", basics));
                }
            }
            return [React.createElement(
                "div",
                {
                    class: 'ngBlockEdit'

                }, out),
                React.createElement(
                    "div",
                    {
                        class: 'ngBlockContent',
                        /*onClick:()=>{
                            console.log('click');
                        }*/
                    })];

        },
        save: function (props) {
            props.attributes['blockName'] = block.block.template;
            return wp.element.createElement(
                "span",
                {
                    'data-ngblock': props.attributes['blockName']
                },

                ''
            );
        }

    };
    wp.blocks.registerBlockType(block.block.template, params);
}

function ngBlockAttributes(block) {
    var attribs = {template: block.block.template};
    for (let i in block.fields) {
        var field = block.fields[i];
        switch (field.type) {
            case 'images':
                var attr = {type: 'array', 'default': []};
                break;
            default:
                var attr = {type: 'string'};
        }
        attribs[field.name] = attr;

    }

    return attribs;
}

function makeElementimages(params, props) {
    return React.createElement(wp.blockEditor.MediaUpload, {
        multiple: true,
        allowedTypes: ['image'],
        gallery: true,
        name: params.name,
        addTogallery: false,
        type: 'image',
        render: function (obj) {
            return React.createElement(window.wp.components.Button, {
                    className: 'components-icon-button image-block-btn is-button is-default is-large',
                    onClick: obj.open
                },
                React.createElement('svg', {
                        className: 'dashicon dashicons-edit',
                        width: '20',
                        height: '20'
                    },
                    React.createElement('path', {d: "M2.25 1h15.5c.69 0 1.25.56 1.25 1.25v15.5c0 .69-.56 1.25-1.25 1.25H2.25C1.56 19 1 18.44 1 17.75V2.25C1 1.56 1.56 1 2.25 1zM17 17V3H3v14h14zM10 6c0-1.1-.9-2-2-2s-2 .9-2 2 .9 2 2 2 2-.9 2-2zm3 5s0-6 3-6v10c0 .55-.45 1-1 1H5c-.55 0-1-.45-1-1V8c2 0 3 4 3 4s1-3 3-3 3 2 3 2z"})
                ),
                React.createElement('span', {},
                    'Select image'
                ),
            );
        },
        value: props.attributes[params.name],
        onSelect: function imageSelected(images) {
            var imgArray = [];
            for (let i in images) {
                imgArray.push(images[i].id);
            }
            return props.setAttributes({
                [params.name]: imgArray
            });
        },
        title: params.type,
    });
}

function makeElementtextarea(params, props) {
    return React.createElement("textarea", params);
}

function makeElementwysiwyg(params, props) {
    params['keepPlaceholderOnFocus'] = true;
    params['onChange'] = function (val) {
        props.setAttributes({[params.name]: val})
    };
    return React.createElement(wp.editor.RichText, params);
}

function ngUpdateBlock(props) {
    if (typeof props.attributes.blockName == "undefined") {
//        var props.attributes['blockName']=props.name;
        props.attributes.blockName = props.name;
    }
    var formData = new FormData();
    formData.append('action', 'renderblock');
    formData.append('data', JSON.stringify(props.attributes, null, '\t'));
    // console.log(formData);
    var id = props.clientId;
    fetch(ajaxurl, {
        method: 'POST',
        //   body: 'action=renderblock&data=' + JSON.stringify(props.attributes),
        body: formData,
        headers: {
            //       'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
            //        'Content-Type': 'multipart/form-data'
        },
    }).then(response => {
        response.json().then(data => {
            if (!data) {
                jQuery('[data-block="' + id + '"]').find('.ngBlockContent').html('loading...');
                return;
            }
            jQuery('[data-block="' + id + '"]').find('.ngBlockContent').html('<object></object>');
            jQuery('[data-block="' + id + '"]').find('.ngBlockContent object').html(data);
        })
    });

}
