<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
preg_match( '/^(\d+)(\.\d+)?/', WPB_VC_VERSION, $matches );
?>
<div class="wrap vc-page-welcome about-wrap">
	<h1><?php echo sprintf( __( 'Welcome to Webcom Composer %s', 'js_composer' ), isset( $matches[0] ) ? $matches[0] : WPB_VC_VERSION ) ?></h1>

	
</div>
