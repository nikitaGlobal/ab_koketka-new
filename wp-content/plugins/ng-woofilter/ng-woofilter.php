<?php
    
    /**
     * Plugin Name: NG WooFilter
     * Plugin URI: https://nikita.global
     * Description: Make your own woocommerce filter easily in theme
     * Author: Nikita Menshutin
     * Version: 1.0
     * Text Domain: ng-woofilter
     * Domain Path: languages
     *
     * PHP version 7.2
     *
     * @category NikitaGlobal
     * @package  NikitaGlobal
     * @author   Nikita Menshutin <nikita@nikita.global>
     * @license  https://nikita.global commercial
     * @link     https://nikita.global
     * */
    defined('ABSPATH') or die("No script kiddies please!");
    if ( ! class_exists("NGWooFilter")) {
        /**
         * Our main class goes here
         *
         * @category NikitaGlobal
         * @package  NikitaGlobal
         * @author   Nikita Menshutin <wpplugins@nikita.global>
         * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
         * @link     https://nikita.global
         */
        Class NGWooFilter
        {
            
            /**
             * Construct
             *
             * @return void
             **/
            public function __construct()
            {
                $this->prefix     = self::prefix();
                $this->version    = self::version();
                $this->pluginName = __('NG WooFilter');
                add_action('admin_enqueue_scripts', array($this, 'getAttributes'));
                add_action('init', array($this, 'getAttributes'));
                $this->options = get_option($this->prefix);
                global $wpdb;
                $this->wpdb = $wpdb;
                load_plugin_textdomain(
                    $this->prefix,
                    false,
                    $this->prefix . '/languages'
                );
                //add_action('init', array($this, 'getFilterParams'));
                add_filter('NGWooFilterparams',
                    array($this, 'getFilterParams'));
                add_filter($this->prefix . 'pricerange',
                    array($this, 'getPriceRange'));
                //      $this->getFilterParams();
                $this->settings = array(
                    'showPrice'         => array(
                        'key'         => 'showPrice',
                        'title'       => __('Show price'),
                        'group'       => '',
                        'type'        => 'checkbox',
                        'placeholder' => __('placeholder 2'),
                        /*'default'     => 'default 2'*/
                    ),
                    'productAttributes' => array(
                        'key'     => 'productAttributes',
                        'title'   => __('Select product attributes'),
                        'group'   => '',
                        'type'    => 'multiselect',
                        'default' => false,
                        'args'    => $this->_getAttributes()
                    )
                );
                foreach ($this->settings as $k => $setting) {
                    if (isset($this->options[$setting['key']])) {
                        $this->settings[$k]['value'] = $this->options[$setting['key']];
                    }
                }
                add_filter('woocommerce_product_query', array($this, 'preGetPosts'),
                    20);
                add_action('wp_enqueue_scripts', array($this, 'scripts'));
                add_action('admin_menu', array($this, 'menuItemAdd'));
                add_action('admin_init', array($this, 'settingsRegister'));
                add_filter(
                    'plugin_action_links',
                    array(
                        $this,
                        'pluginActionLinks'
                    ), 10, 2
                );
                
            }
            
            /**
             * Register settings
             *
             * @return void
             **/
            public function settingsRegister()
            {
                register_setting($this->prefix, $this->prefix);
                $groups = array();
                foreach ($this->settings as $settingname => $array) {
                    if ( ! in_array($array['group'], $groups)) {
                        add_settings_section(
                            $this->prefix . $array['group'],
                            __($array['group'], $this->prefix),
                            array($this, 'sectionCallBack'),
                            $this->prefix
                        );
                        $this->groups[] = $array['group'];
                    }
                    add_settings_field(
                        $array['key'],
                        __($array['title'], $this->prefix),
                        array($this, 'makeField'),
                        $this->prefix,
                        $this->prefix . $array['group'],
                        $array
                    );
                }
            }
            
            public function getFilterParams($array = array())
            {
                global $wp_query;
                if (isset($this->filterParams) && $wp_query->get_queried_object()) {
                    return $this->filterParams;
                }
                $tax                = get_option($this->prefix . 'attr');
                $array              = array(
                    'prefix'    => $this->prefix,
                    'url'       => remove_query_arg('paged',
                        $_SERVER['REQUEST_URI']),
                    'showprice' => $this->options['showPrice']
                );
                $this->filterParams = $array;
                
                $params                       = array();
                $this->filterParams['values'] = array();
                foreach ($tax as $key => $value) {
                    if ( ! in_array($key, $this->options['productAttributes'])) {
                        continue;
                    }
                    $this->filterParams['taxonomies'][$key] = $value;
                    $params[$key]                           = array(
                        'label'     => $value->label,
                        'values'    => $this->_getFilterParamValues($key),
                        'field'     => $this->prefix . '[terms][' . $key . '][]',
                        'fieldtype' => 'select',
                        'taxonomy'  => $key
                    );
                    
                    $params[$key]['value'] = array();
                    if (isset($_REQUEST[$this->prefix][$key])) {
                        $params[$key]['value'][] = $_REQUEST[$this->prefix][$key];
                        
                        // $this->filterParams['values'][] = $_REQUEST[$this->prefix][$key];
                    }
                }
                $this->filterParams['params']      = $params;
                $this->filterParams['priceMinMax'] = $this->_getPriceMinMax();;
                if (isset($_REQUEST[$this->prefix]['price'][0])) {
                    $this->filterParams['priceRange'] = explode(',',
                        $_REQUEST[$this->prefix]['price'][0]);
                } else {
                    //  $this->filterParams['priceRange']=$this->filterParams['PriceMinMax'];
                }
                $cachekey    = self::sanitize($this->filterParams);
                $cachedvalue = get_transient($cachekey);
                if ($cachedvalue) {
                    $this->filterParams = $cachedvalue;
                } else {
                    $this->_filterSetPrice();
                    //      $this->_filterSetTax();
                    $this->_filterParamsPopullateResults();
                    $this->_filterParamsRemoveSingles();
                    set_transient($cachekey, $this->filterParams);
                }
                
                return $this->filterParams;
            }
            
            private function _filterSetPrice()
            {
                $this->filterParams['priceRange'] = false;
                if ( ! $this->options['showPrice']) {
                    return;
                }
                $this->filterParams['priceMinMax'] = $this->_getPriceMinMax();
                if (isset($_REQUEST[$this->prefix]['price'][0])) {
                    $this->filterParams['priceRange'] = explode(',',
                        $_REQUEST[$this->prefix]['price'][0]);
                } else {
                    $this->filterParams['priceRange'] = $this->filterParams['priceMinMax'];
                }
                $this->filterParams['params']['price'] = array(
                    'label'     => __('Price', 'woocommerce'),
                    'values'    => $this->filterParams['priceRange'],
                    'field'     => $this->prefix . '[price][]',
                    'fieldtype' => 'range'
                );
            }
            
            private function _getFilterParamValues($key)
            {
                $out = array();
                foreach ($this->filterParams['taxonomies'][$key]->terms as $term) {
                    $out[$term->term_id] = array(
                        'term'     => $term,
                        'name'     => $term->name,
                        'id'       => $term->term_id,
                        'parent'   => $term->parent,
                        'selected' => false
                    );
                    if ( ! isset($_REQUEST[$this->prefix]['terms'][$key])) {
                        continue;
                    }
                    $passedvalues                       = $_REQUEST[$this->prefix]['terms'][$key];
                    $this->filterParams['values'][$key] = $passedvalues;
                    if (in_array($term->term_id, $passedvalues)) {
                        $out[$term->term_id]['selected'] = true;
                    }
                }
                
                return $out;
            }
            
            
            private function _filterSetTaxTerm($value)
            {
                return true;
            }
            
            private function _filterParamsPopullateResults()
            {
                foreach ($this->filterParams['params'] as $k => $value) {
                    if ( ! isset($value['taxonomy']) || ! $value['taxonomy']) {
                        continue;
                    }
                    foreach ($value['values'] as $term) {
                        if (in_array($term['id'], $value['value'])) {
                            continue;
                        }
                        $try       = $this->filterParams['values'];
                        $try[$k][] = $term['id'];
                        if (isset($this->filterParams['values'][$k])) {
                            $try[$k]=array($term['id']);
                        }
                        $res       = $this->_queryResults($try,
                        $this->filterParams['priceRange']);
                        $count     = $res['count'];
                        if ($term['id'] == 430) {
                            $b = 1;
                        }
                        if ($count <= 1) {
                            unset($this->filterParams['params'][$k]['values'][$term['id']]);
                            continue;
                        }
                        $this->filterParams['params'][$k]['values'][$term['id']]['count'] = $count;
                    }
                }
            }
            
            private function _filterParamsRemoveSingles()
            {
                foreach ($this->filterParams['params'] as $k => $value) {
                    if (count($this->filterParams['params'][$k]['values']) <= 1) {
                        unset($this->filterParams['params'][$k]);
                    }
                }
            }
            
            private function _queryResults($params, $pricerange = false)
            {
                $cachekey   = self::sanitize(array($params, $pricerange));
                $cachevalue = get_transient($cachekey);
                if ($cachevalue) {
                    return $cachevalue;
                }
                $out   = array();
                $q     = '';
                $query = 'SELECT object_id, term_taxonomy_id  FROM `' . $this->wpdb->prefix . 'term_relationships` ';
                $q     .= $query;
                global $wp_query;
                $cat = $wp_query->get_queried_object();
                if (isset($cat->term_id)) {
                    $params[$cat->taxonomy][] = ($cat->term_id);
                }
                $joinParam = ' WHERE (';
                foreach ($params as $taxonomy => $terms) {
                    $q         .= $joinParam;
                    $q         .= ' term_taxonomy_id IN (' . implode(',',
                            $terms) . ')';
                    $joinParam = ' OR ';
                }
                $q           .= ')';
                $q           .= ' AND (object_id IN (SELECT ID FROM `' . $this->wpdb->prefix . 'posts` WHERE post_status="publish"))';
                $q           .= $this->_ANDpriceRange($pricerange);
                $res         = $this->_filterNotMatching($this->_query($q),
                    $params);
                $out['rows'] = $res;
                // $out['rowsPriceApplied'] = $priceFilter['rows'];
                $out['count'] = count($out['rows']);
                set_transient($cachekey, $out);
                
                return $out;
            }
            
            private function _filterNotMatching($result, $params)
            {
                $values = array();
                $keep   = array();
                foreach ($result as $row) {
                    $values[$row->object_id][] = $row;
                    if (count($values[$row->object_id]) == count($params)) {
                        $keep[$row->object_id] = $row;
                    }
                }
                
                return $keep;
            }
            
            private function _ANDpriceRange($pricerange)
            {
                if ( ! $pricerange) {
                    return '';
                }
                
                return ' AND (object_id IN (SELECT post_id FROM `' . $this->wpdb->prefix . 'postmeta` WHERE meta_key="_price" AND meta_value BETWEEN ' .
                       implode(' AND ', $pricerange)
                       . '))';
            }
            
            private function _filterTax($res, $params)
            {
                $rows = array();
                foreach ($res as $row) {
                    $rows[$row->object_id][] = $row->term_taxonomy_id;
                }
                foreach ($rows as $k => $v) {
                    if (count($v) !== count($params)) {
                        unset($rows[$k]);
                    }
                }
                
                return $rows;
            }
            
            public function getPriceRange($values)
            {
                if ( ! isset($this->filterParams['priceMinMax'])) {
                    $this->getFilterParams();
                }
                
                return $this->filterParams['priceMinMax'];
            }
            
            public function _query($query)
            {
                $cachekey   = self::sanitize($query);
                $cachevalue = get_transient($cachekey);
                if ($cachevalue) {
                    return $cachevalue;
                }
                $cachevalue = $this->wpdb->get_results($query);
                set_transient($cachekey, $cachevalue);
                
                return $cachevalue;
            }
            
            private function _priceFilter($rows, $params = false)
            {
                
                $out = array('rows' => $rows, 'priceRange' => false, 'count' => 0);
                if ( ! $this->options['showPrice']) {
                    return $out;
                }
                $prices = array();
                if ( ! $rows || ! is_array($rows) || empty($rows)) {
                    return $out;
                }
                foreach ($rows as $productid => $values) {
                    $prod = wc_get_product($productid);
                    if ( ! $prod->is_in_stock()) {
                        unset($rows[$productid]);
                        continue;
                    }
                    $price = (int)$prod->get_price();
                    if ($price < $params[0] || $price > $params[1]) {
                        unset($rows[$productid]);
                    }
                    $prices[] = $price;
                }
                $out['rows'] = $rows;
                sort($prices);
                $lastindex = count($prices) - 1;
                if ( ! isset($this->priceRange)) {
                    $this->priceRange = array($prices[0], $prices[$lastindex]);
                }
                if ($this->priceRange[0] > $prices[0]) {
                    $this->priceRange[0] = $prices[0];
                }
                if ($this->priceRange[1] < $prices[$lastindex]) {
                    $this->priceRange[1] = $prices[$lastindex];
                }
                
                return $out;
            }
            
            private function _getPriceMinMax()
            {
                global $wp_query;
                $cat = $wp_query->get_queried_object()->term_id;
                $b   = 1;
                
                return array(
                    $this->_queryCatPrice($cat, 'min'),
                    $this->_queryCatPrice($cat, 'max')
                );
            }
            
            private function _queryCatPrice($cat, $type)
            {
                $parts = array(
                    'min' => array('FLOOR', 'ASC'),
                    'max' => array('CEIL', 'DESC')
                );
                $part  = $parts[$type];
                $query = 'SELECT ' . $part[0] . '(meta_value) as price FROM ' . $this->wpdb->prefix . 'postmeta WHERE meta_key="_price" and post_id IN (SELECT object_id FROM `' . $this->wpdb->prefix . 'term_relationships` WHERE term_taxonomy_id=' . (int)$cat . ' AND object_id IN (SELECT ID FROM `' . $this->wpdb->prefix . 'posts` WHERE post_status="publish" AND post_type="product")) ORDER BY price ' . $part[1] . ' LIMIT 1';
                $var   = $this->wpdb->get_var($query);
                
                return $var;
            }
            
            public function preGetPosts($query)
            {
                if (is_admin() || ! $query->is_main_query()) {
                    return $query;
                }
                if ($query->is_single() || $query->is_page()) {
                    return $query;
                }
                if ( ! $query->is_archive) {
                    return $query;
                }
                $query->set('posts_per_page', 15);
                if ( ! isset($_GET[$this->prefix])) {
                    return $query;
                }
                if (isset($_GET[$this->prefix]['terms'])) {
                    $tax_query = array('relation' => 'AND');
                    foreach ($_GET[$this->prefix]['terms'] as $k => $v) {
                        if (is_array($v)) {
                            //             if (false !== strpos($k, 'pa_')) {
                            $tax_query[] = array(
                                'taxonomy' => $k,
                                'field'    => 'id',
                                'terms'    => $v,
                                'operator' => 'IN'
                            );
                            //           }
                            //continue;
                        }
                    }
                    $query->set('tax_query', $tax_query);
                }
                if (isset($_GET[$this->prefix]['price'])) {
                    $priceRange = explode(',', $_GET[$this->prefix]['price'][0]);
                    $args       = array(
                        array(
                            'key'     => '_price',
                            'value'   => array($priceRange[0], $priceRange[1]),
                            'compare' => 'BETWEEN',
                            'type'    => 'NUMERIC'
                        )
                    );
                    $query->set('meta_query', $args);
                }
                
                return $query;
            }
            
            /**
             * Options page in settings
             *
             * @return void
             **/
            public function menuItemAdd()
            {
                add_submenu_page(
                    'woocommerce',
                    $this->pluginName,
                    $this->pluginName,
                    'manage_options',
                    $this->prefix,
                    array($this, 'optionsPage')
                );
            }
            
            /**
             * Backend options options page
             *
             * @return void
             **/
            public function optionsPage()
            {
                ?>
                <form action='options.php' method='post'>
                <h2><?php echo $this->pluginName; ?></h2>
                <?php
                settings_fields($this->prefix);
                do_settings_sections($this->prefix);
                submit_button();
                ?></form><?php
            }
            
            /**
             * Settings field - default
             *
             * @param array $args arguments
             *
             * @return void
             **/
            public function makeField($args)
            {
                $methodName = 'makeField' . $args['type'];
                if (method_exists($this, $methodName)) {
                    return $this->{$methodName}($args);
                }
                echo '<input ';
                echo ' class="regular-text"';
                echo ' type="';
                echo $args['type'];
                echo '"';
                echo $this->_makeFieldAttr($args);
                echo ' value="';
                if (isset($args['value'])) {
                    echo $args['value'];
                } else {
                    if (isset($args['default'])) {
                        echo $args['default'];
                    }
                }
                echo '"';
                echo '>';
            }
            
            /**
             * Settings field - checkbox
             *
             * @param array $args arguments
             *
             * @return void
             **/
            public function makeFieldCheckBox($args)
            {
                echo '<input type="checkbox" value="1"';
                echo $this->_makeFieldAttr($args);
                if (isset($this->options[$args['key']])
                    && $this->options[$args['key']]
                ) {
                    echo 'checked';
                }
                
                echo '>';
            }
            
            public function getAttributes()
            {
                if ( ! is_admin()) {
                    return;
                }
                $args       = array(
                    'name' => array(
                        'product',
                    ),
                );
                $tax        = array();
                $taxonomies = wc_get_attribute_taxonomies();
                foreach ($taxonomies as $taxonomy) {
                    $tax[] = wc_get_attribute($taxonomy->attribute_id);
                }
                //   $alltax=get_taxonomies();
                $alltax          = get_object_taxonomies('product');
                $taxonomyDetails = array();
                foreach ($alltax as $value) {
                    $taxonomy                       = get_taxonomy($value);
                    $taxonomyDetails[$value]        = $taxonomy;
                    $taxonomyDetails[$value]->terms = get_terms(array(
                        'taxonomy'   => $taxonomy->name,
                        'hide_empty' => true
                    ));
                }
                update_option($this->prefix . 'attr', $taxonomyDetails, false);
            }
            
            public function _getAttributes()
            {
                //  $res=$wpdb->get_results('SELECT * FROM `'.$wpdb->prefix.'term_taxonomy` WHERE taxonomy like "pa_%" GROUP BY taxonomy');
                $tax = get_option($this->prefix . 'attr');
                $out = array();
                foreach ($tax as $value) {
                    $out[$value->name] = $value->label . ' (' . $value->name . ')';
                }
                
                return $out;
            }
            
            /**
             * Settings field select
             *
             * @param array $args settings arguments
             *
             * @return void
             */
            public function makeFieldMultiSelect($args)
            {
                echo '<select ';
                echo $this->_makeFieldAttr($args);
                echo ' multiple>';
                echo $this->_makeFieldSelectOptions($args);
                echo '</select>';
            }
            
            /**
             * Settings field select
             *
             * @param array $args settings arguments
             *
             * @return void
             */
            public function makeFieldSelect($args)
            {
                echo '<select ';
                echo $this->_makeFieldAttr($args);
                echo '>';
                echo $this->_makeFieldSelectOptions($args);
                echo '</select>';
            }
            
            /**
             * Settings field select - options tags
             *
             * @param array $args settings arguments
             *
             * @return void
             */
            private function _makeFieldSelectOptions($args)
            {
                foreach ($args['args'] as $k => $v) {
                    echo '<option ';
                    echo 'value="' . $k . '" ';
                    if (isset($args['value'])) {
                        if (is_array($args['value'])) {
                            if (in_array($k, $args['value'])) {
                                echo 'selected ';
                            }
                        } else {
                            if ($args['value'] == $k) {
                                echo 'selected ';
                            }
                        }
                    }
                    echo ">";
                    _e($v, $this->prefix);
                    echo '</option>';
                    
                }
            }
            
            /**
             * Name and Required attribute for field
             *
             * @param array $args arguments
             *
             * @return void
             **/
            private function _makeFieldAttr($args)
            {
                $brackets = '';
                if ($args['type'] == 'multiselect') {
                    $brackets = '[]';
                }
                echo " name=\"";
                echo $this->prefix . '[';
                echo $args['key'] . ']' . $brackets . '" ';
                if (isset($args['placeholder'])) {
                    echo ' placeholder="';
                    echo __($args['placeholder'], $this->prefix) . '"';
                }
                if (isset($args['required']) && $args['required']) {
                    echo ' required="required"';
                }
            }
            
            /**
             * Enqueue scripts
             *
             * @return void
             **/
            public function scripts()
            {
                return;
                wp_register_script(
                    $this->prefix,
                    plugin_dir_url(__FILE__) . '/plugin.js',
                    array('jquery'),
                    $this->version,
                    true
                );
                wp_localize_script(
                    $this->prefix,
                    $this->prefix,
                    array(
                        'ajax_url' => admin_url('admin-ajax.php')
                    )
                );
                wp_enqueue_script($this->prefix);
            }
            
            /**
             * Output under sectionCallBack
             *
             * @return void
             **/
            public function sectionCallBack()
            {
                echo '<hr>';
            }
            
            /**
             * Link to settings page from plugins list page
             *
             * @param array $links links
             * @param string $file plugin file
             *
             * @return array links
             */
            public function pluginActionLinks($links, $file)
            {
                if ($file == plugin_basename(__FILE__)) {
                    $settings_link = '<a href="' . admin_url(
                            'options-general.php?page=' . $this->prefix
                        ) . '">' . __('Settings') . '</a>';
                    array_unshift($links, $settings_link);
                }
                
                return $links;
            }
            
            /**
             * Sanitize values
             *
             * @param any $values
             *
             * @return string
             */
            public static function sanitize($values)
            {
                return self::prefix() . crc32(serialize($values));
            }
            
            /**
             * Method returns prefix
             *
             * @return string prefix
             **/
            public static function prefix()
            {
                return 'NGWooFilter';
            }
            
            /**
             * Method returns plugin version
             *
             * @return string version
             **/
            public static function version()
            {
                return '1.0';
            }
        }
    }
    new NGWooFilter();