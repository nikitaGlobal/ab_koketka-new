jQuery('.woocommerce-ordering_').on('change', 'select', function () {
    ngupdateFilter();
});
jQuery('.woocommerce-ordering_').on('change', 'input', function () {

    if (!jQuery(this).hasClass('nginit')) {
        jQuery(this).addClass('nginit');
        return;
    }
    ngupdateFilter();
});
function ngupdateFilter()
{
    if (typeof ngupdateTimer != undefined) {
        clearTimeout(ngupdateTimer);
    }
    var ngupdateTimer = setTimeout(function () {
        jQuery('#ngsearchSubmit').click();
    }, 1500);
}

jQuery(document).ready(function () {
    setTimeout(function () {
        noticespopup();
        jQuery('.banner_arrow_prev img').attr('src', nglink + '/assets/images/slide-prev.svg');
        jQuery('.banner_arrow_next img').attr('src', nglink + '/assets/images/slide-next.svg');

        jQuery('.select_wrap .dropdown-toggle').on('click', function () {
            $('select').not('[multiple="multiple"]').selectpicker('refresh');
        });
        if (jQuery('.s-shopping_cart').length) {
            jQuery('.dropdown-toggle').on('click', function (){
                //            $('select').not('[multiple="multiple"]').selectpicker('refresh');
            });
            jQuery('body').on('ngqtyupdated', function(){
                jQuery('[name="update_cart"]').prop('disabled',false)
                if (typeof ngupdateTimer != undefined) {
                    clearTimeout(ngupdateTimer);
                }
                var ngupdateTimer = setTimeout(function () {
                    console.log('click');
                    jQuery('[name="update_cart"]').prop('disabled',false).click();
                }, 1500);
            });
            $('.selectpicker').selectpicker('render');
            jQuery('.woocommerce-cart-form').on('change', 'input', function () {

            });
        }
    }, 500);
});

function noticespopup(){
    if (typeof ngwcnotices == 'undefined') {
        return;
    }
    jQuery('#ngnotice .ngnoticehere').html(ngwcnotices);
    jQuery.fancybox.open(jQuery('#ngnotice'));
    jQuery('.popup_close').on('click', function () {
        jQuery.fancybox.close();
    });
}