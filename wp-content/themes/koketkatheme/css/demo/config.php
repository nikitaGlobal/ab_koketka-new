<div class="koketka-image-content hide-for-small hide-for-medium"><div class="koketka-image-content-detail" data-stt="0"></div></div>
<div class="config hide-for-small hide-for-medium">
    <div class="config-options">
        <span class="ss-title"><strong>Layout</strong></span>
        <div class="ss-content clearfix">
            <a class="wide-button ss-button active" href="#">Wide</a>
            <a class="boxed-button ss-button" href="#">Boxed</a>
        </div>

        <div class="ss-patterns-content" style="display: none;">
            <span class="ss-title"><strong>Boxed Mode Backgrounds</strong></span>
            <div class="ss-content clearfix">
                <div class="ss-images-patterns">
                    <div class="ss-image ss-image-1" data-pattern="<?php echo get_template_directory_uri().'/css/koketka/images/pattern/bkgd1.jpg';?>"></div>
                    <div class="ss-image ss-image-2" data-pattern="<?php echo get_template_directory_uri().'/css/koketka/images/pattern/bkgd2.jpg';?>"></div>
                    <div class="ss-image ss-image-3" data-pattern="<?php echo get_template_directory_uri().'/css/koketka/images/pattern/bkgd3.jpg';?>"></div>
                    <div class="ss-image ss-image-4" data-pattern="<?php echo get_template_directory_uri().'/css/koketka/images/pattern/bkgd4.jpg';?>"></div>
                    <div class="ss-image ss-image-5" data-pattern="<?php echo get_template_directory_uri().'/css/koketka/images/pattern/bkgd5.jpg';?>"></div>
                    <div class="ss-image ss-image-6" data-pattern="<?php echo get_template_directory_uri().'/css/koketka/images/pattern/bkgd6.jpg';?>"></div>
                    <div class="ss-image ss-pattern-1" data-pattern="<?php echo get_template_directory_uri().'/css/koketka/images/pattern/pattern1.png';?>"></div>
                    <div class="ss-image ss-pattern-2" data-pattern="<?php echo get_template_directory_uri().'/css/koketka/images/pattern/pattern2.png';?>"></div>
                    <div class="ss-image ss-pattern-3" data-pattern="<?php echo get_template_directory_uri().'/css/koketka/images/pattern/pattern3.png';?>"></div>
                    <div class="ss-image ss-pattern-4" data-pattern="<?php echo get_template_directory_uri().'/css/koketka/images/pattern/pattern4.png';?>"></div>
                    <div class="ss-image ss-pattern-5" data-pattern="<?php echo get_template_directory_uri().'/css/koketka/images/pattern/pattern5.png';?>"></div>
                    <div class="ss-image ss-pattern-6" data-pattern="<?php echo get_template_directory_uri().'/css/koketka/images/pattern/pattern6.png';?>"></div>
                </div>
            </div>
        </div>

        <span class="ss-title"><strong>Style Examples</strong></span>
        <div class="ss-content clearfix">
            <div class="ss-color-wrapper">
                <div class="ss-color ss-color-1" data-style="<?php echo get_template_directory_uri().'/css/koketka/theme-1.css';?>"></div>
                <div class="ss-color ss-color-2" data-style="<?php echo get_template_directory_uri().'/css/koketka/theme-2.css';?>"></div>
                <div class="ss-color ss-color-3" data-style="<?php echo get_template_directory_uri().'/css/koketka/theme-3.css';?>"></div>
                <div class="ss-color ss-color-4" data-style="<?php echo get_template_directory_uri().'/css/koketka/theme-4.css';?>"></div>
                <div class="ss-color ss-color-5" data-style="<?php echo get_template_directory_uri().'/css/koketka/theme-5.css';?>"></div>
                <div class="ss-color ss-color-6" data-style="<?php echo get_template_directory_uri().'/css/koketka/theme-6.css';?>"></div>
            </div>
        </div>

        <span class="ss-title"><strong>Theme koketka (13 koketkas Included!)</strong></span>
        
        <div class="ss-koketka clearfix">

            <div class="ss-koketka-1 lt-items-koketka large-6">
                <div class="ss-koketka-wrapper">
                    <h3>Home Digital</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-digital.jpg';?>">
                        <a class="hover-overlay link-koketka-home-digital" data-stt="7" href="/koketka/koketka-digital/">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ss-koketka-2 lt-items-koketka large-6 lt-dm-right">
                <div class="ss-koketka-wrapper">
                    <h3>Home Fashion 1</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-fashion-1.jpg';?>">
                        <a class="hover-overlay link-koketka-home-fashion" data-stt="1" href="/koketka/koketka-fashion/">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>
            
            <div class="ss-koketka-3 lt-items-koketka large-6">
                <div class="ss-koketka-wrapper">
                    <h3>Home Accessories</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-accessories.jpg';?>">
                        <a class="hover-overlay link-koketka-home-accessories" data-stt="5" href="/koketka/koketka-accessories">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ss-koketka-4 lt-items-koketka large-6 lt-dm-right">
                <div class="ss-koketka-wrapper">
                   
                    <h3>Home Furniture</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-furniture.jpg';?>">
                        <a class="hover-overlay link-koketka-home-furniture" data-stt="8" href="/koketka/koketka-furniture/">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>

           <div class="ss-koketka-5 lt-items-koketka large-6">
                <div class="ss-koketka-wrapper">
                   
                    <h3>Home Handtools</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-handtool.jpg';?>">
                        <a class="hover-overlay link-koketka-home-handtools" data-stt="9" href="/koketka/koketka-handtools/">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ss-koketka-6 lt-items-koketka large-6 lt-dm-right">
                <div class="ss-koketka-wrapper">
                    <h3>Home Cosmetics</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-cosmetics.jpg';?>">
                        <a class="hover-overlay link-koketka-home-cosmetics" data-stt="10" href="/koketka/koketka-cosmetics/">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ss-koketka-7 lt-items-koketka large-6">
                <div class="ss-koketka-wrapper">
                    
                    <h3>Home Flower</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-flower.jpg';?>">
                        <a class="hover-overlay link-koketka-home-flower" data-stt="14" href="/koketka/koketka-flower">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ss-koketka-8 lt-items-koketka large-6 lt-dm-right">
                <div class="ss-koketka-wrapper">
                     <h3>Home Kids</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-kids.jpg';?>">
                        <a class="hover-overlay link-koketka-home-kids" data-stt="11" href="/koketka/koketka-kids/">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ss-koketka-9 lt-items-koketka large-6">
                <div class="ss-koketka-wrapper">
                    <h3>Home Pets</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-pets.jpg';?>">
                        <a class="hover-overlay link-koketka-home-pets" data-stt="12" href="/koketka/koketka-pet">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ss-koketka-10 lt-items-koketka large-6 lt-dm-right">
                <div class="ss-koketka-wrapper">
                    <h3>Home Fashion 2</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-fashion-2.jpg';?>">
                        <a class="hover-overlay link-koketka-home-fashion-2" data-stt="2" href="/koketka/koketka-fashion/home-2">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ss-koketka-11 lt-items-koketka large-6 columns">
                <div class="ss-koketka-wrapper">
                    <h3>Home Fashion 3</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-fashion-3.jpg';?>">
                        <a class="hover-overlay link-koketka-home-fashion-3" data-stt="3" href="/koketka/koketka-fashion/home-3">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ss-koketka-12 lt-items-koketka large-6 lt-dm-right">
                <div class="ss-koketka-wrapper">
                     <h3>Home Fashion 4</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-fashion-4.jpg';?>">
                        <a class="hover-overlay link-koketka-home-fashion-4" data-stt="4" href="/koketka/koketka-fashion/home-4">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ss-koketka-14 lt-items-koketka large-6">
                <div class="ss-koketka-wrapper">
                    <h3>Home Jewellry</h3>
                    <div class="ss-koketka-screenshot" data-img="<?php echo get_template_directory_uri().'/css/koketka/images/koketka_pages/home-jewellry.jpg';?>">
                        <a class="hover-overlay link-koketka-home-jewellry" data-stt="6" href="/koketka/koketka-fashion/home-jewellry/">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>       
        </div>

        <span class="ss-title"><strong>Note:</strong></span>
        <div class="ss-desc">
            This is just a koketka. Every color, font, layout etc can completely be customized in a Theme Option
        </div>
        
    </div>
    <a class="show-theme-options" href="javascript:void(0);">
        <center>
            <i class="fa fa-plus"></i><br />
            koketkaS
        </center>
    </a>
		
</div>
