<?php
/*
 * Plugin Name: Econt Express WooCommerce shipping method
 * Plugin URI: https://mreja.net/produkt/woocommerce-econt-express-shipping-plugin/
 * Description: Integrate Econt Express in WooCommerce and ship your goods.
 * Author: MrejaNet
 * Author URI: https://mreja.net
 * Version: 1.1.9
 * Text Domain: woocommerce-econt
 * Domain Path: /lang
 * Tags: woocommerce, econt express, shipping method
 * Requires at least: 4.0.0
 * Tested up to: 5.2.2
 * WC requires at least: 3.0.0
 * WC tested up to: 3.6.5
 * License: Proprietary license
 * License URI: https://mreja.net/software-license-agreement/
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


global $wpdb;

if (!defined('ECONT_PLUGIN_DIR'))
    define( 'ECONT_PLUGIN_DIR', dirname(__FILE__) );

if (!defined('ECONT_PLUGIN_ROOT_PHP'))
    define( 'ECONT_PLUGIN_ROOT_PHP', dirname(__FILE__).'/'.basename(__FILE__)  );

if (!defined('ECONT_PLUGIN_ADMIN_DIR'))
    define( 'ECONT_PLUGIN_ADMIN_DIR', dirname(__FILE__) . '/admin' );

if (!defined('ECONT_PLUGIN_URL'))
    define( 'ECONT_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

if (!defined('ECONT_VERSION_KEY'))
    define('ECONT_VERSION_KEY', 'woocommerce-econt_version');

if (!defined('ECONT_VERSION_NUM'))
    define('ECONT_VERSION_NUM', '1.1.9');

if (!defined('ECONT_LICENSE_SERVER'))
    define('ECONT_LICENSE_SERVER', 'http://licensing.mreja.net/api.php/esm/');


if( !class_exists('Econt_Express') ) {

	class Econt_Express {

		const VERSION = '1.1.9';
		const ECONT_VERSION = '1.1.9';
		const CAPABILITY = "edit_econt_express";
		const DEMO = false;

		public function __construct() {

			require_once(ECONT_PLUGIN_ADMIN_DIR.'/class-sm-econt.php'); //adds shipping method econt to woocommerce settings 
			require_once(ECONT_PLUGIN_DIR.'/inc/class-mysql-econt.php'); //create econt tables and do all mysql queries
			require_once(ECONT_PLUGIN_DIR.'/inc/class-ajax-econt.php'); 
			require_once(ECONT_PLUGIN_DIR.'/inc/class-checkout-econt.php'); //class s funkcii dobavqshti meta poleta za econt v checkout
			require_once(ECONT_PLUGIN_DIR.'/inc/class-onupdate-econt.php'); //class davasht instrukcii na potrebitelia sled upgrade na plugina
			require_once(ECONT_PLUGIN_ADMIN_DIR.'/class-order-econt.php'); //order details create loading (ot tuk zarejdam i js i css scriptovete)
			require_once(ECONT_PLUGIN_DIR.'/lib/plugin-update-checker/plugin-update-checker.php'); //updates checker

			add_action( 'plugins_loaded', array( &$this,'plugins_loaded' ) );
			//add_action( 'init', array( &$this, 'init') );
			include_once dirname( __FILE__ ) . '/inc/class-mysql-econt.php';
			register_activation_hook( __FILE__, array( 'Econt_mySQL', 'createTables' ) );
			//register_deactivation_hook(__FILE__, array('Econt_mySQL', 'plugin_deactivation'));
			$econtUpdateChecker = Puc_v4_Factory::buildUpdateChecker('http://wpus.mreja.net/updater/?action=get_metadata&slug=woocommerce-econt',__FILE__,'woocommerce-econt');

		}

		public function plugins_loaded() {

			load_plugin_textdomain( 'woocommerce-econt', false, dirname( plugin_basename( __FILE__ ) ). '/languages/' );

			if( !is_admin() ) {
//				require_once(ECONT_PLUGIN_DIR.'/inc/class-debug.php');
			}

		}


/*
		public function init() {

		}
*/
	}
}

new Econt_express();


?>