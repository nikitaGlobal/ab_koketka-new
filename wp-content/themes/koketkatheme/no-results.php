<?php
/**
 *
 * @package koketka
 */
?>

<article id="post-0" class="post no-results not-found">
    <header class="entry-header">
        <h1 class="entry-title"><?php esc_html_e( 'Nothing Found', 'koketkatheme' ); ?></h1>
    </header>

    <div class="entry-content">
        <?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
        <p><?php
            $allowed_html = array(
                'a' => array('href' => array())
            );
            printf(wp_kses(__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'koketkatheme' ), $allowed_html), esc_url( admin_url( 'post-new.php' ) ) );
            ?>
        </p>
        <?php elseif ( is_search() ) : ?>
            <p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'koketkatheme' ); ?></p>
            <?php get_search_form(); ?>
        <?php else : ?>
            <p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'koketkatheme' ); ?></p>
            <?php get_search_form(); ?>
        <?php endif; ?>
    </div>
</article>
