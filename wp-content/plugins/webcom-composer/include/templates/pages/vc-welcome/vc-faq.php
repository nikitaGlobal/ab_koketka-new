<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
?>
<div class="vc_faq-tab changelog">
	<h3><?php _e( 'New to Webcom Composer or Looking for More Information?', 'js_composer' ); ?></h3>

	<p><?php printf( __( 'WPBakery has complete documentation available at our knowledge base: <a target="_blank" href="%s"></a> ', 'js_composer' ), '' ); ?></p>

	<div class="feature-section vc_row">
		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="">Preface</a></h4>
			<ul>
				<li><a target="_blank" href="">Introduction</a></li>
				<li><a target="_blank" href="">Support and Resources</a></li>
				<li><a target="_blank" href="">Support Policy</a></li>
				<li><a target="_blank" href="">Release Notes</a></li>
			</ul>
		</div>

		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="">Licensing</a></h4>
			<ul>
				<li><a target="_blank" href="">Regular License</a></li>
				<li><a target="_blank" href="">Extended License</a></li>
				<li><a target="_blank" href="">In-Stock License (Theme Integration)</a></li>
			</ul>
		</div>

		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="">Getting Started</a></h4>
			<ul>
				<li><a target="_blank" href="">Plugin Installation</a></li>
				<li><a target="_blank" href="">Activation</a></li>
				<li><a target="_blank" href="">Update</a></li>
				<li><a target="_blank" href="">Content Type</a></li>
			</ul>
		</div>
	</div>

	<div class="feature-section vc_row">
		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="">Learning More</a>
			</h4>
			<ul>
				<li><a target="_blank" href="">Basic Concept</a></li>
				<li><a target="_blank" href="">Content Elements</a></li>
				<li><a target="_blank" href="">General Settings</a></li>
				<li><a target="_blank" href="">Custom CSS</a></li>
				<li><a target="_blank" href="">Element Design Options</a></li>
				<li><a target="_blank" href="">Responsive Settings</a></li>
				<li><a target="_blank" href="">Templates</a></li>
				<li><a target="_blank" href="">Predefined Layouts</a></li>
				<li><a target="_blank" href="">Shortcode Mapper</a></li>
				<li><a target="_blank" href="">Grid Builder</a></li>
				<li><a target="_blank" href="">Image filters</a></li>
				<li><a target="_blank" href="">Element Presets</a></li>
			</ul>
		</div>

		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="">Webcom Composer "How To's"</a></h4>

			<p>In this section, you will find quick tips in form of video tutorials on how to operate with Webcom Composer.</p>
		</div>

		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="">FAQ</a></h4>

			<p>Here you can find answers to the Frequently Asked Question about Webcom Composer.</p>
		</div>
	</div>

	<div class="feature-section vc_row">
		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="">Add-ons</a></h4>
			<ul>
				<li><a target="_blank" href="">Templatera</a></li>
				<li><a target="_blank" href="">Easy Tables</a></li>
				<li><a target="_blank" href="">Add-on Development Rules</a></li>
			</ul>
		</div>

		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="">Theme Integration</a></h4>

			<p>See how you can integrate Webcom Composer within your WordPress theme.</p>
		</div>

		<div class="vc_col-xs-4">
			<h4><a target="_blank" href="">Inner API</a></h4>

			<p>Inner API section describes capabilities of interaction with Webcom Composer.</p>
		</div>
	</div>
</div>

<div class="return-to-dashboard">
	<a target="_blank" href=""><?php _e( 'Visit Knowledge Base for more information', 'js_composer' ); ?></a>
</div>
