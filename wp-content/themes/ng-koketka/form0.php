<form id="callback_form" class="form_custom" name="Форма 1" style="display: none;">
    <div class="form_item">
        <h4>Свържете се с нас</h4>
        
        <label for="your-name">Вашето име</label>
        <input name="Име" type="text" id="your-name" required>
        
        <label for="message">Сообщение</label>
        <textarea name="message" id="message" cols="30" rows="8" required></textarea>
        
        <button>Изпрати</button>
    </div>
    
    
    <div class="popup_close">
        <svg width="22" height="21" viewBox="0 0 22 21" fill="none"
             xmlns="http://www.w3.org/2000/svg">
            <line x1="18" y1="3.53553" x2="4.53553" y2="17" stroke="white"
                  stroke-width="5" stroke-linecap="round"/>
            <line x1="17.4645" y1="17" x2="4" y2="3.53554" stroke="white"
                  stroke-width="5" stroke-linecap="round"/>
        </svg>
    </div>
</form>