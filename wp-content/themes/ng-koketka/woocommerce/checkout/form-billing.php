<?php
    /**
     * Checkout billing information form
     *
     * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
     *
     * HOWEVER, on occasion WooCommerce will need to update template files and you
     * (the theme developer) will need to copy the new files to your theme to
     * maintain compatibility. We try to do this as little as possible, but it does
     * happen. When this occurs the version of the template file will be bumped and
     * the readme will list any important changes.
     *
     * @see     https://docs.woocommerce.com/document/template-structure/
     * @package WooCommerce/Templates
     * @version 3.6.0
     * @global WC_Checkout $checkout
     */
    
    defined('ABSPATH') || exit;
?>
<?php foreach (array('left', 'center') as $col) {
    $fields = $checkout->get_checkout_fields('billing');
    ?>
    <?php if ($col == 'left') { ?>

        <div class="left">
            <div class="woocommerce-billing-fields">
                <?php if (wc_ship_to_billing_address_only() && WC()->cart->needs_shipping()) { ?>

                    <h3><?php esc_html_e('Billing &amp; Shipping',
                            'woocommerce'); ?></h3>
                
                <?php } else { ?>  <h3><?php esc_html_e('Billing details',
                    'woocommerce'); ?></h3><?php } ?>
                <div class="woocommerce-billing-fields__field-wrapper">
                    <?php
                        foreach ($fields as $key => $field) {
                            if ($field['col'] == $col) {
                                woocommerce_form_field($key, $field,
                                    $checkout->get_value($key));
                            }
                        }
                    ?></div>
            </div>
            <?php if ( ! is_user_logged_in() && $checkout->is_registration_enabled()) : ?>
                <div class="woocommerce-account-fields">
                    <?php if ( ! $checkout->is_registration_required()) : ?>

                        <p class="form-row form-row-wide create-account">
                            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                                <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox"
                                       id="createaccount" <?php checked((true === $checkout->get_value('createaccount') || (true === apply_filters('woocommerce_create_account_default_checked',
                                            false))), true); ?> type="checkbox"
                                       name="createaccount" value="1"/>
                                <span><?php esc_html_e('Create an account?',
                                        'woocommerce'); ?></span>
                            </label>
                        </p>
            
                    <?php endif; ?>
            
                    <?php do_action('woocommerce_before_checkout_registration_form',
                        $checkout); ?>
            
                    <?php if ($checkout->get_checkout_fields('account')) : ?>

                        <div class="create-account">
                            <?php foreach ($checkout->get_checkout_fields('account') as $key => $field) : ?>
                                <?php woocommerce_form_field($key, $field,
                                    $checkout->get_value($key)); ?>
                            <?php endforeach; ?>
                            <div class="clear"></div>
                        </div>
            
                    <?php endif; ?>
            
                    <?php do_action('woocommerce_after_checkout_registration_form',
                        $checkout); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php } else { ?>
        <div class="center">
            <h3>&nbsp;</h3>
            <?php //do_action('woocommerce_before_checkout_form', $checkout);?>
            <?php
                foreach ($fields as $key => $field) {
                    if ($field['col'] == $col) {
                        woocommerce_form_field($key, $field,
                            $checkout->get_value($key));
                    }
                }
                if (2 == 1) {
                    ?>
                    <p class="country_wrap h-gutter">
                        <label class="required"
                               for="billing_country">Страна</label>
                        <select class="select_custom bootstrap-select"
                                id="billing_country">
                            <option>Россия</option>
                            <option>Беларусь</option>
                            <option>Болгария</option>
                            <option>Украина</option>
                        </select>
                    </p>

                    <p class="validate-required h-gutter">
                        <label for="billing_city" class="required">
                            Город
                        </label>
                        <span class="woocommerce-input-wrapper">
													<input type="text"
                                                           class="input-text"
                                                           id="billing_city">
												</span>
                    </p>

                    <p class="validate-required h-gutter">
                    <div class="street_home">
                        <div class="street h-gutter">
                            <label for="billing_address_1"
                                   class="required">Улица</label>
                            <span class="woocommerce-input-wrapper">
															<input type="text"
                                                                   class="input-text"
                                                                   id="billing_address_1">
														</span>
                        </div>

                        <div class="home">
                            <label for="billing_address_2"
                                   class="required">Дом</label>
                            <span class="woocommerce-input-wrapper">
															<input type="text"
                                                                   class="input-text"
                                                                   id="billing_address_2">
														</span>
                        </div>
                    </div>

                    <p id="ship-to-new-address" class="ship-to-new-address">
                        <a href="#0" data-src="#address_edit_popup"
                           data-fancybox class="add_new_address">
														<span class="circle_plus">
															<img src="assets/images/plus.svg"
                                                                 alt="">
														</span>
                            <span class="add_link">
															Добавить новый <br> адрес доставки
														</span>
                        </a>
                    </p>

                    <!-- Popup Adress BEGIN -->
                    <div class="address_edit_popup" style="display: none;"
                         id="address_edit_popup">
                        <div class="popup_row">
                            <div class="left">
                                <div class="country_wrap h-gutter">
                                    <label class="required"
                                           for="select-country">Страна</label>
                                    <select class="select_custom bootstrap-select"
                                            id="select-country">
                                        <option>Россия</option>
                                        <option>Беларусь</option>
                                        <option>Болгария</option>
                                        <option>Украина</option>
                                    </select>
                                </div>

                                <div class="street_home">
                                    <div class="input_custom h-gutter">
                                        <label for="street"
                                               class="required">Улица</label>
                                        <input type="text" id="street">
                                    </div>
                                    <div class="input_custom h-gutter">
                                        <label for="home"
                                               class="required">Дом</label>
                                        <input type="text" id="home">
                                    </div>
                                </div>
                            </div>


                            <div class="right">
                                <div class="city_phone">
                                    <div class="input_custom h-gutter">
                                        <label for="city"
                                               class="required">Город</label>
                                        <input type="text" id="city">
                                    </div>
                                    <div class="input_custom h-gutter">
                                        <label for="phone"
                                               class="required">Телефон</label>
                                        <input type="text" id="phone"
                                               class="phone__custom">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="#0" class="button_custom">Добавить адрес</a>
                    </div>
                    <!-- Popup Address END -->
                <?php } ?>
        </div>
    <?php } ?>
<?php } ?>


<?php return; ?>
<div class="woocommerce-billing-fields">
    <?php if (wc_ship_to_billing_address_only() && WC()->cart->needs_shipping()) : ?>

        <h3><?php esc_html_e('Billing &amp; Shipping', 'woocommerce'); ?></h3>
    
    <?php else : ?>

        <h3><?php esc_html_e('Billing details', 'woocommerce'); ?></h3>
    
    <?php endif; ?>
    
    <?php do_action('woocommerce_before_checkout_billing_form', $checkout); ?>

    <div class="woocommerce-billing-fields__field-wrapper">
        <?php
            $fields = $checkout->get_checkout_fields('billing');
            
            foreach ($fields as $key => $field) {
                woocommerce_form_field($key, $field, $checkout->get_value($key));
            }
        ?>
    </div>
    
    <?php do_action('woocommerce_after_checkout_billing_form', $checkout); ?>
</div>


