<table class="econt-table">

<?php if(get_post_meta($order->get_id(), 'Econt_Shipping_To', true) == 'OFFICE') { ?>
<!--//Econt Express Office-->
<caption><h3><?php _e('Econt Express shipping to office', 'woocommerce-econt') ?><a href="#" class="edit_econt_address">Edit</a></h3></caption><thead>
    <tr>
    <th scope="col"><?php _e('Name', 'woocommerce-econt') ?></th>
    <th scope="col"><?php _e('Value', 'woocommerce-econt') ?></th>
    </tr>
    </thead>
    <tbody>
    <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Town', 'woocommerce-econt') ?> :</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?> "> <?php echo get_post_meta($order->get_id(), 'Econt_Office_Town', true) ?></td></tr>
<tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"> <?php _e('Office', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?>"><?php echo $office['name'] . __(' address: ', 'woocommerce-econt') . $office['address'] ?></td></tr>
 <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Postcode', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?>"><?php echo get_post_meta($order->get_id(), 'Econt_Office_Postcode', true) ?></td></tr>

<?php }elseif(get_post_meta($order->get_id(), 'Econt_Shipping_To', true) == 'MACHINE'){ ?>
 <!--//Econt Express APS-->
 <caption><h3><?php _e('Econt Express shipping to APS', 'woocommerce-econt') ?><a href="#" class="edit_econt_address">Edit</a></h3></caption><thead>
    <tr>
    <th scope="col"><?php _e('Name', 'woocommerce-econt') ?></th>
    <th scope="col"><?php _e('Value', 'woocommerce-econt') ?></th>
    </tr>
    </thead>
    <tbody>
	<tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Town', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt')  ?>"><?php echo get_post_meta($order->get_id(), 'Econt_Machine_Town', true) ?></td></tr>
    <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('APS', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?>"><?php echo $machine['name'] . __(' address: ', 'woocommerce-econt') . $machine['address'] ?></td></tr>
    <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Postcode', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?>"><?php echo get_post_meta($order->get_id(), 'Econt_Machine_Postcode', true) ?></td></tr>
  
    <?php }elseif(get_post_meta($order->get_id(), 'Econt_Shipping_To', true) == 'DOOR') { ?>    
    <!--//Econt Express Door-->
    <caption><h3><?php _e('Econt Express shipping to door', 'woocommerce-econt') ?><a href="#" class="edit_econt_address">Edit</a></h3></caption><thead>
    <tr>
    <th scope="col"><?php _e('Name', 'woocommerce-econt') ?></th>
    <th scope="col"><?php _e('Value', 'woocommerce-econt') ?></th>
    </tr>
    </thead>
    <tbody>
    <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Town', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?>"><?php echo get_post_meta($order->get_id(), 'Econt_Door_Town', true) ?> </td></tr>
    <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Postcode', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?>"><?php echo get_post_meta($order->get_id(), 'Econt_Door_Postcode', true) ?> </td></tr>
    <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Street', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?>"><?php echo get_post_meta($order->get_id(), 'Econt_Door_Street', true) ?> </td></tr>
    <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Quarter', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?>"><?php echo get_post_meta($order->get_id(), 'Econt_Door_Quarter', true) ?></td></tr>
    <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Street num', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?>"><?php echo get_post_meta($order->get_id(), 'Econt_Door_street_num', true) ?></td></tr>
    <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Building num', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?>"><?php echo get_post_meta($order->get_id(), 'Econt_Door_building_num', true) ?> </td></tr>
    <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Entrance num', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?>"><?php echo get_post_meta($order->get_id(), 'Econt_Door_Entrance_num', true) ?></td></tr>
    <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Apartment num', 'woocommerce-econt') ?>':</td><td data-label="<?php _e('Value', 'woocommerce-econt') ?>"><?php echo get_post_meta($order->get_id(), 'Econt_Door_Apartment_num', true) ?></td></tr>
    <tr><td data-label="<?php _e('Name', 'woocommerce-econt') ?>"><?php _e('Notes', 'woocommerce-econt') ?>:</td><td data-label="<?php _e('Value', 'woocommerce-econt')  ?>"><?php echo get_post_meta($order->get_id(), 'Econt_Door_Other', true) ?></td></tr>

    <?php  } ?>
    </tbody></table>





<div id="econt_edit_receiver_address" style="display: none;">
<script type="text/javascript"> 
	var sender_city_id = "";
	var order_id = "<?php echo $order->get_id(); ?>"; 
	var loading_is_imported = "<?php echo $loading_is_imported; ?>";
</script>
<p class="form-row econt_shipping_to form-row-wide validate-required woocommerce-validated" id="econt_shipping_to_field" data-priority=""><label for="econt_shipping_to" class=""><strong><?php _e('Shipping to', 'woocommerce-econt') ?></strong><abbr class="required" title="задължително">*</abbr></label><span class="woocommerce-input-wrapper"><select name="econt_shipping_to" id="econt_shipping_to" class="select " data-placeholder="<?php _e('Shipping to', 'woocommerce-econt') ?>">
<option value="0"><?php _e('please select...', 'woocommerce-econt') ?></option>
<?php if($wc_econt->send_to_door == 1){ ?>
<option value="DOOR"><?php _e('to door', 'woocommerce-econt') ?></option>
<?php }
if($wc_econt->send_to_office == 1){ ?>
<option value="OFFICE"><?php _e('to office', 'woocommerce-econt') ?></option>
<?php }
if($wc_econt->send_to_machine == 1){ ?>
<option value="MACHINE"><?php _e('to APS', 'woocommerce-econt') ?></option>
<?php } ?>
</select>
</span></p>
<p class="form-row econt_shipping_to_office form-row-wide validate-required" id="econt_offices_town_field" data-priority=""><label for="econt_offices_town" class=""><?php _e('Town (Please, start typing and select from results.)', 'woocommerce-econt') ?><abbr class="required" title="задължително">*</abbr></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text  ui-autocomplete-input" name="econt_offices_town" id="econt_offices_town" placeholder="<?php _e('Enter your town', 'woocommerce-econt') ?>" value="" autocomplete="new-username">
</span></p>
<p class="form-row econt_shipping_to_office form-row-wide validate-required" id="econt_offices_postcode_field" data-priority=""><label for="econt_offices_postcode" class=""><?php _e('Post Code', 'woocommerce-econt') ?><abbr class="required" title="задължително">*</abbr></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="econt_offices_postcode" id="econt_offices_postcode" placeholder="<?php _e('Post Code', 'woocommerce-econt') ?>" value="">
</span></p>
<p>
<a href="javascript:void(0);" class="econt_office_locator button" id="office_locator" style="display:none;" title="офис локатор"><?php _e('office locator','woocommerce-econt') ?></a>
</p>

<?php
//office locator in colorbox jquery
$office_locator = 'https://www.bgmaps.com/templates/econt?office_type=to_office_courier&shop_url=' . get_site_url(); //HTTPS_SERVER;
$office_locator_domain = 'https://www.bgmaps.com';
?>

<script type="text/javascript">
	function receiveMessage(event) {
		if (event.origin !== '<?php echo $office_locator_domain; ?>')
			return;

		message_array = event.data.split('||');
		jQuery("#econt_offices").val(message_array[0]).trigger('change');
		jQuery.colorbox.close();
	}

	if (window.addEventListener) {
		window.addEventListener('message', receiveMessage, false);
	} else if (window.attachEvent) {
		window.attachEvent('onmessage', receiveMessage);
	}

	jQuery(document).ready(function() {
		
		if (jQuery('#econt_offices_town').val()) {
			url = '<?php echo $office_locator; ?>&address=' + jQuery('#econt_offices_town').val();
		} else {
			url = '<?php echo $office_locator; ?>';
		}
		
		jQuery('#econt_offices_town').change(function () {

			if (jQuery('#econt_offices_town').val()) {
				url = '<?php echo $office_locator; ?>&address=' + jQuery('#econt_offices_town').val();
			} else {
				url = '<?php echo $office_locator; ?>';
			}

			jQuery('a#office_locator').colorbox({
				overlayClose: true,
				href : url,
				iframe : true,
				opacity: 0.5,
				width  : '90%',
				height : '90%'
			});

			jQuery(window).resize(function(){
	        	jQuery.colorbox.resize({
	            	width: '90%',
	           	 	height: '90%'
	             });
	        });
		});


	});

</script>




<p class="form-row econt_shipping_to_office form-row-wide validate-required" id="econt_offices_field" data-priority=""><label for="econt_offices" class=""><?php  _e('Office', 'woocommerce-econt') ?><abbr class="required" title="задължително">*</abbr></label><span class="woocommerce-input-wrapper"><select name="econt_offices" id="econt_offices" class="select " data-placeholder="<?php _e('Select office', 'woocommerce-econt') ?>">
<option value="0"><?php _e('please select...', 'woocommerce-econt') ?></option>
</select></span></p>
<p class="form-row econt_shipping_to_machine form-row-wide validate-required" id="econt_machines_town_field" data-priority=""><label for="econt_machines_town" class=""><?php _e('24 часа Еконт - Автоматична пощенска станция” (АПС) е устройство, с което сами изпращате и получавате пратки денонощно, без почивен ден.  Научете повече на: <a href="http://www.econt.com/24-chasa-econt-aps/" target="_blank">http://www.econt.com/24-chasa-econt-aps/</a><br>Град (Моля, започнете да пишете и изберете от резултатите.)', 'woocommerce-econt') ?><abbr class="required" title="задължително">*</abbr></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text  ui-autocomplete-input" name="econt_machines_town" id="econt_machines_town" placeholder="<?php _e('Enter your town', 'woocommerce-econt') ?>" value="" autocomplete="off">
</span></p>
<p class="form-row econt_shipping_to_machine form-row-wide validate-required" id="econt_machines_postcode_field" data-priority=""><label for="econt_machines_postcode" class=""><?php _e('Post Code', 'woocommerce-econt') ?><abbr class="required" title="задължително">*</abbr></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="econt_machines_postcode" id="econt_machines_postcode" placeholder="<?php _e('Post Code', 'woocommerce-econt') ?>" value=""></span></p>
<p class="form-row econt_shipping_to_machine form-row-wide validate-required" id="econt_machines_field" data-priority=""><label for="econt_machines" class=""><?php _e('Office', 'woocommerce-econt') ?><abbr class="required" title="задължително">*</abbr></label><span class="woocommerce-input-wrapper">
<select name="econt_machines" id="econt_machines" class="select " data-placeholder="<?php _e('Select office', 'woocommerce-econt') ?>">
<option value="0"><?php _e('please select...', 'woocommerce-econt') ?></option>
</select>
</span></p>
<p class="form-row econt_shipping_to_door form-row-wide validate-required woocommerce-validated" id="econt_door_town_field" data-priority=""><label for="econt_door_town" class=""><?php _e('Town (Please, start typing and select from results.)', 'woocommerce-econt') ?><abbr class="required" title="задължително">*</abbr></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text  ui-autocomplete-input" name="econt_door_town" id="econt_door_town" placeholder="<?php _e('Enter your town', 'woocommerce-econt') ?>" autocomplete="new-username">
</span></p>
<p class="form-row econt_shipping_to_door form-row-wide validate-required" id="econt_door_postcode_field" data-priority=""><label for="econt_door_postcode" class=""><?php _e('Post Code', 'woocommerce-econt') ?><abbr class="required" title="задължително">*</abbr></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="econt_door_postcode" id="econt_door_postcode" placeholder="<?php _e('Post Code', 'woocommerce-econt') ?>">
</span></p>
<p class="form-row econt_shipping_to_door form-row-wide" id="econt_door_street_field" data-priority=""><label for="econt_door_street" class=""><?php _e('Street (Please, start typing and select from results.)', 'woocommerce-econt') ?></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text  ui-autocomplete-input" name="econt_door_street" id="econt_door_street" placeholder="<?php _e('Enter your street', 'woocommerce-econt') ?>" autocomplete="off">
</span></p>
<p class="form-row econt_shipping_to_door form-row-wide" id="econt_door_quarter_field" data-priority=""><label for="econt_door_quarter" class=""><?php _e('Quarter (Please, start typing and select from results.)', 'woocommerce-econt') ?></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text  ui-autocomplete-input" name="econt_door_quarter" id="econt_door_quarter" placeholder="<?php _e('Enter your quarter', 'woocommerce-econt') ?>" autocomplete="off">
</span></p>
<p class="form-row econt_shipping_to_door form-row-wide" id="econt_door_street_num_field" data-priority=""><label for="econt_door_street_num" class=""><?php _e('Street num.:', 'woocommerce-econt') ?></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="econt_door_street_num" id="econt_door_street_num" placeholder="<?php _e('street num', 'woocommerce-econt') ?>">
</span></p>
<p class="form-row econt_shipping_to_door form-row-wide" id="econt_door_street_bl_field" data-priority=""><label for="econt_door_street_bl" class=""><?php _e('Street block num.:', 'woocommerce-econt') ?></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="econt_door_street_bl" id="econt_door_street_bl" placeholder="<?php _e('blok num', 'woocommerce-econt') ?>">
</span></p>
<p class="form-row econt_shipping_to_door form-row-wide" id="econt_door_street_vh_field" data-priority=""><label for="econt_door_street_vh" class=""><?php _e('Street entrance num.:', 'woocommerce-econt') ?></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="econt_door_street_vh" id="econt_door_street_vh" placeholder="<?php _e('entr. num', 'woocommerce-econt') ?>">
</span></p>
<p class="form-row econt_shipping_to_door form-row-wide" id="econt_door_street_et_field" data-priority=""><label for="econt_door_street_et" class=""><?php _e('Street floor num.:', 'woocommerce-econt') ?></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="econt_door_street_et" id="econt_door_street_et" placeholder="<?php _e('fl. num', 'woocommerce-econt') ?>">
</span></p>
<p class="form-row econt_shipping_to_door form-row-wide" id="econt_door_street_ap_field" data-priority=""><label for="econt_door_street_ap" class=""><?php _e('Street apartment num.:', 'woocommerce-econt') ?></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="econt_door_street_ap" id="econt_door_street_ap" placeholder="<?php _e('ap. num', 'woocommerce-econt') ?>">
</span></p>
<p class="form-row econt_shipping_to_door form-row-wide" id="econt_door_other_field" data-priority=""><label for="econt_door_other" class=""><?php _e('If your address is not in the list please type it here:', 'woocommerce-econt') ?></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="econt_door_other" id="econt_door_other" placeholder="<?php _e('Enter other adress info', 'woocommerce-econt') ?>">
</span></p>
<p>
<a href="javascript:void(0);" class="econt_save_receiver_address button-primary button" id="save_receiver_address" title="<?php _e('save address','woocommerce-econt') ?>"><?php _e('update address','woocommerce-econt') ?></a>
</p>
</div>