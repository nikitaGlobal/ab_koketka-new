<?php abstract Class cwfilter
{
    private static function init()
    {
        return get_option('cwooattr');
    }
    
    private static function _prepareGet($params)
    {
        global $wpdb;
        $q     = '';
        $query = 'SELECT * FROM `' . $wpdb->prefix . 'term_relationships` ';
        $q     .= $query;
        $test  = array();
        $index = 0;
        global $wp_query;
        $cat = $wp_query->get_queried_object();
        if (isset($cat->term_id)) {
            $params[$cat->taxonomy] = array($cat->term_id);
        }
        foreach ($params as $k => $v) {
            if (is_array($v)) {
                if (false !== strpos($k, 'pa_') || $k == 'product_cat') {
                    if ($index > 0) {
                        $q .= ' OR ';
                    } else {
                        $q .= ' WHERE ';
                    }
                    $q      .= ' term_taxonomy_id IN (' . implode(',', $v) . ') ';
                    $test[] = $v;
                    $index++;
                }
            }
        }
//echo $query;
        $res  = $wpdb->get_results($q);
        $rows = array();
        foreach ($res as $row) {
            $rows[$row->object_id][] = $row->term_taxonomy_id;
        }
        foreach ($rows as $k => $v) {
            if (count($v) !== count($test)) {
                unset($rows[$k]);
            }
        }
        
        return count($rows);
        $query['tax_query']['relation'] = 'AND';
        foreach ($params as $k => $v) {
            if (is_array($v)) {
                if (false !== strpos($k, 'pa_')) {
                    $query['tax_query'][] = array(
                        'taxonomy' => $k,
                        'field'    => 'term_id',
                        'terms'    => $v,
                        'operator' => 'IN'
                    );
                }
                continue;
            }
            if ($k == 'sale') {
                $query['meta_query'][] = array(
                    'key'     => '_sale_price',
                    'value'   => 0,
                    'compare' => '>',
                    'type'    => 'NUMERIC'
                );
            }
        }
        
        return $query;
    }
    
    private static function count($try)
    {
        global $wpdb;
        $query = self::_prepareGet($try);
        
        return $query;
        $query['post_type'] = 'product';
        $posts              = get_posts($query);

//echo (count($posts));
        return (count($posts));
    }
    
    public static function filter($out)
    {
        $try = array();
        foreach ($out as $k => $v) {
// print_r($try_);
            foreach ($v as $key => $value) {
                $try_ = $_GET;
                if (isset($try_[$k]) && in_array($value->term_id, $try_[$k])) {
                    continue;
                }
                $try_[$k][]    = $value->term_id;
                $try_['count'] = self::count($try_);
                if ($try_['count'] == 0) {
                    unset($out[$k][$key]);
                }
                $try[] = $try_;
            }
        }
        foreach ($out as $k => $v) {
            if (empty($v)) {
                unset($out[$k]);
            }
        }
        
        return $out;
    }
    
    public static function removeSingles($items)
    {
        
        foreach ($items as $k => $v) {
            if (count($v) <= 1) {
                unset($items[$k]);
            }
        }
        
        return $items;
    }
    
    public static function getOrder()
    {
        global $wpdb;
        $q     = 'SELECT * FROM `' . $wpdb->prefix . 'woocommerce_termmeta` WHERE meta_key LIKE "order_pa%" ORDER BY meta_value ASC';
        $rows  = $wpdb->get_results($q);
        $order = array();
        foreach ($rows as $row) {
            $order[$row->woocommerce_term_id] = $row;
        }
        
        return $order;
    }
    
    public static function sort($items)
    {
        $order   = self::getOrder();
        $ordered = array();
        $items   = self::assoc($items);
//        print_r($order);
        foreach ($items as $label => $values) {
            foreach ($values as $value) {
                if ( ! isset($order[$value->term_id])) {
                    $order[$value->term_id] = array(
                        'meta_id'             => $value->term_id,
                        'woocommerce_term_id' => $value->term_id,
                        'meta_key'            => 'order_' . $value->taxonomy,
                        'meta_value'          => 999
                    );
                }
            }
        }
//print_r($order);
        foreach ($items as $key => $item_) {
            foreach ($order as $term => $number) {
                if ( ! isset($items[$key][$term])) {
                    continue;
                }
                $ordered[$key][] = $items[$key][$term];
            }
        }
        
        return $ordered;
    }
    
    public static function assoc($items)
    {
        $assoc = array();
        foreach ($items as $key => $values) {
            foreach ($values as $k => $v) {
                $assoc[$key][$v->term_id] = $v;
            }
        }
        
        return $assoc;
    }
    
    public static function customsort($values, $filter)
    {
        $labeled = array();
        $ordered = array();
        foreach ($values as $k => $value) {
            $labeled[wc_attribute_label($k)] = $k;
        }
        foreach ($filter as $v) {
            if (isset($labeled[$v]) && isset($values[$labeled[$v]])) {
                $ordered[$labeled[$v]] = $values[$labeled[$v]];
            }
        }
        
        return $ordered;
    }
    
    public static function show($filter = false)
    {
        $taxonomies = get_object_taxonomies('product');
        $terms      = (get_terms($taxonomies));
        $out        = array();
        foreach ($terms as $term) {
            if (wc_attribute_label($term->taxonomy) == $term->taxonomy) {
                continue;
            }
            $term->label            = wc_attribute_label($term->taxonomy);
            $out[$term->taxonomy][] = $term;
        }
        if ( ! $filter) {
            $out = self::sort(self::filter(self::removeSingles($out)));
            
            return $out;
        }
        
        return self::removeSingles(
            self::customsort(
                self::sort(
                    self::filter(
                        $out
                    )
                ), $filter
            )
        );
    }
}
