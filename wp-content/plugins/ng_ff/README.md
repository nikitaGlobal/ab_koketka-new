=== Usage ===

Create form file in your theme folder. For example form.php

All name attributes for input, select, textarea and form tags and also for attribute for label tag can be 'human readable'.
For expamle:

<input type="text" name="Customer name">

NB! Do not forget name attribute for tag form.
Use shortcode to insert form from file:

[cw_ff]form[/cw_ff]

In theme files you can also use NG::form('form');
You can also state file path instead of template-part

Important! Make sure that none of javascripts iterferes form submission

CSS classes applied to form:

.cwprocessing the form is being processed
.cwsubmitted form is being submitted

If form cannot be validated it gets class .error

fields attributes:

data-cwvalidate - regex value validation

data-cwmessage - validation error message

data-cwfilter - regex type filter

You can also use abstract methods in form file to implement validation rules:

<input type="email" name="E-mail address" <?php <?php NG::validate('email');?>>

Currently we have email and phone

Forms are saved in dashboard, but not sent to keep your frontent fast.

Forms are sent after http(s) request to http://yourdomain.com/cw_ff

Use cron to poll this url.

=== Filters ===

If you want to add more recipents or set custom ones, use filter

cwffrecipients
```
    add_filter('cwffrecipients', 'recipients');
    function recipients($to) {
        $to=array('mail1@mail.com', 'mail2@mail.com');
        return $to;
    }
    cw_ffbeforesave
```
Check/change form after validation and before saving


You can create your custom script for after submission event with function cwsuccess($form) or track ngsubmitted (and 'ngsubmitted'+$formid) js action

filter before forms are sent

cw_ffpre_send

Action before forms are sent

cw_ffpre_send

filter to modify validation rules

cw_ffrules

Form submitted messages:

<form data-cwform1="{"success":"<h1>Thank you</h1>"}"
<h1>Thank you</h1> will be placed before <form></form> after submission

=== Manual script loading ===

Use NG::scriptTag() to enqueue required scripts manually in your theme file


=== Использование ===

В папке темы создайте файл с формой. Например, form.php.

Все атрибуты name тегов полей форм (input, select, textarea) и самой, а также атрибутов for для тегов label формы заполняются "человекчитаемыми" значениями, например:

<input type="text" name="Имя пользователя">.

Важно! Не забывайте про атрибут name тега form
Для вставки формы из файла form.php в скрипты или записи используйте "шорт-код":

[cw_ff]form[/cw_ff]

В теме можете использовать NG::form('form');
Вы также можете использовать путь к файлу вместо template-part

Важно! Убедитесь в том что никакой яваскрипт не вмешивается в отправку формы!

CSS классы формы
.cwprocessing форма обрабатывается
.cwsubmitted форма отправлена

Если поле не проходит валидацию, ей присваивается класс .error

Атрибуты полей:

data-cwvalidate - regex правила проверки значения

data-cwmessage - сообщение об ошибке

data-cwfilter - regex-правила фильтра при наборе

Также можете использовать абстрактные методы для присвоения правил, например

<input type="email" name="Адрес эл.почты" <?php <?php NG::validate('email');?>>

В настоящий момент есть email и phone

Формы сохраняются в админке в разделе Submissions, но не отправляются чтобы не
нагружать фронтенд

Для отправки пропишите в крон-службу обращение по http (или https) на адрес

http://вашдомен.ru/cw_ff/

Для добавления адресатов, используйте фильтр:
```
    add_filter('cwffrecipients', 'recipients');
    function recipients($to) {
        $to=array('mail1@mail.com', 'mail2@mail.com');
        return $to;
    }
``
После успешной отправки формы вызывается также js-функция cwffsuccess($form)
при её наличии. Также можно отследить js action 'ngsubmitted' и 'ngsubmitted'+$formid

Фильтры форм для отправки:

cw_ffpre_send

Фильтр перед сохранением формы

cw_ffbeforesave

Действия перед отправкой:

cw_ffpre_send

Фильтр для изменения правил валидации

cwff_rules

Вывод сообщений об отправке

<form data-cwform1="{"success":"<h1>Спасибо</h1>"}"

<h1>Спасибо</h1> будет размещено перед <form></form> после отправки

=== Ручное подключение скрипта ===

Используйте метод NG::scriptTag() для принудительной генерации скрипта
в файле темы