<?php
    
    Class Ngtheme
    {
        function __construct($timber)
        {
            $this->timber  = $timber;
            $this->prefix  = self::prefix();
            $this->version = self::prefix();
            remove_action('woocommerce_single_product_summary',
                'woocommerce_template_single_excerpt', 20);
            remove_action('woocommerce_single_product_summary',
                'woocommerce_template_single_meta', 40);
            remove_action('woocommerce_single_product_summary',
                'woocommerce_template_single_sharing', 50);
            remove_action('woocommerce_single_variation',
                'woocommerce_single_variation', 10);
            remove_action('woocommerce_single_variation',
                'woocommerce_single_variation_add_to_cart_button', 20);
            remove_action('woocommerce_variable_add_to_cart',
                'woocommerce_variable_add_to_cart', 30);
            add_action('woocommerce_variable_add_to_cart',
                'woocommerce_variable_add_to_cart', 10);
            add_action('woocommerce_single_variation',
                'woocommerce_single_variation_add_to_cart_button', 20);
            add_action('woocommerce_single_variation',
                'woocommerce_single_variation', 30);
            add_filter('wp_get_attachment_image_src', array($this, 'removeImgAttr'),
                100, 4);
            add_action('after_setup_theme', function () {
                add_theme_support('woocommerce');
            });
            remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
            add_action( 'woocommerce_before_main_content', 'woocommerce_result_count', 20, 0 );
            $this->_fieldsFilters();
            add_theme_support("editor-style");
            add_theme_support("post-thumbnails");
            $this->_registerMenu();
            define('NGBLOCKSAUTO', true);
            $this->_runInit();
            add_filter('woocommerce_enqueue_styles', '__return_empty_array');
            add_action("wp_enqueue_scripts", array($this, "wpEnqueueScripts"), 2);
            $this->_initTimber();
            add_action('pre_get_posts', array($this, 'onlyProductsInSearch'));
        }
        
        public function onlyProductsInSearch($query)
        {
            if ( ! is_admin() && is_search() && $query->is_main_query()) {
                $query->set('post_type', 'product');
            }
        }
        
        public static function version()
        {
            if (defined('WP_DEBUG') && WP_DEBUG == true) {
                return time();
            }
            
            return "1.1";
        }
        
        
        private function _fieldsFilters()
        {
            $filters = array(
                'wooFilterFields',
                'wooSortFields',
                'wooSortCols'
            );
            add_filter('woocommerce_form_field',
                array($this, 'woocommerceFormFieldFilter'), 999, 5);
            foreach (
                array(
                    'woocommerce_checkout_fields',
                    'woocommerce_default_address_fields'
                ) as $filter
            ) {
                foreach ($filters as $method) {
                    add_filter($filter,
                        array($this, $method));
                }
            }
            
            return;
            
            add_filter('woocommerce_checkout_fields',
                array($this, 'wooSortFields'));
            add_filter('woocommerce_checkout_fields',
                array($this, 'wooSortCols'));
        }
        
        public static function wooFilter($key, $param)
        {
            if ( ! isset($param['fieldtype']) || (int)strlen($param['fieldtype']) == 0) {
                return;
            }
            if ( ! method_exists(__CLASS__, 'wooFilter' . $param['fieldtype'])) {
                return;
            }
            
            return self::{'wooFilter' . $param['fieldtype']}($key, $param);
        }
        
        public static function wooFilterrange($key, $param)
        {
            $values = implode(',', $param['values']);
            ?>
        <div class="<?php echo $key; ?>_range">
            <input type="hidden" name="<?php echo $param['field']; ?>"
                   class="slider-input"
                   value="<?php
                       echo $values;
                   ?>">
            </div><?php
        }
        
        public static function wooFilterselect($key, $param)
        {
            ?><select name="<?php echo $param['field']; ?>"
                      id="<?php echo $key; ?>" class=""
                      multiple="multiple">
            <?php foreach ($param['values'] as $value) {
            if ($value['count'] == 0) {
                continue;
            }
            ?>
            <option
                <?php if ($value['selected']) {
                    ?> selected <?php
                } ?>
                    value='<?php echo $value['id']; ?>'><?php
                    echo $value['name'];
                ?></option>
        <?php } ?>
            </select><?php
        }
        
        public static function prefix()
        {
            
            return "ngtheme";
        }
        
        private function _registerMenu()
        {
            foreach (
                array(
                    array('topMenu', __('Top menu', $this->prefix)),
                    array('socialMenu', __('Social menu', $this->prefix)),
                    array('footerMenu', __('Footer menu', $this->prefix)),
                    array('sideMenu', __('Side menu', $this->prefix)),
                ) as $menu
            ) {
                register_nav_menu($menu[0], $menu[1]);
            }
        }
        
        public function sccatpicture($att, $content)
        {
            $cat    = $att['cat'];
            $imgsrc = wp_get_attachment_image_src(get_term_meta($cat, 'thumbnail_id',
                true), 'original');
            if ( ! isset($imgsrc[0])) {
                return '';
            }
            
            return $imgsrc[0];
            
            
        }
        
        private function _runInit()
        {
            $methods = get_class_methods($this);
            if (empty($methods)) {
                return;
            }
            foreach ($methods as $method) {
                foreach (array('init', 'wp') as $action) {
                    if (strpos($method, $action) === 0) {
                        add_action($action, array($this, $method), 999);
                    }
                }
                if (strpos($method, 'filter') === 0) {
                    $filter = str_replace('filter', '', $method);
                    add_filter($filter, array($this, $method), 9999);
                }
                if (strpos($method, 'sc') === 0) {
                    $shortcode = str_replace('sc', self::prefix(), $method);
                    add_shortcode($shortcode, array($this, $method));
                }
            }
        }
        
        public function woocommerceFormFieldFilter($field, $key, $args, $value)
        {
            if ($args['id'] == 'user_consents_privacy-policy') {
                return $this->_woocommerceFormFieldFilterPrivacy($field, $key, $args,
                    $value);
            }
            $method = '_woocommerceFormFieldFilter' . $args['type'];
            if (method_exists($this, $method)) {
                return $this->{$method}($field, $key, $args, $value);
            } else {
                if ( ! in_array($args['type'],
                    array('tel', 'password', 'checkbox', 'email'))) {
                    $args['type'] = 'text';
                }
                
                return $this->{'_woocommerceFormFieldFiltertext'}($field, $key,
                    $args, $value);
            }
        }
        
        private function _woocommerceFormFieldFilterPrivacy(
            $field,
            $key,
            $args,
            $value
        ) {
            return '<p class="form-row form-row-wide create-account ' . $key . ' woocommerce-validated">
                            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                                <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="' . $key . '" type="checkbox" name="' . $key . '" value="1">
                                <span>' . $args['label'] . '</span>
                            </label>
                        </p>';
        }
        
        private function _woocommerceFormFieldFilterstate(
            $field,
            $key,
            $args,
            $value
        ) {
            $states = WC()->countries->get_states('BG');
            foreach ($states as $code => $country) {
                $values[$code] = '<option value="' . $code . '"';
                if ($value == $code) {
                    $values[$code] .= ' selected="selected"';
                }
                $values[$code] .= '>' . $country . '</option>';
            }
            $options = implode('', $values);
            
            return '<div class="select_wrap h-gutter"><div class="select_item">
                <label class="required"
                       for="' . $key . '">' . $args['label'] . '</label>
                <select name="' . $key . '" class="select_custom bootstrap-select "
                        id="' . $key . '">' . $options . '
                </select>
            </div></div>';
        }
        
        private function _woocommerceFormFieldFiltercountry(
            $field,
            $key,
            $args,
            $value
        ) {
            return '<input type="hidden" name="' . $key . '" value="BG">';
            $countries = 'shipping_country' === $key ? WC()->countries->get_shipping_countries() : WC()->countries->get_allowed_countries();
            $values    = array();
            foreach ($countries as $code => $country) {
                $values[$code] = '<option value="' . $code . '"';
                if ($value == $code) {
                    $values[$code] .= ' selected="selected"';
                }
                $values[$code] .= '>' . $country . '</option>';
                
                
            }
            echo '';
            $options = implode('', $values);
            
            return '<p class="country_wrap h-gutter">
                <label class="required"
                       for="' . $key . '">' . $args['label'] . '</label>
                <select class="select_custom bootstrap-select"
                        id="' . $key . '">' . $options . '
                </select>
            </p>';
            $inner = '<select name="' . $key . '" class="select_custom bootstrap-select"
                        id="billing_country">' . implode('', $values) . '</select>';
            
            return $this->_woocommerceFormFieldWrap($field, $key, $args, $value,
                $inner);
        }
        
        private function _woocommerceFormFieldWrap(
            $field,
            $key,
            $args,
            $value,
            $inner
        ) {
            $required = '';
            if ($args['required']) {
                $required = 'required';
            }
            
            return '<p data-priority="' . $args['priority'] . '" class="validate-required checkout_fullname h-gutter ' . $key . '"
               id="' . $args['id'] . '">
                <label for="' . $args['id'] . '"
                       class="' . $required . '">' . $args['label'] . '</label>
                <span class="woocommerce-input-wrapper">' . $inner . '</span></p>';
        }
        
        private function _woocommerceFormFieldFiltertextarea(
            $field,
            $key,
            $args,
            $value
        ) {
            $required = '';
            if ($args['required']) {
                $required = 'required';
            }
            $field = $this->_woocommerceFormFieldWrap($field, $key, $args, $value,
                '<textarea  rows="10" cols="20" name="' . $key . '">' . $value . '</textarea>');
            
            return $field;
        }
        
        private function _woocommerceFormFieldFiltertext($field, $key, $args, $value)
        {
            $required = '';
            if ($args['required']) {
                $required = 'required';
            }
            $field = $this->_woocommerceFormFieldWrap($field, $key, $args, $value,
                '<input name="' . $args['id'] . '"
type="' . $args['type'] . '" class="input-text" value="' . $value . '" id="' . $args['id'] . '">');
            
            return $field;
        }
        
        private function _initTimber()
        {
            add_filter(
                'ngblocksdir', function ($values) {
                return array('templates', 'views');
            }
            );
            add_filter('ngthemecontext', array($this, 'timberContext'));
            add_action($this->prefix . 'render', array($this, 'render'));
        }
        
        public function render()
        {
            //$this->context=$this->timer->getContext();
            //$this->context['posts'] = Timber::get_posts();
            $this->timber->render(
            //  $this->context
            );
        }
        
        public function wpEnqueueScripts()
        {
            $scripts = array(
                'assets/js/popper.min.js',
                'assets/js/bootstrap.min.js',
                'assets/js/bootstrap-select.min.js',
                'assets/js/jquery.inputmask.min.js',
                'assets/js/slick.min.js',
                'assets/js/jquery.fancybox.min.js',
                'assets/js/fotorama.js',
                'assets/js/bootstrap-multiselect.js',
                'assets/js/jquery.range-min.js',
                'assets/js/script.js',
                'script.js'
            );
            $styles  = array(
                'assets/css/bootstrap.min.css',
                'assets/css/normalize.css',
                'assets/css/bootstrap-select.min.css',
                'assets/css/font-awesome.min.css',
                'assets/css/slick.css',
                'assets/css/jquery.fancybox.min.css',
                'assets/css/fotorama.css',
                'assets/css/bootstrap-multiselect.css',
                'assets/css/asRange.min.css',
                'assets/css/jquery.range.css',
                'assets/css/main.css',
                'style.css'
            );
            if ( ! is_admin()) {
                wp_deregister_script('jquery');
                
                wp_register_script(
                    'jquery',
                    self::scriptPath('assets/js/jquery.min.js'),
                    null
                );
            }
            wp_enqueue_script('jquery');
            foreach ($scripts as $k => $script) {
                $scr = self::scriptPath($script);
                wp_register_script(
                    self::sanitize($scr), $scr, array("jquery"),
                    self::version(), true
                );
                if ($k == 0) {
                    wp_localize_script(
                        self::sanitize($scr), 'nglink',
                        get_stylesheet_directory_uri()
                    );
                    wp_localize_script(
                        self::sanitize($scr), 'priceRange',
                        apply_filters('NGWooFilterpricerange', array(1, 1000))
                    );
                    if ( ! is_admin()) {
                        ob_start();
                        echo wc_print_notices();
                        $this->notices = ob_get_clean();
                        if (strlen($this->notices) > 0) {
                            wp_localize_script(
                                self::sanitize($scr), 'ngwcnotices',
                                $this->notices
                            );
                        }
                    }
                }
                wp_enqueue_script(self::sanitize($scr));
            }
            foreach ($styles as $style) {
                $scr = self::scriptPath($style);
                wp_register_style(
                    self::sanitize($scr), $scr, array(),
                    self::version(), "all"
                );
                wp_enqueue_style(self::sanitize($scr));
            }
        }
        
        public function filterngthemeoptions($array = array())
        {
            $array[] = array(
                'id'      => 'phone1',
                'label'   => __('Номер телефона #1', $this->prefix),
                'section' => __('Контакты')
            );
            $array[] = array(
                'id'      => 'phone2',
                'label'   => __('Номер телефона #2', $this->prefix),
                'section' => __('Контакты')
            );
            $array[] = array(
                'id'      => 'email',
                'label'   => 'email',
                'section' => __('Контакты')
            );
            $array[] = array(
                'id'      => 'address',
                'label'   => __('Адрес', $this->prefix),
                'section' => __('Контакты')
            );
            
            return $array;
        }
        
        public function wooFilterFields($fields)
        {
            $unset = array('company', 'address_2', 'postcode1');
            foreach ($fields as $k => $v) {
                foreach ($v as $k1 => $v1) {
                    foreach ($unset as $field) {
                        if (strpos($k1, $field)) {
                            unset($fields[$k][$k1]);
                            //    continue;
                        }
                    }
                }
            }
            
            return $fields;
        }
        
        public function wooSortFields($fields)
        {
            $priority = array(
                'first_name' => 10,
                'last_name'  => 20,
                'phone'      => 30,
                'mail'       => 40,
                'country'    => 50,
                'state'      => 45,
                'city'       => 70,
                'address'    => 80
            );
            foreach ($fields as $k => $v) {
                foreach ($v as $k1 => $v1) {
                    foreach ($priority as $field => $value) {
                        if (strpos($k1, $field)) {
                            $fields[$k][$k1]['priority'] = $value;
                            continue;
                        }
                    }
                }
            }
            
            return $fields;
        }
        
        public function wooSortCols($fields)
        {
            $cols = array(
                'name'     => 'left',
                'phone'    => 'left',
                'mail'     => 'left',
                'country'  => 'center',
                'state'    => 'center',
                'city'     => 'center',
                'address'  => 'center',
                'comment'  => 'bottom',
                'postcode' => 'center'
            );
            foreach ($fields as $k => $v) {
                foreach ($v as $k1 => $v1) {
                    foreach ($cols as $field => $col) {
                        if (strpos($k1, $field)) {
                            $fields[$k][$k1]['col'] = $col;
                            //    continue;
                        }
                    }
                }
            }
            
            return $fields;
        }
        
        public function removeImgAttr($image, $attachment_id, $size, $icon)
        {
            
            $image[1] = '';
            $image[2] = '';
            
            return $image;
        }
        
        public static function scriptPath($file)
        {
            if (strpos($file, 'http') !== 0) {
                return get_bloginfo("template_url") . "/" . $file;
            }
            
            return $file;
        }
        
        public static function sanitize($title)
        {
            return self::prefix() . crc32($title);
        }
        
    }
    
    require 'templates/timberclass.php';
    $timber = new TimberClass();
    new Ngtheme($timber);
    
    
    
    