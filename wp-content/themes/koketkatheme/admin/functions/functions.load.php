<?php
/**
 * Functions Load
 *
 * @package     koketka
 */
require_once( get_template_directory() . '/admin/functions/functions.php' );
require_once( get_template_directory() . '/admin/functions/functions.filters.php' );
require_once( get_template_directory() . '/admin/functions/functions.interface.php' );
require_once( get_template_directory() . '/admin/functions/functions.options.php' );
require_once( get_template_directory() . '/admin/functions/functions.admin.php' );