=== ng-dragonfly-blocks ===
Contributors: nikitaglobal
Plugin Name: ng-dragonfly-blocks
Tags: gutenberg, timber, themes
Author: Nikita Menshutin
Text Domain: Ngdragonflyexpertblocks
Domain Path: /languages
Requires at least: 3.6
Tested up to: 5.2
Stable tag: 1.3
Requires PHP: 5.6
Version: 1.3
License: 			GPLv2 or later
License URI: 		http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Description

[https://nikita.global/](https://nikita.global)

Your website should support https.

= Filters =

1. ngblocks getting blocks array from theme or other plugins

``

    array(

		'block' => array(
			'category' => 'common',
			'title' => __('Front page FBForm', $this->prefix),
			'template' => 'ngtheme/fform',
			'icon' => 'smiley',
			),
		'fields' => array(
			array(
				'name' => 'generalHeader',
				'type' => 'text',
				'label' => __('Block header', $this->prefix)
			),
		array(
				'name' => 'priceTitle',
				'type' => 'text',
				'label' => __('priceTitle', $this->prefix)
				),
		array(
				'name' => 'price',
				'type' => 'text',
				'label' => __('Price', $this->prefix)
		),
		array(
				'name' => 'header',
				'type' => 'text',
				'label' => __('Header', $this->prefix)
		),
		array(
				'name' => 'youget',
				'type' => 'wysiwyg',
				'label' => __('What you get', $this->prefix)
		),
		array(
				'name' => 'buttonText',
				'type' => 'text',
				'label' => __('Button text', $this->prefix)
		),
		array(
				'name' => 'formTitle',
				'type' => 'text',
				'label' => __('Form title', $this->prefix)
		),
		array(
				'name' => 'formShortcode',
				'type' => 'text',
				'label' => __('Form shortcode', $this->prefix)
		)
	));
    ?>
```

= Twig extra functions =

pul - line breaks to ul or p

nl2ul - line breaks to <ul><li></li></ul> code

nl2p - line breaks to <p></p>


= Actions =

No

== Installation ==

Use WordPress' Add New Plugin feature, searching "ng-dragonfly-blocks", or download the archive and:

1. Unzip the archive on your computer  
2. Upload plugin directory to the `/wp-content/plugins/` directory
3. Activate the plugin through the 'Plugins' menu in WordPress

Then in theme development:

1. Create folder templates.
2. Create folder ngtheme in it
3. Add the following snippet
```PHP
            add_filter('ngblocksdir', function ($values) {
                return array('templates', 'views');
            });
            define('NGBLOCKSAUTO', true);
            add_filter('ngthemecontext', array($this, 'timberContext'));
            function timberContext($values)
            {
                $values['site']->lang = preg_replace('/_[^\Z]*\Z/', '', get_locale());
                $mods                 = get_theme_mods();
                foreach ($mods as $key => $mod) {
                    $values['mods'][$key] = ($mod);
                }
                return $values;
            }
```
4. Create files for each block in templates/ngtheme with twig extensions
5. Use ngblock operator to create fields automatically:
```twig
{% set images=block.images|ngblock('{"type":"images","label":"slides"}') %}
```
6. All the rest like in timber-library plugin
== Changelog ==

= 1.0 (2019-11-09) =
* Ready, has filters
