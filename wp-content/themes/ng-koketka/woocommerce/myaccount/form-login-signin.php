<?php
    if ( ! defined('ABSPATH')) {
        exit; // Exit if accessed directly.
    }
    do_action('woocommerce_before_customer_login_form');
    
    $b1 = 1;
?>
    <form id="sign_in"
          class="form_custom form_sign_in<?php if ( ! $args['notmodal']) { ?> notmodal<?php } ?> "
          <?php if ( ! $args['notmodal']){ ?>style="display:none;"<?php } ?>
          method="post">
        <?php do_action('woocommerce_login_form_start'); ?>
        <?php do_action('woocommerce_login_form_start'); ?>
        <div class="form_item">
            <h4><?php esc_html_e('Login', 'woocommerce'); ?></h4>

            <label class="required"
                   for="username"><?php esc_html_e('Username or email address',
                    'woocommerce'); ?></label>
            <input type="text"
                   class="woocommerce-Input woocommerce-Input--text input-text"
                   name="username" id="username" autocomplete="username"
                   value="<?php echo ( ! empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>"
                   required="required"/><?php // @codingStandardsIgnoreLine ?>

            <label class="required" for="password"><?php esc_html_e('Password',
                    'woocommerce'); ?></label>
            <input class="woocommerce-Input woocommerce-Input--text input-text"
                   type="password" name="password" id="password"
                   autocomplete="current-password" required="required"/>

            <div class="checkbox_wrap">
                <div class="checkbox_custom">
                    <input class="woocommerce-form__input woocommerce-form__input-checkbox"
                           name="rememberme" type="checkbox" id="remember_me"
                           value="forever"/>
                    <label for="remember_me"><?php esc_html_e('Remember me',
                            'woocommerce'); ?></label>
                </div>
                <div class="forgot_password">
                    <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('Lost your password?',
                            'woocommerce'); ?></a>
                </div>
            </div>
            <?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
            <button type="submit"
                    class="woocommerce-button button woocommerce-form-login__submit"
                    name="login" value="<?php esc_attr_e('Log in',
                'woocommerce'); ?>"><?php esc_html_e('Log in',
                    'woocommerce'); ?></button>
        </div>
        <?php if ( ! $args['notmodal']) { ?>
            <div class="popup_close">
                <svg width="22" height="21" viewBox="0 0 22 21" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <line x1="18" y1="3.53553" x2="4.53553" y2="17" stroke="white"
                          stroke-width="5" stroke-linecap="round"/>
                    <line x1="17.4645" y1="17" x2="4" y2="3.53554" stroke="white"
                          stroke-width="5" stroke-linecap="round"/>
                </svg>
            </div>
        <?php } ?>
        <?php do_action('woocommerce_login_form_end'); ?>
    </form>
<?php do_action('woocommerce_after_customer_login_form'); ?>