<?php
    
    /**
     * Plugin Name: NG dragonfly.expert blocks
     * Plugin URI: https://nikita.global
     * Description: Gutenberg blocks for dragonfly.expert site by nikita.global
     * Author: Nikita Menshutin
     * Version: 1.1
     * Text Domain: ngdragonflyexpertblocks
     * Domain Path: languages
     *
     * PHP version 7.2
     *
     * @category NikitaGlobal
     * @package  NikitaGlobal
     * @author   Nikita Menshutin <nikita@nikita.global>
     * @license  https://nikita.global commercial
     * @link     https://nikita.global
     * */
    defined('ABSPATH') or die("No script kiddies please!");
if (! class_exists("ngdragonflyexpertblocks")) {
    /**
         * Our main class goes here
         *
         * @category NikitaGlobal
         * @package  NikitaGlobal
         * @author   Nikita Menshutin <wpplugins@nikita.global>
         * @license  http://opensource.org/licenses/gpl-license.php GNU
         * @link     https://nikita.global
         */
    Class Ngdragonflyexpertblocks
    {
        /**
             * Construct
             *
             * @return void
             **/
        public function __construct()
        {
            $this->prefix     = 'ngblocks';
            $this->version    = '1.1';
            $this->pluginName = __('NG dragonfly.expert blocks');
            $this->options    = get_option($this->prefix);
            $this->xmlprefix  = '<?xml encoding="utf-8" ?>';
            $this->twigExt='.twig';
            add_action('init', array($this, 'initContext'));
            add_action('init', array($this, 'initTemplates'));
            add_filter($this->prefix.'thememods', array($this,'thememods'));
            load_plugin_textdomain(
                $this->prefix,
                false,
                $this->prefix . '/languages'
            );
            add_filter(
                'timber_post_getter_get_posts',
                array(
                    $this,
                    'processBlocks'
                )
            );
            add_action(
                'enqueue_block_editor_assets',
                array(
                    $this,
                    'gutenbergCustomAssets'
                )
            );
            add_action('wp_ajax_renderblock', array($this, 'renderBlock'));
            add_action('wp_ajax_nopriv_renderblock', array($this, 'renderBlock'));
            add_filter($this->prefix, array($this, 'registerAutoBlocks'));
            add_filter('timber/twig', array($this, 'addToTwig'));
            add_filter('timber/context', array($this, 'addMenus'));
            remove_filter('the_content', 'wpautop');
            remove_filter('the_excerpt', 'wpautop');
            if (! function_exists('str_get_html')) {
                include_once 'simple_html_dom.php';
            }
        }
    
        /**
         * Make autoblocks
         *
         * @return void
         */
        public function initTemplates()
        {
            if (! defined('NGBLOCKSAUTO') || ! NGBLOCKSAUTO) {
                return;
            }
            if (! defined('WP_DEBUG') || ! WP_DEBUG) {
   //             return;
            }
            $key = $this->prefix . 'blocksToRegister';
            if (get_transient($key)) {
                $this->blocksToRegister = get_transient($key);
            } else {
                $this->blocksToRegister = array();
                foreach ($this->blockFolder as $folder) {
                    $path = get_stylesheet_directory() . '/' . $folder;
                    $this->_initTemplatesPath($path);
                }
            }
            if (empty($this->blocksToRegister)) {
                return;
            }
            update_option($this->prefix . 'btr', $this->blocksToRegister, false);
            
        }
    
        /**
         * Extracting templates for autoblocks
         *
         * @param string $path to templates
         *
         * @return void
         */
        private function _initTemplatesPath($path)
        {
            $path .= '/ngtheme/';
            if (! is_dir($path)) {
                return;
            }
            $files = (scandir($path));
            if (count($files) <= 2) {
                return;
            }
            foreach ($files as $file) {
                $fullpath = $path . $file;
                if (! is_file($fullpath)) {
                    continue;
                }
                if (substr(strrchr($fullpath, '.'), 1) == 'twig') {
                    $this->_initTemplate($fullpath);
                }
            }
        }
    
        /**
         * Register auto blocks (from option)
         *
         * @param array $blocks blocks
         *
         * @return array blocks
         */
        public function registerAutoBlocks($blocks = array())
        {
            $autoblocks = get_option($this->prefix . 'btr');
            if (!$autoblocks|| !is_array($autoblocks)) {
                return $blocks;
            }
            return array_merge($autoblocks, $blocks);
                

        }
        
        /**
         * Getting data from template file
         *
         * @param string $file file with path
         *
         * @return void
         */
        private function _initTemplate($file)
        {
            $tpl      = basename($file);
            $contents = file_get_contents($file);
            $pattern  = 'block\.([^\|]*)\|[^ngblock]*ngblock\(\'([^\)]*)\'\)';
            preg_match_all('/' . $pattern . '/', $contents, $matches);
            $vars = array();
            if (empty($matches)) {
                return;
            }
            foreach ($matches[0] as $index => $values) {
                $vars[$matches[1][$index]] = $matches[2][$index];
            }
            $this->blocksToRegister[$tpl] = array(
                'block'  => array(
                    'category' => 'common',
                    'title'    => $tpl,
                    'template' => 'ngtheme/' . str_replace($this->twigExt, '', $tpl),
                    'icon'     => 'smiley'
                    
                ),
                'fields' => $this->_initTemplateMakeFields($vars)
                
            );
        }
    
        /**
         * Make fields for autoblocs
         *
         * @param array $vars extracted data
         *
         * @return array fields
         */
        private function _initTemplateMakeFields($vars)
        {
            $fields = array();
            foreach ($vars as $name => $var) {
                $fields[$name]         = json_decode(
                    $this->_removeWhiteSpaces($var),
                    true
                );
                $fields[$name]['name'] = $name;
            }
                
            return $fields;
        }
    
        /**
         * Remove whitespaces
         *
         * @param string $var string
         *
         * @return string without whitespaces
         */
        private function _removeWhiteSpaces($var)
        {
            $string = str_replace(array("\n", "\r"), " ", $var);
            return preg_replace('/\ {2,}/', ' ', $string);
                

        }
            
        /**
             * Initing context for blocks
             *
             * @return void
             */
        public function initContext()
        {
            $this->blockFolder = apply_filters('ngblocksdir', array());
            if (empty($this->blockFolder)) {
                $this->blockFolder=array('templates', 'views');
            }
            Timber::$dirname   = $this->blockFolder;
            $this->context     = apply_filters(
                $this->prefix.'thememods',
                Timber::context()
            );
            $this->blocks      = apply_filters('ngblocks', array());
            $this->blocks      = self::_blocksDefaults($this->blocks);
            $this->blockNames  = array();
            if (empty($this->blocks)) {
                return;
            }
            foreach ($this->blocks as $block) {
                $this->templates[] = $block['block']['template'];
            }
        }
    
        /**
         * Theme mods
         *
         * @param array $values mods
         *
         * @return array
         */
        public function themeMods($values=array())
        {
            $values['site']->lang = preg_replace('/_[^\Z]*\Z/', '', get_locale());
            $mods                 = get_theme_mods();
            if (!is_array($mods) || empty($mods)) {
                return array();
            }
            foreach ($mods as $key => $mod) {
                $values['mods'][$key] = ($mod);
            }
            return $values;
        }
        /**
             * Enqueue styles and scripts
             *
             * @return void
             */
        public function gutenbergCustomAssets()
        {
            wp_register_style(
                $this->prefix . 'css',
                plugin_dir_url(__FILE__) . 'style.css',
                array(), $this->version, 'all'
            );
            wp_enqueue_style($this->prefix . 'css');
            wp_register_script(
                $this->prefix,
                plugin_dir_url(__FILE__) . 'js/custom.js',
                array('wp-blocks', 'wp-editor'),
                true
            );
            if (defined('WP_DEBUG') && WP_DEBUG) {
                $this->blocks['__params']          = array();
                $this->blocks['__params']['debug'] = true;
            }
            wp_localize_script(
                $this->prefix, $this->prefix,
                $this->blocks
            );
            wp_enqueue_script($this->prefix);
        }
            
        /**
             * Filter blocks with timber
             *
             * @param array $context blocks not filtered
             *
             * @return array blocks
             */
        public function processBlocks($context)
        {
            if (!$context || !is_array($context) || empty($context))
            {
                return $context;
            }
            foreach ($context as $key => $value) {
                $context[$key] = $this->_processBlocksSinglePost($value);
            }
            return $context;
        }
            
        /**
             * Adding menus to context
             *
             * @param array $context blocks not filtered
             *
             * @return array blocks
             */
        public function addMenus($context)
        {
            if (! isset($context['menus'])) {
                $context['menus'] = array();
            }
            foreach (get_nav_menu_locations() as $location => $menu) {
                @$context['menus'][$location] = new \Timber\Menu($location);
            }
            return $context;
        }
            
        /**
             * Remove filter autop
             *
             * @param string $content content
             *
             * @return string content
             */
        public function removeP($content)
        {
            'post' === get_post_type() && remove_filter('the_content', 'wpautop');
                
            return $content;
        }
            
        /**
             * Rendering blocks for editor in backend
             *
             * @return json compiled block
             */
        public function renderBlock()
        {
            $data = (json_decode(wp_unslash($_POST['data']), true));
                
            $context = $this->context;
                
            $blockParams = ($data);
            foreach ($blockParams as $k => $v) {
                if (is_string($v)) {
                    $blockParams[$k] = html_entity_decode($v);
                }
            }
            if (isset($data['name'])) {
                $blockParams['name'] = $data['name'];
            }
            $context['block'] = $blockParams;
            $out              = Timber::compile(
                $blockParams['blockName'] .
                $this->twigExt,
                $context,
                0
            );
            wp_send_json(do_blocks($out));
            wp_die();
        }
            
        /**
             * Adding useful custom methods for twig
             * (extending)
             *
             * @param object $twig twig object given by Timber
             *
             * @return object twig extended
             */
        public function addToTwig($twig)
        {
            $methods = (get_class_methods($this));
            foreach ($methods as $method) {
                if (strpos(
                    $method, 'twig'
                ) === 0) {
                    $methodname = str_replace('twig', '', $method);
                    $twig->addFilter(
                        new Timber\Twig_Filter(
                            $methodname,
                            array(
                                $this,
                                $method
                            )
                        )
                    );
                }
            }
                
            return $twig;
        }
            
        /**
             * Nl2ul method
             * Replacing line-breaks, wrapping with <ul><li>
             *
             * @param string $text text
             *
             * @return string wrapped text
             */
        function twignl2ul($text)
        {
            return '<ul><li>' .
                   str_replace(
                       array(
                           "\r\n",
                           "\r",
                           '<br><br>',
                           '<br>',
                           "\n"
                       ),
                       '</li><li>',
                       $text
                   ) . '</li></ul>';
        }

        function twigwcprice($string)
        {
            if (!function_exists('wc_price')) {
                return $string;
            }
            return wc_price($string);
        }
        
        /**
             * Does nothing, required to be parsed by another method
             *
             * @param string $string string
             * @param string $params filter params
             *
             * @return string string
             */
        function twigngblock($string, $params=false)
        {
            if ($params) {
                
                $params=json_decode($params, true);
            }
            
            return $string;
        }
        
        function twigngoption($string, $params=false)
        {
            if ($params)
            {
                $params=json_decode($params, true);
            }
            return $string;
        }
            
        /**
             * Nl2p method
             * Replacing line-breaks, wrapping with <p></p>
             *
             * @param string $text text
             *
             * @return string wrapped text
             */
        function twignl2p($text)
        {
            return '<p>' .
                   str_replace(
                       array(
                           "\r\n",
                           "\r",
                           '<br><br>',
                           "\n"
                       ),
                       '</p><p>',
                       $text
                   ) .
                   '</p>';
        }

        
        /**
             * Replace br with white-spaces
             *
             * @param string $text text
             *
             * @return string text
             */
        function twignobr($text)
        {
            return str_replace('<br>', "\n", $text);
        }
            
        /**
             * If no line breaks returns <p></p>
             * else <ul><li></li></ul>
             *
             * @param string $text text
             *
             * @return string p or ul wrapped text
             */
        function twigpul($text)
        {
            $text1 = str_replace(
                array(
                    "\r\n", "\r", "<br><br>", "\n"
                ),
                "<br>",
                $text
            );
                
            $parts = explode('<br>', $text1);
            if (count($parts) == 1) {
                return '<p>' . $text . '</p>';
            }
                
            return self::twignl2ul($text1);
        }
            
        /**
             * Remove all non-numbers from string
             *
             * @param string $text string
             *
             * @return int number
             */
        function twigkeepNumbers($text)
        {
            return preg_replace('/[^0-9]/', '', $text);
        }
            
        /**
             * Processing single post,
             * extracting blocks
             * replacing
             *
             * @param string $value post
             *
             * @return string post content
             */
        private function _processBlocksSinglePost($value)
        {
                
            $this->postValue = $value;
            preg_match_all(
                '/(<!-- .* -->.*<!-- .* -->)/sU',
                $this->postValue->post_content,
                $matches
            );
            if (! isset($matches[1])) {
                return $value;
            }
            $replace = array();
            foreach ($matches[0] as $k => $v) {
                $replace[$k] = $this->_processBlock($v);
            }
            foreach ($replace as $k => $v) {
                $value->post_content = str_replace(
                    $matches[0][$k],
                    html_entity_decode($v),
                    $value->post_content
                );
            }
                
            return $value;
        }
            
        /**
             * Processing html raw block
             * checking for comments,
             * calling method to process commented block
             *
             * @param string $htmlRaw html
             *
             * @return string after processing
             */
        private function _processBlock($htmlRaw)
        {
            $html = str_get_html($htmlRaw);
            $es   = $html->find('comment');
            foreach ($es as $el) {
                if (! $this->_processBlockComment($el->innertext)) {
                    return $htmlRaw;
                }
                return $this->_processBlockComment($el->innertext);
            }
        }
            
        /**
             * Rendering blocks
             *
             * @param string $comment html with comments
             *
             * @return string rendered block
             */
        private function _processBlockComment($comment)
        {
            preg_match_all('/wp\:([^ ]*) \{/', $comment, $matches);
            if (empty($matches[1])) {
                return false;
            }
            $blockname = $matches[1][0];
            if (! isset($this->templates)
                || ! in_array($blockname, $this->templates)
            ) {
                return false;
            }
            
            
            
            return preg_replace_callback(
                '/.*(\{[^\}]*\}).*/', function ($matches) use ($blockname) {
                    $context          = $this->context;
                    $context['block'] = json_decode($matches[1], true);
                    
                    return do_blocks(
                        Timber::compile(
                            $blockname . $this->twigExt,
                            $context
                        )
                    );
                }, $comment
            );
        }
    
        /**
         * If field default value is not set
         * settings false
         *
         * @param array $blocks blocks
         *
         * @return array
         */
        private static function _blocksDefaults($blocks)
        {
            foreach ($blocks as $tpl=>$block) {
                foreach ($block['fields'] as $fieldName=>$field) {
                    if (!isset($field['default'])) {
                        $blocks[$tpl]['fields'][$fieldName]['default'] =false;
                    }
                }
            }
            return $blocks;
        }
    }
}
    
    $blocks=new ngdragonflyexpertblocks();

